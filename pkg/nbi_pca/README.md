# Principal Component Analysis 

This package provides a simple PCA class and some additional helper
functions. 

## Note on PCA

The note [Principal Component
Analysis](https://cholmcc.gitlab.io/nbi-python/statistics/#OA) gives
some background and examples of use.

## Application Programming Interface Documentation 

The API is
[documented](https://cholmcc.gitlab.io/nbi-python/statistics/nbi_pca). 

2019 © _Christian Holm Christensen_
