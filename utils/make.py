#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import os
import tempfile
import shutil

def runCmdAndCheck(cmd,target):
    print("Running the command:\n\n\t{}\n".format(cmd))
    
    ret = os.system(cmd)
    if ret != 0 or not os.access(target,os.F_OK):
        raise RuntimeError("Command failed to produce {}\n\n\t{}"
                           .format(target,cmd))
    return True
        
def runXeLaTeX(basename):
    shutil.copy("utils/danish.ldf","danish.ldf")
    cmd = "xelatex -interaction=batchmode {}.tex".format(basename)
    ret = runCmdAndCheck(cmd,basename+".aux")
    if os.access('danish.ldf',os.F_OK):
        os.unlink('danish.ldf')
    return ret 

def runNBConvert(basename,fmt,*argv,**kwargs):
    cmd = "jupyter nbconvert --to {} ".format(fmt)
    cmd += ' '.join(["--" + a for a in argv])
    cmd += ' '.join(["--" + k + " " + v for k,v in kwargs.items()])
    cmd += " {}.ipynb".format(basename)
    return runCmdAndCheck(cmd,basename+".tex")

def runPost(basename):
    _,tmp = tempfile.mkstemp('.tmp',basename,'.',True)
    cmd = "./utils/post.sh < {}.tex > {}".format(basename,tmp)
    if runCmdAndCheck(cmd,tmp):
        os.rename(tmp,basename+".tex")

def makePDF(basename,template="tplxs/latex/dkminearticle.tplx"):
    runNBConvert(basename,"latex",template=template)
    runPost(basename)
    for _ in range(3):
        runXeLaTeX(basename)

def makeHTML(basename,template=""):
    return runNBConvert(basename,"html","allow-errors","execute")

def makeLib(basename,out):
    cmd = "./utils/writelib.py {}.ipynb -o {}.py".format(basename,out)
    return runCmdAndCheck(cmd,out+".py")

if __name__ == "__main__":
    import argparse
    import sys

    cmds = ["all", "pdf", "html", "py", "clean"]
    ap = argparse.ArgumentParser(description="Make stuff")
    ap.add_argument('what',default='all',choices=cmds,
                    help="What to do")
    args = ap.parse_args()
    what = args.what.lower().strip()

    doPDF   = what in ['all', 'pdf']
    doHTML  = what in ['all', 'html']
    doPY    = what in ['all', 'py']
    doClean = what in ['clean']

    if doPDF:
        makePDF("Python")
        makePDF("Statistik","tplxs/latex/dkminereport.tplx")

    if doHTML:
        makeHTML("Python");
        makeHTML("Statistik");

    if doPY:
        makeLib("Statistik","nbi_stat")

    if doClean:
        os.system("rm -f *~ *.aux *.log *.out *.toc *.synctex*")
        os.system("rm -f nbi_stat.py Python.pdf Statistik.pdf")
        os.system("rm -rf Python.html Statistik.html Statistik_files")
        os.system("rm -f tplxs/latex/*~ utils/*~ .gitignore~")
        os.system("rm -rf auto __pycache__ public/nbi_stat")


        
        
        
        
    
    
    
