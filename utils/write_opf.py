#!/usr/bin/env python3
# -*- encoding: utf-8 -*- 

def split_authors(authors):
    from re import split
    return split('[&,]',authors)

def make_opf(authors='',title='',subtitle='',publisher=''):
    from re import split
    s = f'''\
<package xmlns="http://www.idpf.org/2007/opf" 
         version="3.0">
  <metadata xmlns:opf="http://www.idpf.org/2007/opf" 
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:dcterms="http://purl.org/dc/terms/" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <dc:title id="id">{title}</dc:title>
    <dc:publisher>{publisher}</dc:publisher>
'''
    for i, author in enumerate(split_authors(authors)):
        author = author.strip()
        s += f'    <dc:creator id="id-{i}">${author}</dc:creator>\n'

    s += '''\
  </metadata>
</package>
'''
    return s

def make_txt(authors='',title='',subtitle='',publisher=''):
    return f'{title}\n' + \
        f'{subtitle}\n' + \
        f'{" & ".join([a.strip() for a in split_authors(authors)])}\n' + \
        f'{publisher}'

def write_opf(nbfile,maker):
    from pathlib import Path
    from nbformat import read, reads, current_nbformat
    from nbformat.v4 import new_markdown_cell
    from traitlets.config import Config
    from pprint import pprint
    from re import sub, match
    
    nb = read(nbfile,as_version=current_nbformat)

    # meta  = nb.get('metadata',{})
    # trans = meta.get('nbTranslate',{})
    # pprint(meta)
    
    for cell in nb['cells']:
        meta = cell.get('metadata',{})
        tags = meta.get('tags',[])

        if not 'title' in tags:
            continue

        src = cell['source']
        authors   = ''
        title     = ''
        subtitle  = ''
        publisher = ''

        for line in src.split('\n'):
            if not line.startswith('#'):
                continue

            content = sub(r'^#+\s+(.*)<a class=.{1,2}tocSkip.*',r'\1',line)
            if content is None:
                continue
            
            which = None
            if match('#\s+',line): title = content
            elif  match('##\s+',line):
                if subtitle == '': subtitle = content
            elif match('###\s+',line):
                if authors == '': authors = content
                elif publisher == '': publisher = content

    s = maker(authors=authors,title=title,subtitle=subtitle,publisher=publisher)
    print(s)

if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser('Write meta data to OPF')
    ap.add_argument('input',help='Input notebook',
                    type=FileType('r'))
    ap.add_argument('-t','--type',help='Output type',
                    choices=['opf','txt'],default='txt')
    args = ap.parse_args()

    m = {'opf':make_opf, 'txt':make_txt}[args.type]
    
    write_opf(args.input,m)
    
