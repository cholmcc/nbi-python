#!/usr/bin/env python3

from logging import warning as warn, info, debug, getLogger

def read_code(code_file):
    from re import match 
    return [l for l in [l.replace('\n','') for l in code_file.readlines()]
            if match('##|[^#]',l) or l == '']

def one_code(src,code,inname):
    start = code[0].strip();
    end   = code[-1].strip();
    srcl  = [l.replace('\n','') for l in src.split('\n')]

    debug(f'Looking for "{start}","{end}"')
    m     = [i for i,l in enumerate(srcl)
             if l.startswith(start) or l.startswith(end)]

    if len(m) == 0:
        return src, False

    if len(m) == 1:
        lsrc  = '\n'.join([f'{i:3d} {l}' for i,l in enumerate(srcl)])
        warn(f'Found only one of "{start}","{end}" in ({inname})\n'
             f'{lsrc}')
        return src, False

    debug(f'Found match of "{start}","{end}" at {m}')
    nsrc = "\n".join(srcl[:m[0]] + code + srcl[m[1]+1:])
    
    return nsrc,True

def replace(inp,out,code_files):
    codes = [read_code(f) for f in code_files]

    from nbformat import read, reads, write, current_nbformat
    from nbformat.sign import NotebookNotary
    from nbformat.v4 import new_markdown_cell

    nb = read(inp,as_version=current_nbformat)

    all_status = False
    for cell in nb.cells:
        if cell.cell_type != 'code':
            continue

        src = cell.source

        status = False
        for code,code_file in zip(codes,code_files):
            src,st = one_code(src,code,inp.name)
            if st:
                info(f'Replaced {code_file.name}')
                status = True

        if status:
            all_status  = True
            cell.source = src
            

    if not all_status:
        warn(f'No substitutions made in {inp.name}')
        
    write(nb,out)

from logging import  CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET

logd = {'critical':CRITICAL,
        'error':   ERROR,
        'warning': WARNING,
        'info':    INFO,
        'debug':   DEBUG,
        'default': NOTSET}

def set_log_level(level):
    getLogger().setLevel(logd[level])

if __name__ == "__main__":
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser('Replace special code with updated code')
    ap.add_argument('input',help='Input file',
                    type=FileType('r'))
    ap.add_argument('output',help='Otuput file',
                    default='tmp.ipynb',
                    nargs='?',
                    type=FileType('w'))
    ap.add_argument('-c','--code',type=FileType('r'),
                    help='Files with code to replace',
                    nargs='+')
    ap.add_argument('--verbose', '-v',
                    choices=list(logd.keys()),default='info')


    args = ap.parse_args()

    set_log_level(args.verbose)
    if args.code is None:
        warn('No codes to substitute')
        exit(1)
    
    
    replace(args.input,args.output,args.code)
    
