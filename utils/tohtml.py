#!/usr/bin/env python3

def gen_html():
    from nbconvert import HTMLExporter

    expo = HTMLExporter()

    return expo, '.html'

def gen_reveal():
    from nbconvert import SlidesExporter
    
    expo = SlidesExporter()
    expo.default_config.reveal_theme = 'simple'

    return expo, '.slides.html'
    
def to(nbfile,template,gen):
    from pathlib import Path
    from nbformat import read, reads, current_nbformat
    from nbformat.v4 import new_markdown_cell
    from traitlets.config import Config


    nb = read(nbfile, as_version=current_nbformat)

    expo, ext = gen(template);
    if template is not None and template != '':
        expo.default_config.template_file = template

    outs, _   = expo.from_notebook_node(nb)
    outfn     = nbfile.name.replace('.ipynb',ext)
    print(f'Writing output to {outfn}')
    with open(outfn,'w') as outf:
        outf.write(outs)

def to_reveal(nbfile,template):
    from pathlib import Path
    from nbformat import read, reads, current_nbformat
    from nbformat.v4 import new_markdown_cell
    from traitlets.config import Config
    from nbconvert import HTMLExporter


    nb = read(nbfile, as_version=current_nbformat)

    expo = HTMLExporter()

    if template is not None and template != '':
        expo.default_config.template_file = template

    outs, _ = expo.from_notebook_node(nb)

    outfn = nbfile.name.replace('.ipynb','.html')
    print(f'Writing output to {outfn}')
    with open(outfn,'w') as outf:
        outf.write(outs)
        
if __name__ == "__main__":
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser('Export NB as HTML')
    ap.add_argument('input',help='Input notebook',
                    type=FileType('r'))
    ap.add_argument('--template',help='Template to use',
                    default='')
    ap.add_argument('--to',help='Format to write',
                    default='html')

    args = ap.parse_args()

    to2gen = {'html': gen_html,
              'slides': gen_reveal }

    to(args.input,args.template,to2gen[args.to])
    

# EOF


    
