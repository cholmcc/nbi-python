# -*- coding: utf-8 -*-
"""EPUB Exporter class

Author: Christian Holm Christensen <cholmcc@gmail.com>
Copyright: © 2021 Christian Holm Christensen
License: BSD-3-clause
"""
from traitlets import default, Unicode, Bool, List, Integer, Enum, Dict
from traitlets.config import Config
from nbconvert.exporters.html import HTMLExporter

class EPUBExporter(HTMLExporter):
    '''Exports to EPUB via an HTML exporter.  The export goes in several
    steps

    1. The Notebook is exported to HTML via the standard HTMLExporter 
       (base class of this class) but using a specific template. 

       That template is needed to 
    
       a. Pass specific options to MathJax - for example SVG generation 
          (EPUB does not support MathJax) 

       b. In the MathJax queue inject code to add a '<div>' with a
          specific ID (#MathJaxEnd) once the MathJax rendering is
          done.  We use this later on with selenium (headless browser)
          to make sure all math has been rendered before we grap the
          HTML

    2. Start up a headless browser (selenium) and point it to the HTML
       just generated.

       We make the browser wait for the '<div>' with ID '#MathJaxEnd'. 
       Once that tag is present in the HTML page, we know that all 
       the math on the page has been rendered to SVG. 

       We then retrieve the rendered page source. 

    3. The page source retrieved from the headless browser is passed on 
       to an HTML parser (beautifulsoup).   This will parse out the DOM 
       of the HTML page. 

    4. We then start to clean the DOM for unwanted elements, attributes, 
       and so on.  This envolves 

       a. Remove the MathJax script tags 
       b. Remove all MathML tags 
       c. Remove all script tags that point to remote locations 
          (most EPUB viewers do not allow this, and JavaScript 
          is generally ill-supported)
       d. Remove script and link tags related the 'toc2' expansion. 
       e. Remove link to custom.css 
       f. (optionally) make all IDs lower case in links 
       g. Convert SVGs.  This is needed because we should make sure
          the SVGs are properly sized and because some viewers do not
          like SVGs but prefer PNGs.

          For each SVG, do  

          i. Collect 'defs' tags and remove from 'svg' tag
          ii. If we have one or more 'defs' give up
          iii. Clean up the SVG by resolving references from 
               collected 'defs'.  Also, if the SVG has defined 
               a width and height define this in units of 'px' 
          iv. Create an 'img' tag preserving some attributes from the 
              SVG. If the 'alt' attribute is set, we keep that too

              Then either turn the SVG into a PNG (via cairo), or the
              SVG, and base64 encode either to the 'img' 'src'
              attribute.
          v. Replace the SVG tag with the created 'img' tag
          
       h. Fix up the CSS in the HTML file.  We collect all 'style' 
          tags.  Then we define all definitions of variable and 
          substitute in the values at variable references 
          ('var(--name)').  Note, we do this incrementally as some 
          variables may reference other variables.  Some variables 
          are blacklisted from this process.  We do this because 
          Calibre does not really understand variables in the CSS.
       i. Make sure that the 'overflow' property isn't set on the 
          'body' element (messes up pagination by the EPUB viewers)
    5. For all header elements, if it contains an 'a' tag with class 
       'tocSkip', add the class 'nobreak'.  This is so that we will 
       only break up the HTML at real chapter or section headings. 
    6. Once this is done, we have a clean HTML.  To further clean 
       it, we may pass it through 'tidy'. 
    7. Finally, we call the application `ebook-convert` (from Calibre) 
       to generate the EPUB.

    Initially I did all the steps one-by-one and I had specialised
    templates for it.  However, after thinking about it and a little
    inspiration from the WebPDF exporter, I realised I could put
    everthing into an exporter. That simplifies things quiet a bit.

    '''
    export_from_notebook = 'EPUB'

    svg_as_png = Bool(True, help='Convert SVGs to PNGs').tag(config=True)

    lower_case_ids = Bool(False,  help='Lower case anchor IDs').tag(config=True)

    ex_in_points = Integer(20, help='Size of 1ex in points (not pixels)'
                           ).tag(config=True)

    render_timeout = Integer(100,help='Rendering timeout in seconds'
                             ).tag(config=True)

    chromium_path = Unicode('/usr/bin/chromium',
                            help='Path to chromium application'
                            ).tag(config=True)

    converter_path = Unicode('/usr/bin/ebook-convert',
                             help='Path to Calibre converter application'
                             ).tag(config=True)

    nobreak_tags = List(help='Add class nobreak to these tags'
                        ).tag(config=True)

    epub_version = Enum([2,3],default_value=3,
                        help='Version of EPUB to write').tag(config=True)

    mathjax_end = Unicode('MathJaxEnd',help='ID of tag to insert'
                          ).tag(config=True)

    output_profile = Unicode('tablet',help='Output profile to use'
                             ).tag(config=True)

    title = Unicode('',help='Title of EPub').tag(config=True)

    authors = Unicode('',help='Authors (separated by "&")').tag(config=True)
    
    comments = Unicode('',help='Book description').tag(config=True)

    publisher = Unicode('',help='Publisher').tag(config=True)

    series = Unicode('',help='Series of EPub').tag(config=True)

    isbn = Unicode('',help='ISBN of the book').tag(config=True)

    language = Unicode('',help='Set language of book').tag(config=True)

    pubdate = Unicode('',help='Set publication date').tag(config=True)

    cover = Unicode('',help='Cover SVG or PNG').tag(config=True)

    keep_temporary = Bool(False,help='Keep temporary files').tag(config=True)
    
    # #
    # 'breadth-first',
    # # Output options
    # 'preserve-cover-aspect-ratio','no-default-epub-cover',
    # 'pretty-print','flow-size','epub-toc-at-end','toc-title',
    # 'epub-inline-toc','dont-split-on-page-breaks',
    # 'epub-flatten','no-svg-cover',
    # # Look and feel
    # 'base-font-size','disable-font-rescaling','font-size-mapping',
    # 'line-height','minimum-line-height','linearize-tables',
    # 'transform-html-rules','extra-css','filter-css',
    # 'transform-css-rules','expand-css','smarten-punctuation',
    # 'unsmarten-punctuation','margin-top','margin-left',
    # 'margin-right','margin-bottom','insert-blank-line',
    # 'insert-blank-line-size','remove-paragraph-spacing',
    # 'remove-paragraph-spacing-indent-size',
    # 'keep-ligatures',
    # # Heuristics
    # 'enable-heuristics',
    # 'disable-markup-chapter-headings',
    # 'disable-italicize-common-cases',
    # 'disable-fix-indents','html-unwrap-factor',
    # 'disable-unwrap-lines', 'disable-delete-blank-paragraphs',
    # 'disable-format-scene-breaks',
    # 'disable-dehyphenate','disable-renumber-headings',
    # 'replace-scene-break',
    # # Search replace
    # 'search-replace',
    # # Structure
    # 'chapter-mark','prefer-metadata-cover',
    # 'remove-first-image','insert-metadata',
    # 'disable-remove-fake-margins',
    # 'start-reading-at',
    # # TOC
    # 'level3-toc',
    # # Meta data
    # 'title','authors','cover','comments',
    # 'publisher','series','rating','isbn',
    # 'tags','language','pubdate','from-opf'],

    extra_args = Dict(Unicode(),key_trait=Unicode(),
                      help='Additional options (with leading "--" to pass to '+\
                      'ebook-convert (see manual)').tag(config=True)

    @default('nobreak_tags')
    def _nobreak_tags_default(self): return ['h'+str(i) for i in range(3,10)]

    @property
    def default_config(self):
        c = super(EPUBExporter,self).default_config

        return c

    # ----------------------------------------------------------------
    def _assert_modules(self):
        '''Try to load the modules we will need

        - BeautifulSoup
        - cairosvg 
        - Calibre 
        - selenium

        We do this up-front so that we can opt out as soon as possible. 
        It also means we don't need to check later on if we can import 
        these modules.  

        We also check if the chromium and ebook-converter applications 
        can be found where we are told they are. 
        '''
        import sys
        from os import environ
        from pathlib import Path

        
        try:
            from bs4 import BeautifulSoup
        except ModuleNotFoundError as e:
            raise RuntimeError('BeautifulSoup not found, cannot parse HTML'
                               ) from e

        try:
            from selenium import webdriver
            from selenium.common.exceptions import WebDriverException
            from selenium.webdriver.common.by import By
            from selenium.webdriver.support.wait import WebDriverWait
            from selenium.webdriver.support import expected_conditions as EC
        except ModuleNotFoundError as e:
            raise RuntimeError('selenium not found, cannot render HTML'
                               ) from e
        
        try:
            from cairosvg import svg2png
        except ModuleNotFoundError as e:
            raise RuntimeError('cairo is not installed, cannot make PNGs'
                               ) from e 

        try:
            cal     = 'calibre'
            calexec = Path(self.converter_path)
            calprfx = calexec.parents[1]
            callib = calprfx / 'lib' / cal

            if str(callib) not in sys.path:
                sys.path.append(str(callib))
            
            # Figure out the calibre paths 
            cal     = 'calibre'
            calexec = Path(self.converter_path)
            calprfx = calexec.parents[1]
            callib  = calprfx / 'lib' / cal
            
            # Is this OK? We're modifyin a lot here
            sys.resources_location  = environ.get('CALIBRE_RESOURCES_PATH',
                                                  str(calprfx / 'share' / cal))
            sys.extensions_location = environ.get('CALIBRE_EXTENSIONS_PATH',
                                                  str(callib / cal / 'plugins'))
            sys.executables_location = environ.get('CALIBRE_EXECUTABLES_PATH',
                                                   str(calprfx / 'bin'))
            sys.system_plugins_location = None

            from calibre.ebooks.conversion.cli import main, abspath
            from calibre.ebooks.conversion.cli import create_option_parser
            from calibre.customize.conversion import OptionRecommendation
            from calibre.ebooks.css_transform_rules import \
                import_rules as import_css, validate_rule as validate_css
            from calibre.ebooks.html_transform_rules import \
                import_rules as import_html, validate_rule as validate_html
        except ModuleNotFoundError as e:
            raise RuntimeError('calibre is not installed, cannot make EPUB'
                               ) from e

        if not Path(self.chromium_path).exists():
            raise RuntimeError(f'Chromium {self.chromium_path} '
            'does not exist')

        if not Path(self.converter_path).exists():
            raise RuntimeError(f'Calibre converter {self.converter_path} '
                               'does not exist')
        
        
    
    # ----------------------------------------------------------------
    def from_notebook_node(self,nb,resources=None,**kw):
        '''Convert notebook from a node to EPUB
        
        The first thing we do is to make sure we can load all needed
        module.  This is so we may fail early.

        Next, we use the parent class' conversion to get an HTML
        document.  This we write to a temporary file which we then
        open with a headless browser.

        This will return a complete page where all MathJax equations
        are rendered into SVG.

        We then massage that returned HTML document to place nice with
        the rest of the chain.

        Finally, we convert this new HTML document using Calibre. 

        '''
        self._assert_modules()
        
        from os import unlink
        
        self.log.setLevel('DEBUG')
        
        from tempfile import NamedTemporaryFile
        
        #        
        self.log.info('Creating HTML input')
        html, resources = super(EPUBExporter,self).from_notebook_node(
            nb, resources=resources, **kw)

        # Fix up initial HTML for MathJax 
        html = self.fix_mathjax(self.parse(html))
        
        self.log.info('Getting rendered HTML')
        
        # Open temporary file and write HTML to it 
        tmp_file = NamedTemporaryFile(suffix='.html',dir='.',delete=False)
        with tmp_file:
            tmp_file.write(html.encode('utf-8'))

        self.log.info(f'Initial HTML temporary file {tmp_file.name}')

        # Try to render 
        try:
            doc = self.render(tmp_file.name)
        finally:
            if not self.keep_temporary:
                unlink(tmp_file.name)


        # Parse and clean the rendered HTML 
        epub = self.convert(self.clean(self.parse(doc)),nb)
        
        # We now have a clean HTML page. 

        self.log.info('EPUB created')

        resources['output_extension'] = '.epub'

        return epub, resources

    # ----------------------------------------------------------------
    # Initial fix-up of MathJax for SVG output
    def fix_mathjax(self,doc):
        '''We change the load options on MathJax, since we use the regular
        HTML template which sets to render dynamically.  we need SVG
        rendering for the static pages in an EPub.

        We also instructs MathJax to make an (invisible) tag once the
        rendering is done.  We use this to make selenium wait until
        this element is present, and thus we have a complete page we
        can grap.
        '''
        from re import sub
        self.log.info('Setting up MathJax in converted HTML')
        
        for s in doc.find_all('script'): 
            if s.has_attr('src') and 'mathjax' in s['src']: 
                # s.extract()

                # Set the source to contain SVG option We need this,
                # because EPUB does not support MathJax (or any
                # javascript for that matter)
                s['src'] = sub(r'\?config=.*','?config=TeX-AMS_SVG,Safe',
                               s['src'])
                # Fix up invalid "file://<dir>" URL
                s['src'] = sub(r'file://+','file:/',s['src'])

            elif s.has_attr('type') and 'mathjax-config' in s['type']:
                # Fix-up MathJax set-up.  This make MathJax inject a
                # <div> element with id 'MathJaxEnd' after rendering
                # the math in the page.
                to_add = r'''
                \1
                /* Create element at end of MathJax rendering */
                MathJax.Hub.Queue(function () {{
                  console.log('Adding {mathjax_end} element')
                  window.MathJax.HTML.addElement(document.body,"div",
				                 {{id: '{mathjax_end}',
				                  style: {{
                                                    'display': 'none'}} }},
		                                  ["MathJax Finished"]);
                  console.log('Added {mathjax_end} element')
                }});'''.format(mathjax_end=self.mathjax_end)

                patt = r'(MathJax.Hub.Queue\(.*\);)'

                s.string = sub(patt,to_add,s.string)

        return doc
        
    # ----------------------------------------------------------------
    # Parsing 
    def parse(self,src):
        """Parse the retrieved source into DOM using BeautifulSoup"""
        from bs4 import BeautifulSoup

        return BeautifulSoup(src, features="lxml")
    
    def parse_length(self,value, def_units='px'):
        """Parses value as SVG length and returns it in pixels, or a
        negative scale (-1 = 100%)."""
        import re
    
        if not value:
            return 0.0

        parts = re.match(r'^\s*(-?\d+(?:\.\d+)?)\s*(px|in|cm|mm|pt|pc|ex|%)?',
                         value)
        if not parts:
            raise Exception('Unknown length format: "{}"'.format(value))

        num = float(parts.group(1))
        units = parts.group(2) or def_units
        if   units == 'px':        return num
        elif units == 'pt':        return num * 1.25
        elif units == 'pc':        return num * 15.0
        elif units == 'in':        return num * 90.0
        elif units == 'mm':        return num * 3.543307
        elif units == 'cm':        return num * 35.43307
        elif units == '%':         return -num / 100.0
        elif units == 'ex':        return num * 1.25 * self.ex_in_points
        else:
            raise Exception('Unknown length units: {}'.format(units))
    

    # ----------------------------------------------------------------
    # Render using headless 
    def render(self,filename):
        '''Open headless browser and point to temporary file.  Then wait until
        it finishes rendering. Return the rendered HTML

        '''
        import os
        from time import perf_counter
        from selenium import webdriver
        from selenium.common.exceptions import WebDriverException
        from selenium.webdriver.common.by import By
        from selenium.webdriver.support.wait import WebDriverWait
        from selenium.webdriver.support import expected_conditions as EC
        
        # Start the browser and direct to temporary file
        try :
            options = webdriver.ChromeOptions()
            options.add_argument('headless')
            options.binary_location = self.chromium_path

            with webdriver.Chrome(options=options) as driver:                
                url = 'file:'+os.path.abspath(filename)
                driver.get(url)

                # Wait for 'MathJaxEnd' element
                locator = (By.ID, self.mathjax_end)
                start   = perf_counter()
                WebDriverWait(driver, self.render_timeout, 0.5)\
                    .until(EC.presence_of_element_located(locator))
                end     = perf_counter()
                self.log.info(f'It took MathJax {end-start:.3f} '
                              'seconds to finish')
                print(f'It took MathJax {end-start:.3f} seconds to finish')

                return driver.page_source
        except Exception as e:
            raise RuntimeError(f'Failed to render {filename}') from e

        return None

    
    # ----------------------------------------------------------------
    # Cleaning
    def clean(self,doc):
        """Clean up document"""
        self.remove_mathjax(doc)
        self.remove_mathml (doc)
        self.remove_remote (doc)
        self.remove_toc    (doc)
        self.remove_other  (doc)
        self.remove_fonts  (doc)
        self.fix_href      (doc)
        self.convert_svgs  (doc)
        self.fix_headers   (doc)
        self.fix_css       (doc)
        
        return doc

    def remove_mathjax(self,doc):
        """Remove script tags that loads MathJax, or configure mathjax"""
        for s in doc.find_all('script'): 
            if s.has_attr('src') and 'mathjax' in s['src']: 
                s.extract()
            elif s.has_attr('type') and 'mathjax' in s['type']: 
                s.extract()
            elif s.string is not None and 'MathJax' in s.string:
                s.extract()
                
        return doc


    def remove_remote(self,doc):
        """Remove all script reference to external JS"""
        for s in doc.find_all('script'): 
            if s.has_attr('src') and 'http' in s['src']: 
                s.extract()


    def remove_other(self,doc):
        '''Remove link to 'custom.css' style sheet and 
        tags with attribute 'ontoggle' '''
        for s in doc.find_all('link',href='custom.css'):
            s.extract()

        for s in doc.find_all(lambda tag: tag.has_attr('ontoggle')):
            del s['ontoggle']


    def remove_mathml(self,doc):
        """Remove MathML equations (previews)"""
        import re
        
        ass_re = re.compile('MJX_Assistive_MathML')

        for s in doc.find_all('span','MathJax_SVG'): 
            # Remove MathML preview code 
            if s.has_attr('data-mathml'):
                del s['data-mathml']

                # Remove assistive MathML code 
                for c in s.find_all('span',ass_re):
                    c.extract()


    def remove_fonts(self,doc):
        '''Remove font references from <style> tags

        This doesn't work with EPubs 
        '''
        import re

        pat = re.compile('@font-face[ \\t]*{[^}]*}') 
  
        for s in doc.find_all('style',text=pat): 
            bla = pat.sub('/* Removed @font-face */',s.string) 
            s.string = bla 
                
        return doc


    def remove_toc(self,doc):
        '''Remove stuff from 'toc2' extension

        The EPub will generate the ToC
        '''
        import re

        for s in doc.find_all('link',href=re.compile('http')):
            s.extract()

        for s in doc.find_all('script'):
            st = s.string
            if st is None: continue 
            if 'toc2' in st or (s.has_attr('src') and
                                      'toc2.js' in s['src']):
                s.extract()

        for s in doc.find_all('div',id='toc-wrapper'):
            s.extract()

        c = doc.find('div',id='notebook-container')
        if c is not None:
            c['style'] = ''

        return doc

    
    def fix_href(self,doc):
        '''Possibly lower-case 'href' in 'a' link anchors

        This is, if we have '<a href="#FOO">' make it into '<a
        href="#foo">'.  This is because conversion lowercases all IDs

        '''
        if not self.lower_case_ids:
            return doc
        
        import re
        for s in doc.find_all('a',href=re.compile('#[A-Z].*')):
            s['href'] = s['href'].lower()

        return doc
                              

    def convert_svgs(self,doc):
        """Convert embedded SVG to images
        
        For every SVG image, we find all <defs> (glyphs) 
        and store those.  

        We then loop over the SVGs again and clean them.  That entails
        adding an initial <defs> tag and moving all found glyphs
        there.  If cleaning like that results in a null image, we skip
        that SVG.

        Once we have done that, we can create a proper image tag (see
        create_img).

        """
        from pprint import pprint
        
        # Find the SVGs defs (contains all glyphs)
        defs = []
        for svg in doc.find_all('svg'):
            for df in svg.find_all('defs'):
                defs.append(df)
                df.extract()

        defs.reverse()
        
        # Insert defs into SVGs and make image 
        for svg in doc.find_all('svg'):
            if len(svg.find_all('defs')) > 0:
                continue

            cl = self.clean_svg(svg,defs,doc)
            if not cl:
                svg.extract()
                continue

            img = self.create_img(doc,svg,cl[1],cl[2])
            svg.replace_with(img)

        return doc

    def clean_svg(self,svg,defs,doc):
        """Fix up with and height of SVG

        first we create a <defs> tag in the beginning of the SVG.  We
        will add glyphs to this tag.

        Then we find all <use> tags in the SVG (references to glyphs).
        We see if we can find the corresponding glyph in our previous
        generated list.  If we do, then we add it to our <defs>.  If
        we do not find it, it is an error and we raise an exception.

        Next, we check for the width and height of the SVG. If we find
        none, then we cannot work with this SVG and we return None.
        That means the SVG will disappear from the output. 

        We set 'viewBox' if the SVG specifies a view port as 'viewbox'
        (rather than 'viewBox').  We then try to find the aspect ratio
        (height over width) of the SVG by parsing the view box. 

        We parse out the length and adjust so that the aspect ratio is
        preserved.

        Finally, we fix up some bad URL escapes in links in the SVG.

        """
        import re
        import copy
        
        sd = doc.new_tag('defs')
        svg.insert(0,sd)

        for u in svg.find_all('use'):
            # See if we have a reference for this <use>
            xr = u['xlink:href'][1:]

            # If we already have this in out new <defs>,
            # just go on. 
            if sd.find(id=xr):
                continue

            # Next, loop over all the <defs> we got earlier and see if
            # any of those had this reference.
            p = None
            for dd in defs:
                p = dd.find(id=xr)

                # If we found the glyph, break out 
                if p is not None:
                    break

            # If we did not find the end-point of the reference, it is
            # an error and we raise an exception.
            if p is None:
                raise ValueError(f'Failed to find id {xr}')

            # Otherwise we make a copy of the glyph and add it to the
            # new <defs>
            cp = copy.copy(p)
            sd.append(cp)

        # IF we have no dimensions, give up on this 
        if not (svg.has_attr('width') and svg.has_attr('height')):
            return None


        # Makre sure we use 'viewBox' and not 'viewbox'
        if svg.has_attr('viewbox') and not svg.has_attr('viewBox'):
            svg['viewBox'] = svg['viewbox']
            del svg['viewbox']

        # Find aspect ratio, width, height, and normalize the
        # dimensions observing the given aspect ratio.
        vb      = re.split('[ ,\t]+',svg.get('viewBox','').strip())
        aspect  = float(vb[3]) / float(vb[2])  # Height over width
        ow      = svg['width']
        oh      = svg['height']
        width   = self.parse_length(ow)
        height  = self.parse_length(oh)
        svg['width'] = f'{width:.1f}px'  
        svg['height'] = f'{width*aspect:.1f}px'

        # for s in svg.find_all(id=re.compile(':')):
        #     s['id'] = s['id'].replace(':','_')

        # Fix up bad encoding in links 
        for s in svg.find_all('a',{'xlink:href':re.compile('%3A')}):
            s['xlink:href'] = s['xlink:href'].replace('%3A',':')
            
        return svg, ow, oh


    def create_img(self,doc,svg,w,h):
        """Create img tag to replace SVG tag

        """
        from base64 import b64encode as encode

        # Create the tag 
        img   = doc.new_tag('img');

        # Copy attributes
        docpy = ['style']
        for a,v in svg.attrs.items():
            if a in docpy:
                img[a] = v

        # We take the width and heights deduced earlier 
        img['style'] = ';'.join(img['style'].split(';')
                                + [ f'max-width: {w}',
                                    f'max-height: {h}',
                                    f'width: {svg["width"]}',
                                    f'height: {svg["height"]}'])

        # Get LaTeX code and add as alternative text
        pid = svg.parent['id']
        if pid is not None:
            scr = doc.find('script',id=pid.replace('-Frame',''))
            if scr is not None:
                img['alt'] = scr.string
                scr.extract()

        # Find references in the SVG. 
        a    = None
        svga = svg.find(lambda t:t.name == 'a' and t.has_attr('xlink:href'))
        if svga is not None:
            ref = svga['xlink:href']
            svga.unwrap()
            a   = doc.new_tag('a',href=ref)
            a.append(img)

        # Find all 'g' tags with an ID and move the ID to the parent
        # of the SVG.
        svgi = svg.find(lambda t: t.name == 'g' and t.has_attr('id'))
        if svgi is not None:
            i = svgi['id']
            del svgi['id']
            svg.parent['id'] = i

        if self.svg_as_png:
            # Convert the SVG into PNG, and create binary (base64)
            # encoding for inline embedding.
            png        = self.to_png(svg)
            bpng       = encode(png).decode()
            img['src'] = f'data:image/png;base64,{bpng}'
        else:
            # Otherwise, we clean up some spurious attributes,
            # and do a direct inline SVG 
            torm = ['aria-hidden','focusable','role','style']
            for a in torm:
                if svg.has_attr(a):
                    del svg[a]

            svg['xmlns:svg'] = "http://www.w3.org/2000/svg"
            svg['xmlns']     = "http://www.w3.org/2000/svg"
            src  = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n'+\
                str(svg)
            bsvg       = encode(src.encode()).decode()
            img['src'] = f'data:image/svg+xml;base64,{bsvg}'

        # If we found an anchor reference then that is the tag to
        # return
        if a is not None:
            img = a

        return img


    def to_png(self,svg):
        """Converts SVG to PNG using CairoSVG"""
        from cairosvg import svg2png

        return svg2png(str(svg).replace('currentColor',"#000000ff"))


    
    def fix_headers(self,doc):
        '''All headers with immediate '<a class="tocSkip"></a>' tags 
        get the class "nobreak"  That means that we can make 
        title pages and such using Markdown headers and 
        _not_ get page breaks.  For example 


            # My title <a class='tocSkip'></a>
            ## My name <a class='tocSkip'></a>
            > *Abstract* 
            > bla bla bla 

            # Introduction 

        will result in a page break before 'Introduction' but 
        _not_ at 'My title' nor at 'My name'

        This also plays nice with the 'toc2' extension 
        '''
        from re import compile
        hdrs = compile('^h[1-6]$')
        
        for hdr in doc.find_all(hdrs):
            ts = False
            for aaa in hdr.findChildren('a'):
                if 'tocSkip' in aaa.get('class',''):
                    ts = True
                    break
            
            if ts:
                # print(f'Addding no-break to {hdr}')
                hdr['class'] = hdr.get('class',[])+['nobreak']

        for hdr in doc.find_all(lambda t:t.name in self.nobreak_tags):
            hdr['class'] = hdr.get('class',[])+['nobreak']
            
        return doc
        
    def fix_css(self,doc):
        '''Extract all <style> tag content'''
        css = ''
        par = None
        for sty in doc.find_all('style'):
            if par is None:
                par = sty.parent
                
            css += '\n'+sty.string
            sty.extract()

        from re import findall

        # Find variable definitions and store in dictionary 
        matches = findall(r'(--[-a-zA-Z0-9]+):([^;]+)',css)
        db      = {m[0]: m[1] for m in matches}

        # Blacklist some variables we should not expand (mainly images)
        bl = [k for k in db.keys() if k.startswith('--jp-icon-')]
        for k in bl:
            if k in db:
                del db[k]
        
        # Now loop and replace variable calls in each step
        # We need to do this because attributes may be set using
        # recursive variables 
        tmp     = css
        step    = 1
        maxstep = 10
        changed = True
        while changed  and step < maxstep:
            step += 1
            
            changed = False
            for name, value in db.items():
                tmpp = tmp.replace(f'var({name})',value)

                if tmpp != tmp:
                    changed = True
                    tmp     = tmpp

        if step >= maxstep:
            self.log.error(f'Replacing CSS variables not finished '
                           f'in ${step} steps')

        # Fix up various CSS settings to not meddle with EPUB 
        tmp += '\n.jp-Notebook { overflow: unset; }\n'
        tmp += '@media (min-width: 768px) { .container { width: initial; }}\n'
        tmp += '@media (min-width: 992px) { .container { width: initial; }}\n'
        tmp += '@media (min-width: 1200px) { .container { width: initial; }}\n'
        tmp += 'html { font-size: 12px; }\n'
        tmp += '''
        body { overflow:  initial;
               position:  initial;
               top:       initial;
               left:      initial;
               right:     initial;
               bottom:    initial;
               font-size: unset; }'''
        
        sty = doc.new_tag('style',type='text/css')
        sty.string = tmp
        par.append(sty)

        return doc

    # ----------------------------------------------------------------
    # Convert to EPUB
    def convert(self,doc,nb):
        from tempfile import NamedTemporaryFile
        from os import unlink
        from pprint import pprint


        inp_file = NamedTemporaryFile(suffix='.xhtml',dir='.',delete=False)
        inp_name = inp_file.name
        with inp_file:
            inp_file.write(doc.encode('utf-8'))

        self.log.info(f'Second HTML temporary file {inp_file.name}')
        
        out_file = NamedTemporaryFile(suffix='.epub',dir='.',delete=False)
        out_name = out_file.name
        with out_file:  # Close the file 
            pass 
            
        self.log.info(f'EPUB HTML temporary file {out_file.name}')


        kw = {'--embed-all-fonts'       : '',
              '--change-justification'  : 'justify',
              '--chapter'               : '//h:h1[not(@class="nobreak")]',
              '--page-breaks-before'    : '//h:h1[not(@class="nobreak")]',
              '--level1-toc'            : '//h:h1',
              '--level2-toc'            : '//h:h2[not(@class="nobreak")]',
              '--toc-threshold'         : '0',
              '--use-auto-toc'          : '', 
              '--epub-version'          : str(self.epub_version),
              '--filter-css'            : 'direction',
              '--output-profile'        : self.output_profile,
              '--book-producer'         : 'NBConvert',
              '--duplicate-links-in-toc': '',
              '--input-encoding'        : 'utf-8',
              }

        # Take title and data from notebook meta data - if set
        # This is how the LaTeX template behaves 
        meta = nb.get('metadata',{})
        if 'title' in meta:  kw['--title'] = meta['title']
        if 'authors' in meta:
            kw['--authors'] = '&'.join(a['name'] for a in meta['authors'])

        # For each of the special configurations, possibly set the
        # corresponding option, if the value isn't the null string.
        for k,v in zip(['title',       'authors',   'comments',
                        'publisher',   'series',    'isbn',
                        'language',    'pubdate',   'cover'],
                       [self.title,    self.authors,self.comments,
                        self.publisher,self.series, self.isbn,
                        self.language, self.pubdate,self.cover]):
            if v != '':
                kw[f'--{k}'] = v 

        # The configuration 'extra_args' can be used to set any option
        # to calibre's ebook-convert.  This will also override the
        # specific options 
        kw.update({f'--{k}': str(v) for k,v in self.extra_args.items()})

        # Now build argument list 
        args = [self.converter_path, inp_name, out_name]
        for k,v in kw.items():
            args.append(k);
            if v != '':
                args.append(v)

        try:
            # Run calibre ebook-convert - not done, instead we recode it
            # below
            if False:
                main(args)
            else:
                self._convert(args)
        
            # Read in the generated file.  Note, this is a zip file -
            # i.e., binary - which is OK, because the writers write in binary
            with open(out_name,'rb') as file:
                epub = file.read()

        finally:
            # Delete the temporary files no matter what 
            if not self.keep_temporary:
                unlink(inp_name)
                unlink(out_name)
        
        return epub


    def _convert(self,args):
        '''Actually do the conversion'''
        from pathlib import Path
        import sys


        # We need to import quite a lot here, since we want to recode the 'main'
        # of 'ebook-convert'.  We recode mainly to silence the quite verbose
        # conversion 
        from calibre.ebooks.conversion.cli import main, abspath
        from calibre.ebooks.conversion.cli import create_option_parser
        from calibre.customize.conversion import OptionRecommendation
        from calibre.ebooks.css_transform_rules import \
            import_rules as import_css, validate_rule as validate_css
        from calibre.ebooks.html_transform_rules import \
            import_rules as import_html, validate_rule as validate_html

        # Recoding of 'main' above
        # 
        # Wrap our log in a proxy object - sometimes loose
        # typing can save the day :-) 
        class callog:
            def __init__(self,logger):
                self._logger = logger

            def __call__(self,*args,**kwargs):
                self._logger.info(*args,**kwargs)

            def warn(self,*args,**kwargs):
                self._logger.warn(*args,**kwargs)

            def error(self,*args,**kwargs):
                self._logger.error(*args,**kwargs)

            def info(self,*args,**kwargs):
                self._logger.info(*args,**kwargs)
                
            def debug(self,*args,**kwargs):
                self._logger.debug(*args,**kwargs)
                
        # Make our special logger 
        log = callog(self.log) # Log()

        # Create the options and 'Plumber' which sets up
        # the Calibre pipe-line 
        parser, plumber = create_option_parser(args, log)
        opts, leftover_args = parser.parse_args(args)

        # If options are left after parsing, it means
        # we got something unexpected.  Bark
        if len(leftover_args) > 3:
            raise RuntimeError('Extra arguments not understood: '+
                               ', '.join(leftover_args[3:]))

        # OPF specifies meta data such as title, authors, etc. 
        for x in ('read_metadata_from_opf', 'cover'):
            if getattr(opts, x, None) is not None:
                setattr(opts, x, abspath(getattr(opts, x)))

        # Calibre has some options for search and replace in the source.
        # We allow that here, but doubt it will be used 
        if opts.search_replace:
            opts.search_replace = read_sr_patterns(opts.search_replace, log)

        # We can in principle also transform CSS rules via a file
        # We already do a bit of this here, so again probably
        # not widely used 
        if opts.transform_css_rules:
            with open(opts.transform_css_rules, 'rb') as tcr:
                opts.transform_css_rules = rules = \
                    list(import_css(tcr.read()))
                for rule in rules:
                    title, msg = validate_css(rule)
                    if title and msg:
                        raise RuntimeError(
                            'Failed to parse CSS transform rules: '
                            f'{title}: {msg}')

        # We can in principle also transform HTML tags via a file.
        # Again probably not widely used
        if opts.transform_html_rules:
            with open(opts.transform_html_rules, 'rb') as tcr:
                opts.transform_html_rules = rules = \
                    list(import_html(tcr.read()))
                for rule in rules:
                    title, msg = validate_html(rule)
                    if title and msg:
                        raise RuntimeError(
                            'Failed to parse CSS transform rules: '
                            f'{title}: {msg}')


        # Get the options (recommendations in Calibre's lingo) and
        # merge in to the pipeline.  I think the idea is this is how
        # the pipeline is made aware of exporter etc. options.
        recommendations = [(n.dest, getattr(opts, n.dest),
                            OptionRecommendation.HIGH)
                           for n in parser.options_iter()
                           if n.dest]
        plumber.merge_ui_recommendations(recommendations)

        # Now run ebook-convert.  This will raise exceptions on
        # errors.
        plumber.run()
#
# EOF
#


        
            
                                                  
            
            

           
        

        
