from nbconvert.preprocessors import Preprocessor
from traitlets import Unicode
import nbformat


# ====================================================================
class Tags2Classes(Preprocessor):
    def __init__(self,**kw):
        super(Tags2Classes,self).__init__(**kw)

    def makeCss(self):
        css = {'cell_type': 'code',
               'metadata': {},
               'execution_count': None,
               'outputs': [
                   { 'data': { 
                       'text/html': [
                           "<style>alert { border: thin solid red; }</style>" ]
                       },
                     'output_type': 'display_data',
                     'metadata': {}
                   }],
               'source': [
                   '%%HTML\n',
                   '<style>\n',
                   '  alert { border: solid thin red; }\n',
                   '</style>'
                   ]}
        return nbformat.from_dict(css)

    def makeCssSimple(self):
        return nbformat.v4.new_code_cell('%%HTML\n'
                                          '<style>\n'
                                          'alert {\n '
                                          '  border: thin solid red;\n'
                                          '}\n'
                                          '</style>')
        
    def preprocess(self,notebook,resource):
        """
        Preprocessing to apply on each notebook.

        Must return modified nb, resources.

        Parameters
        ----------
        nb : NotebookNode
            Notebook being converted
        resources : dictionary
            Additional resources used in the conversion process.  Allows
            preprocessors to pass variables into the Jinja engine.
        """
        if resource['output_extension'] != '.ipynb':
            return notebook,resource

        nb,res = super(Tags2Classes,self).preprocess(notebook,resource)

        # nb.cells = [self.makeCss()] + nb.cells
        return nb,res 

    def preprocess_cell(self, cell, resource, index):
        """
        Preprocess cell
    
        Parameters
        ----------
        cell : NotebookNode cell
            Notebook cell being processed
        resources : dictionary
            Additional resources used in the conversion process.  Allows
            preprocessors to pass variables into the Jinja engine.
        cell_index : int
            Index of the cell being processed (see base.py)
        """
        if cell.cell_type != 'markdown':
            return cell, resource

        clss = cell.get('metadata',{}).get('tags', []) + \
            cell.get('metadata',{}).get('classes',[])
        if clss is None or len(clss) == 0:
            return cell,resource

        if isinstance(clss,list):
            clss = ' '.join(clss)

        cell.source = "<div class='{}'>\n{}\n</div>"\
            .format(clss.replace(',',' '), cell.source)
        return cell,resource

# ====================================================================
class SetLanguage(Preprocessor):
    language = Unicode('en',
                       help="Selected language").tag(config=True,alias='lang')
    
    def __init__(self,**kw):
        '''Set language in notebook meta-data'''
        super(SetLanguage,self).__init__(**kw)

        
    def preprocess(self,notebook,resource):
        """
        Preprocessing to apply on each notebook.

        Must return modified nb, resources.

        Parameters
        ----------
        nb : NotebookNode
            Notebook being converted
        resources : dictionary
            Additional resources used in the conversion process.  Allows
            preprocessors to pass variables into the Jinja engine.
        """
        lang = SetLanguage.language.get(self)
        # print(f'Set language={lang}')
        # dir(SetLanguage.language)
        notebook.get('metadata',{}).get('nbTranslate',{})['transLang']=lang

        return notebook,resource

# ====================================================================
class LanguagePreprocessor(Preprocessor):
    language = Unicode(
        'en', help="Selected language").tag(config=True, alias="lang")

    def __init__(self,**kw):
        '''Select only markdown cells that match the select language
        or does not have a specific language''' 
        super(LanguagePreprocessor,self).__init__(**kw)

    def preprocess(self, nb, resources):
        """
        Preprocessing to apply on each notebook.

        Must return modified nb, resources.

        Parameters
        ----------
        nb : NotebookNode
            Notebook being converted
        resources : dictionary
            Additional resources used in the conversion process.  Allows
            preprocessors to pass variables into the Jinja engine.
        """
        self.lang = LanguagePreprocessor.language.get(self)
        # print(f'Selected language {self.lang}')
    
        filtered_cells = []
        for cell in nb.cells:
            if cell.cell_type == 'markdown':
                cell_lang = cell.get('metadata', {}).get('lang', None)
                # print(f'Cell language: {cell_lang} ({self.lang}) ',end='')
                    
                if not cell_lang or cell_lang == self.lang: # noqa
                    # print('Adding',end='')
                    filtered_cells.append(cell)

                # print('')
            else:
                filtered_cells.append(cell)

        nb.cells = filtered_cells
        return super(LanguagePreprocessor, self).preprocess(nb, resources)

    def preprocess_cell(self, cell, resources, index):
        """
        Preprocess cell
    
        Parameters
        ----------
        cell : NotebookNode cell
            Notebook cell being processed
        resources : dictionary
            Additional resources used in the conversion process.  Allows
            preprocessors to pass variables into the Jinja engine.
        cell_index : int
            Index of the cell being processed (see base.py)
        """
    
        return cell, resources
    
#
# EOF
#
