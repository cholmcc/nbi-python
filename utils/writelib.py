#!/usr/bin/env python3
# -*- encoding: utf-8 -*- 

def writelib(input,output,meta,init,authors,year):
    import json
    from datetime import datetime
    from textwrap import dedent

    if not isinstance(authors,list):
        authors = [authors]

    other = json.load(input)

    out = [dedent(f'''        # Copyright {year} {" & ".join(authors)} 
        #
        # This code is distributed under the GNU General Public License 
        # version 3 or any later version 
        # 
        # Generated {datetime.utcnow()} UTC
        ''')]

    for l in init:
        out.append('from {} import *'.format(l))
        
    idx = 0
    for c in other['cells']:
        if c['cell_type'] != "code":
            continue
        pyf = c['metadata'].get('pyfile',False)
        if not pyf or pyf != meta:
            continue
        out.append("# -------------------------------------------------")
        out.append("".join(c['source']))
        out.append("")

    out.append(f'if __doc__ is not None:\n')
    out.append(f'    __doc__ += \\\n"""\n{datetime.utcnow()} UTC\n"""\n')
    out.append(dedent("""	#
	# EOF
	#
	"""))
    output.write("\n".join(out))
    
    

if __name__ == "__main__":
    from argparse import ArgumentParser as ArgParse
    from argparse import ArgumentDefaultsHelpFormatter as Formatter
    from argparse import FileType
    import sys
    try:
        cfg = get_ipython().config 
        sys.argv = ['Statistik', 'Statistik.ipynb']
    except:
        pass
    
    ap = ArgParse(description='Export code cells marked as lib to module',
                  formatter_class=Formatter)                  
    ap.add_argument('-o', '--output',
                    type=FileType('w'),
                    default='nbi_stat.py',
                    nargs='?',
                    help='File to write output to')
    ap.add_argument('-t', '--tag',
                    type=str,
                    default='lib',
                    nargs='?',
                    help='Cell meta value to look for and test')
    ap.add_argument('-i', '--packages',
                    type=str,
                    default=[],
                    nargs='*',
                    help='Packages to fully import')
    ap.add_argument('-a','--author',
                    type=str,
                    default='Christian Holm Christensen',
                    nargs='*',
                    help='Author(s)')
    ap.add_argument('input',
                     type=FileType('r'),
                    default='Statistik.ipynb',
                    help='Input file name')
    ap.add_argument('-y','--year',
                    type=int,
                    default=2018,
                    help='Copyright year')
    args = ap.parse_args()

    writelib(args.input,args.output,args.tag,args.packages,args.author,args.year)

#
# EOF
#
