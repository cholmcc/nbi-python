#!/bin/bash

# /\\\\maketitle/,/\\\\renewcommand\\\\contentsname/ {
#   /^[[:space:]]*\\\\maketitle$/d; 
#   s/\(.*Overblik.*\|.*Bedste.*\|.*Overview.*\|.*Best.*\)/\\\\title{\1}/; 
#   s/^\(Med Python\|With Python\)/\\\\subtitle{\1}/; 
#   s/\(Version.*\)/\\\\date{\\1}\\\\maketitle\\\\vfill\\\\begin{abstract}/; 
#   s/Niels.*/\\\\end{abstract}\\\\vfill\\\\par/;
#   s/\(Christian.*\)/\\\\author{\1}\\\\publisher{Niels Bohr Institutet}/; 
#   s/~\{3,\}\(.*\)~*/\1/g;
#   s/!.*py.png.*\./\\\\includegraphics[height=2ex]{imgs\/py.png}./}

sc=`mktemp -p . -u` 
cat > ${sc} <<EOF
s/{attachment:py.png}/[height=1.5ex]{imgs\/py}/g
s/{attachment:\([^}]*\)\.[^.]*}/{imgs\/\1}/g
s/Section *\\\\ref/\\\\sectionname~\\\\ref/g
s/\\\\ref{numeriske-type}/\\\\ref{numeriske-typer}/
s/\\\\ref{numeric-type}/\\\\ref{numeric-types}/
s/\\\\ref{boolsk-typer}/\\\\ref{boolsk-type}/
s/\\\\ref{boolean-types}/\\\\ref{boolean-type}/
s/\\\\ref{boolske-typer}/\\\\ref{boolsk-type}/
s/\\\\ref{\([a-z0-9-]*\)æ\([a-z0-9-]*\)}/\\\\ref{\1uxe6\2}/
s/\\\\ref{\([a-z0-9-]*\)ø\([a-z0-9-]*\)}/\\\\ref{\1uxf8\2}/
s/\\\\ref{\([a-z0-9-]*\)å\([a-z0-9-]*\)}/\\\\ref{\1uxe5\2}/
s/\\\\ref{eksempel-chi5e2-fordelingen}/\\\\ref{eksempel-chi2-fordelingen}/
s/\\\\ref{example-chi5e2-distribution}/\\\\ref{example-chi2-distribution}/
EOF
# s/Â//g
# s/µ/mu/g
# s/±/+\-/g
# 

sed -f ${sc}

rm -f ${sc}

