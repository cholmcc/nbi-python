#!/usr/bin/env python
# coding: utf-8

def concrete_expand_log(expr, first_call=True,**kwargs):
    """Force expansion of `log(Product(...))"""
    from sympy import __version__ as syvers
    try:
        if float(syvers) >= 1.5:
            return expr.expand(log=True,force=True,**kwargs)
    except:
        pass
    
    from sympy.concrete.products import Product
    from sympy.concrete.summations import Sum
    from sympy.functions.elementary.exponential import log

    if first_call:
        expr = expr.expand(log=True,**kwargs)
    if expr.args is ():
        return expr
    if expr.func is log and expr.args[0].func is Product:
            return Sum(log(expr.args[0].args[0]), *expr.args[0].args[1:])
    return expr.func(*map(lambda x:concrete_expand_log(x, False,**kwargs),
                          expr.args))


#
# EOF
#
