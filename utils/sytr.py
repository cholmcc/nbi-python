#!/usr/bin/env python
# coding: utf-8

class SyTreePrint:
    """Class to print the expression tree of a SymPy expression

    This will print the full expression tree with indention outlining
    the structure of the expression.  Named expression will be printed
    with its name leading, then the class name follows, and then the
    regular string representation.
    """
    def __init__(self):
        self._i = ''
        
    def _incr(self):
        """Increment indention"""
        self._i += ' '
        
    def _decr(self):
        """Decrement indention"""
        self._i = self._i[:-1]
        
    def _pr(self,what,end='\n'):
        """Print string with indention 

        Parameters 
        ----------
        what : str 
            What to print 
        end : str
            Ending to print.  Typically a new line or the empty string 
        """
        return '{:s}{}{}'.format(self._i,what,end)
        
    def _tr(self,expr):
        """Traverse an expression 
        
        Parameters
        ----------
        expr : sympy.Basic 
            Expression to traverse 

        Returns
        -------
        out : str 
            The string representing the expression tree 
        """
        from sympy import Symbol
        
        txt = []
        if hasattr(type(expr),"__name__"):
            txt += [type(expr).__name__]
        if hasattr(expr,'name'):
            txt += [expr.name]
        if isinstance(expr,Symbol):
            txt += ['('+str(id(expr))+')']
        txt = ': '.join(txt)
        txt += '=' + str(expr)
            
        out = self._pr(txt)
            
        self._incr()
        for e in expr.args:
            out += self._tr(e)
            
        self._decr()

        return out

    def traverse(self,expr,file=None):
        """Traverse expression and print tree to stream"""
        from sys import stdout
        if file is None:
            file = stdout

        print(self._tr(expr), file=file)
        
def sytr(expr):
    """Traverse and print expression tree"""
    p = SyTreePrint()
    p.traverse(expr)

#
#
#
