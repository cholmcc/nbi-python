# This does not work - IPython inspect get_ipython().ast_node_interactivity 
# at the start of exection of a cell - not the end.  
#
# That is, setting ast_node_interactivity will always only effect 
# _subsequent_ cells - not the current cell - sigh!
#
class MultiOutput:
    _old = None
    _dbg = False
    
    def __init__(self,tgt='all',dbg=False): 
        self._tgt = tgt 
        MultiOutput._old = MultiOutput._get()
        MultiOutput._dbg = dbg

    @classmethod 
    def _ip(cls):
        from IPython import get_ipython
        return get_ipython()
    
    @classmethod
    def _msg(cls,func,*args):
        if not cls._dbg:
            return 
        
        print(cls.__name__ + "." + func + ':',*args)
        
    @classmethod
    def _get(cls):
        try:
            cls._msg('_get',cls._ip().ast_node_interactivity)
            return cls._ip().ast_node_interactivity
        
        except Exception as e:
            cls._msg('_reset',e)
            return None
        
    @classmethod
    def _set(cls,val):
        try:
            cls._msg('_set',cls._ip().ast_node_interactivity,'->',val)
            cls._ip().ast_node_interactivity = val
            
        except Exception as e:
            cls._msg('_reset',e)
            pass
    
    @classmethod
    def _reset(cls,result=None):
        try:
            cls._msg('_reset',result)
            cls._ip().events.unregister('post_run_cell',cls._reset)
            #cls._set(cls._old)
            
        except Exception as e:
            cls._msg('_reset',e)
            pass    
    
    def __enter__(self):
        MultiOutput._msg('__enter__')
        MultiOutput._old = self._get()
        MultiOutput._set(self._tgt)
        return self
        
    def __exit__(self,exc_type, exc_value, exc_traceback):
        MultiOutput._msg('__exit__')
        # if exc_type is not None:
        # MultiOutput._set(MultiOutput._old)
            
        #try:
        #    MultiOutput._ip().events.register('post_run_cell',MultiOutput._reset)
        #except Exception as e:
        #    cls._msg('__exit__',e)
        #    MultiOutput._set(MultiOutput._old)
            
    
class Dbg:
    def __init__(self):
        pass 
    
    def __enter__(self):
        print('Enter')
        
    def __exit__(self,et,ev,eb):
        print('Exit')

