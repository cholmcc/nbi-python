#!/usr/bin/env python3
# -*- encoding: utf-8 -*- 
import json

def checkCell(c):
    meta = c['metadata']
    tags = meta.get('tags',{})
    return 'nomerge' not in meta and 'nomerge' not in tags


def appendCells(content, out):
    for c in content['cells']:
        if not checkCell(c):
            continue

        out['cells'].append(c)

    return out
    

def fromPart(file, out):
    try:
        content = json.load(file)

        print("Appending cells from {}".format(file.name))
        return appendCells(content, out)
    except Exception as e:
        print(f'Failed to append cells from {file.name}\n{e}')
        raise
        


def fromFront(front):
    content = json.load(front)

    out = {'cells':[]}
    out = appendCells(content, out)
    print("Appending metadata from {}".format(front.name))
    out['metadata'] = content["metadata"]
    out['nbformat'] = content["nbformat"]
    out['nbformat_minor'] = content["nbformat_minor"]

    front.close()
    
    return out

        
def merge(output,files):
    import json, textwrap


    out = fromFront(files[0])
    for file in files[1:]:
        out = fromPart(file,out)
        file.close()

    print("Writing Notebook to {}".format(output.name))
    output.write(json.dumps(out, indent=1, ensure_ascii=False))
    output.close()
    

if __name__ == "__main__":
    import argparse
    import sys
    try:
        cfg = get_ipython().config 
        sys.argv = ['merge', '']
    except:
        pass
    
    ap = argparse.ArgumentParser(description='Merge content')
    ap.add_argument('-o','--output', type=argparse.FileType('w'),
                   default='output.ipynb')
    ap.add_argument('input',default='body.ipynb',
                    type=argparse.FileType('r'),nargs='+')
    args = ap.parse_args()

    merge(args.output,args.input)

