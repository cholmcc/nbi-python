#!/usr/bin/env python3

from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime, timedelta
import os

class SaveAs:
    def __init__(self,ex=5,png=True,timeout=20,lowerids=False):
        self._ex  = ex # One ex in pt 
        self._png = png
        self._to  = timeout
        self._li  = lowerids
        
    def render(self,input,chromium=True):
        """Renders the page using selenium and Chrome"""
        from time import perf_counter
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        if chromium:
            options.binary_location = '/usr/bin/chromium'
    
        #with webdriver.Chrome(options=options) as driver:
        with webdriver.Chrome(options=options) as driver:
            url = 'file:'+os.path.abspath(input)
            driver.get(url)

            # Wait for 'MathJaxEnd' element 
            locator = (By.ID, 'MathJaxEnd')
            start   = perf_counter()
            WebDriverWait(driver, self._to, 0.5)\
                .until(EC.presence_of_element_located(locator))
            end     = perf_counter()
            print(f'It took MathJax {end-start:.3f} seconds to finish')

            return driver.page_source

    def parse(self,src):
        """Parse the retrieved source into DOM using BeautifulSoup"""
        from bs4 import BeautifulSoup

        return BeautifulSoup(src, features="lxml")
    
    def parse_length(self,value, def_units='px'):
        """Parses value as SVG length and returns it in pixels, or a
        negative scale (-1 = 100%)."""
        import re
    
        if not value:
            return 0.0

        parts = re.match(r'^\s*(-?\d+(?:\.\d+)?)\s*(px|in|cm|mm|pt|pc|ex|%)?',
                         value)
        if not parts:
            raise Exception('Unknown length format: "{}"'.format(value))

        num = float(parts.group(1))
        units = parts.group(2) or def_units
        if   units == 'px':        return num
        elif units == 'pt':        return num * 1.25
        elif units == 'pc':        return num * 15.0
        elif units == 'in':        return num * 90.0
        elif units == 'mm':        return num * 3.543307
        elif units == 'cm':        return num * 35.43307
        elif units == '%':         return -num / 100.0
        elif units == 'ex':        return num * 1.25 * self._ex
        else:
            raise Exception('Unknown length units: {}'.format(units))

    def remove_mathjax(self,doc):
        """Remove script tags that loads MathJax, or configure mathjax"""
        for s in doc.find_all('script'): 
            if s.has_attr('src') and 'mathjax' in s['src']: 
                s.extract()
            elif s.has_attr('type') and 'mathjax' in s['type']: 
                s.extract()
            elif s.string is not None and 'MathJax' in s.string:
                s.extract()
                
        return doc

    def remove_remote(self,doc):
        """Remove all script reference to external JS"""
        for s in doc.find_all('script'): 
            if s.has_attr('src') and 'http' in s['src']: 
                s.extract()


    def remove_other(self,doc):
        for s in doc.find_all('link',href='custom.css'):
            s.extract()

        for s in doc.find_all(lambda tag: tag.has_attr('ontoggle')):
            del s['ontoggle']

    def remove_mathml(self,doc):
        """Remove MathML equations (previews)"""
        import re
        
        ass_re = re.compile('MJX_Assistive_MathML')

        for s in doc.find_all('span','MathJax_SVG'): 
            # Remove MathML preview code 
            if s.has_attr('data-mathml'):
                del s['data-mathml']

                # Remove assistive MathML code 
                for c in s.find_all('span',ass_re):
                    c.extract()

    def remove_fonts(self,doc):
        import re

        pat = re.compile('@font-face[ \\t]*{[^}]*}') 
  
        for s in doc.find_all('style',text=pat): 
            bla = pat.sub('/* Removed @font-face */',s.string) 
            s.string = bla 
                
        return doc

    def remove_toc(self,doc):
        import re

        for s in doc.find_all('link',href=re.compile('http')):
            s.extract()

        for s in doc.find_all('script'):
            st = s.string
            if st is None: continue 
            if 'toc2' in st or (s.has_attr('src') and
                                      'toc2.js' in s['src']):
                s.extract()

        for s in doc.find_all('div',id='toc-wrapper'):
            s.extract()

        c = doc.find('div',id='notebook-container')
        if c is not None:
            c['style'] = ''

        return doc

    def fix_href(self,doc):
        if not self._li:
            return doc
        
        import re
        print('Lower case IDs')
        for s in doc.find_all('a',href=re.compile('#[A-Z].*')):
            s['href'] = s['href'].lower()

        return doc
                              

    def clean_svg(self,svg,defs,doc):
        """Fix up with and height of SVG"""
        import re
        import copy
        
        sd = doc.new_tag('defs')
        svg.insert(0,sd)

        for u in svg.find_all('use'):
            xr = u['xlink:href'][1:]

            if sd.find(id=xr):
                continue

            p = None
            for dd in defs:
                p = dd.find(id=xr)
                if p is not None:
                    break

            if p is None:
                raise ValueError(f'Failed to find id {xr}')

            cp = copy.copy(p)
            sd.append(cp)
            
        if not (svg.has_attr('width') and svg.has_attr('height')):
            return None

        
        if svg.has_attr('viewbox') and not svg.has_attr('viewBox'):
            svg['viewBox'] = svg['viewbox']
            del svg['viewbox']
                
        vb      = re.split('[ ,\t]+',svg.get('viewBox','').strip())
        aspect  = float(vb[3]) / float(vb[2])  # Height over width
        ow      = svg['width']
        oh      = svg['height']
        width   = self.parse_length(ow)
        height  = self.parse_length(oh)
        svg['width'] = f'{width:.1f}px'  
        svg['height'] = f'{width*aspect:.1f}px'

        # for s in svg.find_all(id=re.compile(':')):
        #     s['id'] = s['id'].replace(':','_')

        for s in svg.find_all('a',{'xlink:href':re.compile('%3A')}):
            s['xlink:href'] = s['xlink:href'].replace('%3A',':')
            
        return svg, ow, oh

    def to_png(self,svg):
        """Converts SVG to PNG using CairoSVG"""
        from cairosvg import svg2png

        return svg2png(str(svg).replace('currentColor',"#000000ff"))


    def create_img(self,doc,svg,w,h):
        """Create img tag to replace SVG tag"""
        from base64 import b64encode as encode
        
        img   = doc.new_tag('img');

        # Copy attributes
        docpy = ['style']
        for a,v in svg.attrs.items():
            if a in docpy:
                img[a] = v
        img['style'] = ';'.join(img['style'].split(';')
                                + [ f'max-width: {w}',
                                    f'max-height: {h}',
                                    f'width: {svg["width"]}',
                                    f'height: {svg["height"]}'])

        # Get LaTeX code and add as alternative text
        pid = svg.parent['id']
        if pid is not None:
            scr = doc.find('script',id=pid.replace('-Frame',''))
            if scr is not None:
                img['alt'] = scr.string
                scr.extract()

        a    = None
        svga = svg.find(lambda t:t.name == 'a' and t.has_attr('xlink:href'))
        if svga is not None:
            ref = svga['xlink:href']
            svga.unwrap()
            a   = doc.new_tag('a',href=ref)
            a.append(img)

        svgi = svg.find(lambda t: t.name == 'g' and t.has_attr('id'))
        if svgi is not None:
            i = svgi['id']
            del svgi['id']
            svg.parent['id'] = i

        # Create binary (base64) encoding 
        if self._png:
            png        = self.to_png(svg)
            bpng       = encode(png).decode()
            img['src'] = f'data:image/png;base64,{bpng}'
        else:
            torm = ['aria-hidden','focusable','role','style']
            for a in torm:
                if svg.has_attr(a):
                    del svg[a]

            svg['xmlns:svg'] = "http://www.w3.org/2000/svg"
            svg['xmlns']     = "http://www.w3.org/2000/svg"
            src  = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n'+\
                str(svg)
            bsvg       = encode(src.encode()).decode()
            img['src'] = f'data:image/svg+xml;base64,{bsvg}'

        if a is not None:
            img = a

        return img

    def convert_svgs(self,doc):
        """Convert embedded SVG to images"""

        # Find the SVGs defs (contains all glyphs)
        defs = []
        for svg in doc.find_all('svg'):
            for df in svg.find_all('defs'):
                defs.append(df)
                df.extract()

        defs.reverse()
        
        # Insert defs into SVGs 
        for svg in doc.find_all('svg'):
            if len(svg.find_all('defs')) > 0:
                print('Second pass, found defs')
                continue

            cl = self.clean_svg(svg,defs,doc)
            if not cl:
                svg.extract()
                continue

            img = self.create_img(doc,svg,cl[1],cl[2])
            svg.replace_with(img)

        return doc

    def fix_css(self,doc):
        '''Extract all <style> tag content'''
        css = ''
        par = None
        for sty in doc.find_all('style'):
            if par is None:
                par = sty.parent
                
            css += '\n'+sty.string
            sty.extract()

        from re import findall

        # Find variable definitions and store in dictionary 
        matches = findall(r'(--[-a-zA-Z0-9]+):([^;]+)',css)
        db      = {m[0]: m[1] for m in matches}

        # Blacklist some variables we should not expand (mainly images)
        bl = [k for k in db.keys() if k.startswith('--jp-icon-')]
        for k in bl:
            if k in db:
                del db[k]
        
        # Now loop and replace variable calls in each step
        # We need to do this because attributes may be set using
        # recursive variables 
        tmp     = css
        step    = 1
        maxstep = 10
        changed = True
        while changed  and step < maxstep:
            step += 1
            
            changed = False
            for name, value in db.items():
                tmpp = tmp.replace(f'var({name})',value)

                if tmpp != tmp:
                    changed = True
                    tmp     = tmpp

        if step >= maxstep:
            print(f'Replaceing CSS variables not finished in ${step} steps')

        tmp += '\n.jp-Notebook { overflow: unset; }\n'
        
        print(f'Creating new style tag and adding to {par.name}')
        sty = doc.new_tag('style',type='text/css')
        sty.string = tmp
        par.append(sty)

        return doc

    def write(self,doc,out):
        """Write document to disk"""
        print(f'Writing {out}')
        with open(out,'w') as file:
            file.write(str(doc))
        
    def clean(self,doc):
        """Clean up document"""
        import re
        from base64 import b64encode as encode
    
        self.remove_mathjax(doc)
        self.remove_mathml (doc)
        self.remove_remote (doc)
        self.remove_toc    (doc)
        self.remove_other  (doc)
        self.remove_fonts  (doc)
        self.fix_href      (doc)
        self.convert_svgs  (doc)

        return doc

    def run(self,input,output):
        """Run the entire thing"""
        src = self.render(input)
        doc = self.parse(src)
        doc = self.clean(doc)
        doc = self.fix_css(doc);
        self.write(doc,output)
    
if __name__ == "__main__":
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Expand MathJax for EPUB')
    ap.add_argument('input',type=str,default='tests/Test.epub.html',
                    help='Input file')
    ap.add_argument('output',type=str,default='tmp.html',
                    help='Output file')
    ap.add_argument('-x','--ex',type=int,default=7,
                    help='The size of 1ex in points (not pixels)')
    ap.add_argument('-w','--timeout',type=int,default=20,
                    help='Seconds before headless browser timesout')

    tg = ap.add_mutually_exclusive_group(required=False)
    tg.add_argument('-p','--png', action='store_true',
                    help='Use PNG for formulas')
    tg.add_argument('-s','--svg', action='store_false', dest='png',
                    help='Use SVG for formulas')

    ig = ap.add_mutually_exclusive_group(required=False)
    ig.add_argument('-i','--lower-ids',action='store_true',
                    help='Lower case href internal IDs')
    ig.add_argument('-I','--no-lower-ids',action='store_false',dest='lower-ids',
                    help='Do not lower-case IDs internal hrefs')

    ap.set_defaults(png=True,lower_ids=False)
    
    args = ap.parse_args()

    s = SaveAs(args.ex,args.png,args.timeout,args.lower_ids)
    s.run(args.input,args.output)
