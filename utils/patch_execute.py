### BEGIN patch_execute
### Update 30/10-'20
def patch_execute_code():
    html = '''
    <script>
    if (typeof Jupyter == 'undefined') {
        console.log("Cannot patch CodeCell.execute - probably JupyterLab")
    }
    else {
        if (Jupyter.CodeCell.execute_patched == undefined) {
            console.log('Patching CodeCell.execute')
            
            Jupyter.CodeCell.prototype.old_execute = Jupyter.CodeCell.prototype.execute;
            Jupyter.CodeCell.prototype.execute = function (stop_on_error) {
                if (this.metadata !== undefined && 
                    this.metadata.tags !== undefined) {
                    if (this.metadata.tags.indexOf('no-execution') !== -1 ||
                        this.metadata.tags.indexOf('no-execute')   !== -1
                        ) {
                        console.log('Not executing this code cell');
                        return;
                    }
                };
                console.log('Executing as normal');
                this.old_execute(stop_on_error);
            }
            
            console.log('Patched CodeCell.execute');
            
            Jupyter.CodeCell.execute_patched = true;
        }
        else {
            console.log(Jupyter.CodeCell.execute_patched);
        }
    }
    </script>
    '''
    return html

def patch_execute():
    from IPython.display import HTML 
    return HTML(hide_toggle_code()+patch_execute_code())
### END patch_execute
