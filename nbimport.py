#
# Python module that defines Jupyter Notebook importing
#
# See also
#
#   https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Importing%20Notebooks.html
#
#
"""Import notebooks into other notebooks as a module"""

# --------------------------------------------------------------------
class NotebookFinderLoader(object):
    """Module finder that locates Jupyter Notebooks"""
    def __init__(self,
                 tags=None,
                 keys=None,
                 withshell=False):
        self._shell = None
        self._read = None
        self._tags = tags
        self._keys = keys
        self._spec = {}
        try:
            from nbformat import read
            self._read  = read
        except:
            print('Failed to get nbformat.read')

        if withshell:
            try:
                from IPython.core.interactiveshell import InteractiveShell
                self._shell = InteractiveShell.instance()
            except:
                print("Failted to get shell instance")

    def _find_ipynb(self,fullname,path):
        """find a notebook, given its fully qualified name and an optional
        path

        This turns "foo.bar" into "foo/bar.ipynb" and tries turning
        "Foo_Bar" into "Foo Bar" if Foo_Bar does not exist.

        Note, this does not work for paths like "Foo Bar/Baz_Gnus.ipynb"

        """
        if self._read is None:
            return None
        
        from os import path as ospath
        
        name = fullname.rsplit('.', 1)[-1]
        if not path:
            path = ['']

        for d in path:
            nb_path = ospath.join(d, name + ".ipynb")
            if ospath.isfile(nb_path):
                return nb_path
            
            # let import Notebook_Name find "Notebook Name.ipynb"
            nb_path = nb_path.replace("_", " ")
            if ospath.isfile(nb_path):
                return nb_path

        # print(f'{fullname} not found in {path}')
        return None

    def find_spec(self,fullname,path,target=None):
        """See if we can find the given module as a notebook

        Parameters
        ----------
            - fullname : str 
              Name of the notebook as a package path 
            - path : str 
              Search path 
            - target : str 
              Target 
        
        Returns
        -------
            - spec : ModuleSpec 
              The found module spec or None
        """
        if self._read is None:
            return None

        nb_path = self._find_ipynb(fullname, path)
        if nb_path is None:
            return None

        spec = None
        try:
            from importlib.machinery import ModuleSpec
            spec = ModuleSpec(fullname,self,origin=nb_path)
        except:
            class Dummy:
                pass
            spec = Dummy()
            spec.name = fullname
            spec.loader = self
            spec.origin = nb_path
            spec.submodule_search_locations = path
            spec.cached = None
            spec.loader_state = None
            spec.parent = None
            spec.has_location = True
        if spec is None:
            print('No spec found!')
            return None

        self._spec[fullname] = spec

        return spec
        
    def create_module(self, spec):
        """Create the module.  

        We read the NB here and store the cell content in the key
        __nbcode__, which we will then execute later on. 

        Note, if `tags` or `keys` where given in the constructor, 
        then only cells with a tag in the `tags` list, or a 
        metadata key in `keys` are imported. 

        Parameters
        ----------
            - spec : ModuleSpec 
              Module specification 
        
        Returns
        -------
            - module : module 
              The created module

        """
        if self._read is None:
            raise RuntimeError('nbformat.read not available')
        
        import sys
        from io import open
        
        module = type(sys)(spec.name)
        path   = spec.origin
        if path is None:
            raise ModuleNotFoundError(fullname)

        # load the notebook object
        with open(path, 'r', encoding='utf-8') as f:
            nb = self._read(f, 4)
            
        if nb is None:
            raise RuntimeError("Failed to read notebook")

        try:
            #from IPython.core.inputsplitter import IPythonInputSplitter
            #splitter = IPythonInputSplitter(line_input_checker=False)
            from IPython.core.inputtransformer2 import TransformerManager
            splitter = TransformerManager()
        except:
            print("Failed to make splitter")
            raise RuntimeError("Cannot transform code, no splitter")
        

        module.spec = spec
        module.__nbcode__ = []
        for cell in nb.cells:
            if cell.cell_type != 'code':
                continue

            do_import = self._tags is None and self._keys is None
            if self._tags is not None:
                if any([t in self._tags for t in cell.metadata.get('tags',[])]):
                    do_import = True

            if self._keys is not None:
                if any([k in self._keys for k in cell.metadata.keys()]):
                    do_import = True

            if not do_import:
                continue 

            try:
                # transform the input to executable Python
                code = splitter.transform_cell(cell.source)
                # print('Appending code: ',code)
                module.__nbcode__.append(code)
            except Exception as e:
                print('Failed to load cell:')
                print(cell.source)
                print(e)
                raise

        return module
    
    def exec_module(self, module):
        """Execute the code of the module

        The notebook code is stored in module.__nbcode__ 
        
        Parameters
        ----------
            - module  : module 
              The module 
        """
        if getattr(module,'__nbcode__', None) is None:
            return None

        save_user_ns = None
        if self._shell is not None:
            try:
                from IPython import get_ipython
            except:
                raise RuntimeError('Cannot import nbformat')
            
            
            # extra work to ensure that magics that would affect the user_ns
            # actually affect the notebook module's ns
            module.__dict__['get_ipython'] = get_ipython
            save_user_ns = shell.user_ns
            self._shell.user_ns = module.__dict__
        

        try:
            for code in module.__nbcode__:
                # run the code in the module namespace 
                exec(code, module.__dict__)
        except Exception as e:
            print("Error while loading code: {}".format(e))
            print(code)
            raise
        finally:
            if self._shell is not None:
                self._shell.user_ns = save_user_ns
            del module.__nbcode__

    def find_module(self,fullname,path=None):
        """Compatibility function"""
        spec = self.find_spec(fullname,path,None)
        return spec.loader if spec is not None else None

                        
    def load_module(self,fullname):
        """Compatibility function"""        
        import sys

        if fullname not in self._spec:
            return None
        
        spec = self._spec[fullname]

        if fullname in sys.modules:
            module = sys.modules[fullname]
        else:
            module = self.create_module(spec)
            module.__name__ = spec.name
            module.__loader__ = spec.loader
            module.__package__ = spec.parent
            sys.modules[spec.name] = module
            
        spec.loader.exec_module(module)
    
# --------------------------------------------------------------------
def initialize(tags=["nbimport"],
               keys=["pyfile","lib","nbimport"],
               withshell=False):
    """Initialize the notebook importer 

    Parameters
    ----------
        - libonly : boolean, optional 
          If true (default), only load cells with metadata lib=true. 
          Otherwise, load all cells 
        - withshell : boolean, optional
          If true, make an IPython shell instance 
    
    """
    from sys import meta_path
    meta_path.append(NotebookFinderLoader(tags,keys,withshell))


#
# EOF
#
