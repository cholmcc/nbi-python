numpy
scipy
sympy>=1.5
matplotlib
ipywidgets
nbconvert
sphinx-autorun
sphinx-automodapi
