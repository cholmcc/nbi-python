class NotebookBase(object):
    def __init__(self):
        pass
    
    def find_notebook(self,fullname, path=None):
        """find a notebook, given its fully qualified name and an optional path

        This turns "foo.bar" into "foo/bar.ipynb"
        and tries turning "Foo_Bar" into "Foo Bar" if Foo_Bar
        does not exist.
        """
        import os
        
        name = fullname.rsplit('.', 1)[-1]
        if not path:
            path = ['']

        for d in path:
            nb_path = os.path.join(d, name + ".ipynb")
            if os.path.isfile(nb_path):
                return nb_path
            # let import Notebook_Name find "Notebook Name.ipynb"
            nb_path = nb_path.replace("_", " ")
            if os.path.isfile(nb_path):
                return nb_path


class NotebookLoader(NotebookBase):
    """Module Loader for Jupyter Notebooks"""
    def __init__(self, path=None):        
        from IPython.core.interactiveshell import InteractiveShell

        super(NotebookLoader,self).__init__()

        self.shell = InteractiveShell.instance()
        self.path = path

    def load_module(self, fullname):
        """import a notebook as a module"""
        from nbformat import read
        from IPython import get_ipython
        import io, types, sys
        
        # Find the notebook 
        path = self.find_notebook(fullname, self.path)

        print("importing Jupyter notebook from {}".format(path))

        # load the notebook object
        with io.open(path, 'r', encoding='utf-8') as f:
            nb = read(f, 4)

        # create the module and add it to sys.modules
        # if name in sys.modules:
        #    return sys.modules[name]
        mod = types.ModuleType(fullname)
        mod.__file__ = path
        mod.__loader__ = self
        mod.__dict__['get_ipython'] = get_ipython
        sys.modules[fullname] = mod

        # extra work to ensure that magics that would affect the user_ns
        # actually affect the notebook module's ns
        save_user_ns = self.shell.user_ns
        self.shell.user_ns = mod.__dict__

        try:
          for cell in nb.cells:
            if cell.cell_type == 'code':
                # transform the input to executable Python
                code = self.shell.input_transformer_manager\
                                 .transform_cell(cell.source)
                # run the code in themodule
                exec(code, mod.__dict__)
        finally:
            self.shell.user_ns = save_user_ns
        return mod

class NotebookFinder(NotebookBase):
    """Module finder that locates Jupyter Notebooks"""
    def __init__(self):
        super(NotebookFinder,self).__init__()

        self.loaders = {}

    def find_module(self, fullname, path=None):
        import os
        
        nb_path = self.find_notebook(fullname, path)
        if not nb_path:
            return

        key = path
        if path:
            # lists aren't hashable
            key = os.path.sep.join(path)

        if key not in self.loaders:
            self.loaders[key] = NotebookLoader(path)

        return self.loaders[key]


import sys
sys.meta_path.append(NotebookFinder())
