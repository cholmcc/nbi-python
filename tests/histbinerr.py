#!/usr/bin/env python3 -i

from numpy import histogram, linspace, diff, sqrt
from numpy.random import normal
from matplotlib.pyplot import plot, errorbar, gca, twinx, legend, ion, close

ion()
close('all')

data = normal(0,1,size=1000)

h1, b1 = histogram(data,density=True)
h2, b2 = histogram(data,bins=b1)
h3     = h2/diff(b2)/len(data)

x      = (b1[1:]+b1[:-1])/2
w      = diff(b1)

e1     = sqrt(h1/w/len(data))
e2     = sqrt(h2)
e3     = sqrt(h2)/w/len(data)

errorbar(x,h1,e1,w/2,fmt='none',label='Normalized',color='C0',
         elinewidth=2)
errorbar(x,h3,e3,w/2,fmt='none',label='Raw, normalized',color='C1',
         elinewidth=.00001,capsize=10)
legend(loc='upper left')

ax = twinx()
#ax.errorbar(x,h2,e2,w/2,fmt='none',label='Raw',color='C3',
#            elinewidth=.00001,capsize=10)


ax.legend(loc='upper right')
