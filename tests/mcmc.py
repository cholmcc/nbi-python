def mcmc_step(old,sample,post,take,rg):
    """A single step in a Markov-Chain Monte-Carlo
    
    Parameters
    ----------
    old : tuple of theta, posterior 
        The old state an posterior likelihood 
    sample : callable 
        A function to sample the theta space with 
        prototype 
        
            def f(theta_old : float-or-array,
                  generator : numpy.random.Generator) -> float_or_array:
                  
    post : callable 
        Posterior probability given by product of likelihood and 
        prior probability.  Prototype 
        
            def f(theta : float_or_array) -> float:
            
    take : callable 
        Decide wether to accept new state.  Prototype 
        
            def f(p_new : float,
                  p_old : float,
                  generator : numpy.random.Generator) -> bool:
            
    rg : numpy.random.Generator (optional)
        Random number generator.
        
    Returns
    ------- 
    new_theta : float-or-array 
        New theta (possibly equal to old)
    new_posterior : float 
        New posterior likelihood 
    
    """
    p_old, theta_old = old 
    theta_new        = sample(theta_old,rg)
    p_new            = post(theta_new)
    
    
    return (p_new,theta_new) if take(p_new,p_old,rg) else old

def metropolis(p_new,p_old,rg):
    """Metropolis-Hastings Criteria for update
    
    If 
    
       p_new/p_old < u 
       
    for a uniform random variable u, accept the candidate 
    
    Parameters
    ----------
    p_new : float
        Candidate posterior probability 
    p_old : float 
        Old posterior probability 
    rg : numpy.random.Generator
        Random number generator 
    
    Returns
    ------- 
    accept : bool 
        True if we are to accept candidate 
    """
    r = p_new / p_old
    u = rg.uniform()
    return r > u

def mcmc_sample(theta_old,rg,sigma):
    """Sample parameter space by pertubing a random component
    
    The random component of is pertubed by a normal random deviate 
    with variance given by sigma 
    
    Parameters
    ---------- 
    theta_old : array (1,p)
        Old parameter set 
    rg : numpy.random.Generator 
        Random number generator 
    sigma : array (1,p)
        Standard deviation of pertubation 
        
    Returns
    -------
    candidate : array (1,p)
        Candidate parameter set 
    """
    from numpy import eye
    
    n     = len(theta_old)
    coord = rg.integers(n)
    direc = eye(1,n,coord)[0]
    return theta_old + rg.normal(0,sigma[coord])*direc

def mcmc(steps,
         theta0,
         like,
         prior,
         sample=mcmc_sample,
         take=metropolis,
         rg=None):
    """Markov-Chain Monte-Carlo 
    
    Parameters
    ----------
    steps : int 
        Number of steps in the chain 
    theta0 : array (p,)
        Initial value 
    like : callable 
        Likelihood function with prototype 
        
            def f(theta : array) -> float:
            
    prior : callable 
        Prior distribution of theta with prototype 
        
            def f(theta : array) -> float: 
            
    sample : callable or array
        How to sample the theta space. Prototype 
        
            def f(old_theta : array,
                  generator : numpy.random.Generator) -> array:
                  
        or array of standard deviations in which case mcmc_sample will
        be used
    take : callable 
        Decide wether to accept new state.  Prototype 
        
            def f(p_new : float,
                  p_old : float,
                  generator : numpy.random.Generator) -> bool:
                        
    rg : numpy.random.Generator (optional)
        Random number generator. If not specified the default
        generator will be used.  This is used to sample the theta
        space and the accept and reject random number
        
    Returns
    -------
    posterios : array (steps)
        Associated posteriors 
    thetas : array (p,steps)
        Accepted parameters

    """
    from functools import reduce, partial
    from numpy import array
    
    if rg is None or isinstance(rg,int):
        from numpy.random import default_rng
        rg = default_rng(rg if isinstance(rg,int) else None)
        
    if not callable(sample):
        sample = partial(mcmc_sample,sigma=sample)
        
    def post(theta):
        p = prior(theta)
        if p <= 0:
            return 0
        
        return like(theta)*p 
        
    start = [(post(theta0),theta0)]
    chain = reduce(lambda l, _ :
                   l + [mcmc_step(l[-1],sample,post,take,rg)],
                   range(steps-1),start)
    return array([c[0] for c in chain]), array([c[1] for c in chain])

def mcmc_result(posteriors,thetas,burnin):
    """Calculcates results from MCMC sample"""
    from numpy import sqrt, average, cov,diagonal, array
    
    sub              = thetas[burnin:]
    prob             = posteriors[burnin:]
    dof              = len(sub)
    means            = average(sub,axis=0,weights=prob)
    wcov             = cov(sub,aweights=prob,ddof=1,rowvar=False)
    var              = array([wcov.item()]) \
                       if wcov.ndim == 0 else diagonal(wcov)
    uncers           = sqrt(var/dof)
    
    return means, uncers 

def test(steps,th0=.5,a=5,b=17,n=100,k=66,sigma=0.3):
    from scipy.stats import beta, binom
    from numpy import array
    
    prior = lambda theta : beta.pdf(theta[0],a,b)
    like  = lambda theta : binom.pmf(k,n,theta[0])

    theta0 = array([th0])
    sigma0 = array([sigma])
    return mcmc(steps,theta0,prior,like,sample=sigma0)

def run(steps,burnin):
    post, theta = test(steps)
    means, uncer = mcmc_result(post, theta, burnin)
    
    from matplotlib.pyplot import subplots
    from numpy import log

    print(theta.shape,post.shape)
    fig, ax = subplots(nrows=theta.shape[1]+1,sharex=True,
                       gridspec_kw={'hspace':0},figsize=(8,8))

    ax[0].plot(post)
    ax[0].set_ylabel('Posterior')
    tax = ax[0].twinx()
    tax.plot(post[1:]/post[:-1],'.',color='C1')
    tax.set_ylabel(r'$P_{i+1}/P_i$')
    tax.set_ylim([0.95,1.05])

    for i,(t,m,u,a) in enumerate(zip(theta.T,means,uncer,ax[1:])):
        print(t)
        a.plot(t,label=fr'$p_{i}={m:.3f}\pm {u:.3f}$')
        a.set_ylabel(fr'$p_{i}$')
        a.legend()

    fig.tight_layout()
    fig.show()

from matplotlib.pyplot import ion, close

ion()

run(300,20)

    
