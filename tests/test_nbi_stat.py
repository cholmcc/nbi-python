#!/usr/bin/env python3
# 
# """Unit testing of nbi_stat"""

# Import libraries
import sys
sys.path.append("..")
sys.path.append(".")
import numpy as np
import nbi_stat as nbi
import unittest

# ====================================================================
class ArrayTest(unittest.TestCase):
    def __init__(self,methodName='runTest'):
        super(ArrayTest,self).__init__(methodName)
        self.addTypeEqualityFunc(np.ndarray, self._testArray)
        self._rtol = 1e-5
        self._atol = 1e-5

    def _testArray(self,exp,val,msg):
        if not np.allclose(exp,val,rtol=self._rtol,atol=self._atol):
            raise self.failureException(msg)
        
# ====================================================================
class TestNSignificant(unittest.TestCase):
    """Test of the function nSignificant"""
    def test_all(self):
        """We test some numbers.  Note, to pass a whole number with leading
        digits, we must pass it as a string - otherwise it will be
        interpreted as an octadecimal number or cause an error.

        """
        
        nums = [12345, 12340, "012345", 0.00123, "0.01200"]
        expt = [5,     5,      5,       3,       4        ]
        for n,e in zip(nums,expt):
            ns = nbi.nSignificant(n)
            self.assertEqual(ns,e,
                             "Got {} significant digits in {}, expected {}"
                             .format(ns, n, e))



# ====================================================================
class TestRound(unittest.TestCase):
    """Test of round"""

    def test_all(self):
        """We test some numbers"""
        numbers = (12.6, 12.4, 12.50, 12.51, 13.5, 13.55, 12.504, 13.504)
        expect  = (13,   12,   12,    13,    14,   14,    12,     14)
        prec    = 0
        for n, e in zip(numbers, expect):
            r = nbi.round(n, prec)
            self.assertEqual(r, e,"{} round to {} gave {}, expected {}"
                             .format(n,10**(prec), r, e))
            
                             

# ====================================================================
class TestRoundResult(ArrayTest):
    """Test of roundResult"""

    def _form1(self,r,ndig,width):
        """Format a single number with a partcular set of digits and a certain
        width.  If ndig is None, do not format, just print as is.

        """ 
        if ndig is None:
            return "{}".format(r)
        return "{:{}.{}f}".format(r,width,ndig)
        
    def _form(self,r,u,ndig=None,width=8):
        """Format a result."""
        return self._form1(r,ndig,width) + \
            " +/- ".join([self._form1(uu,ndig,width) for uu in u])
            
    def _test(self, r, u, nsig, er, eu):
        """Test of rounding"""
        rr, ru, ndig = nbi.roundResult(r, u, nsig)
        r = np.array((rr,*ru))
        e = np.array((er,*eu))
        self.assertEqual(r, e,
                        "\n  {}\nrounded to {} significant digit(s) gave\n"
                        "  {}\n"
                        "expected\n  {}".format(self._form(r,u),nsig,
                                                self._form(rr,ru,ndig),
                                                self._form(er,eu,ndig)))

    def test_1(self):
        """First test with 1 significant digit"""
        self._test(1234.56, [12.3456, 1.23456, 0.123456] , 1,
                   1234.6,  [12.3,    1.2,     0.1])

    def test_2(self):
        """Second test with 2 significant digit"""
        self._test(1234.56, [12.3456, 1.23456, 0.123456] , 2,
                   1234.56, [12.35,   1.23,    0.12])

    def test_3(self):
        """Whote numbers"""
        self._test(1234.56, [123.456, 12.3456], 1,
                   1230,    [120,     10,  ])
        
    def test_4(self):
        """Whole numbers"""
        self._test(1234.56, [123.456, 12.3456], 2,
                   1235,    [123,     12,  ])
        
    def test_5(self):
        """Negative numbers"""
        self._test(-1234.56, [12.3456, 1.23456, -0.123456] , 1,
                   -1234.6,  [12.3,    1.2,     -0.1])

# ====================================================================
class TestUpdate(ArrayTest):
    """Base class for tesing welford and west updates"""
    def setUp(self):
        nsamples = 1000
        ndim     = 4
        np.random.seed(123456)
        self._problem     = np.array([4 ,7 ,13 ,16] ,dtype=np.float64)+1e9
        self._poisson     = np.random.poisson(10,size=(nsamples,ndim))
        self._uniform     = np.random.random(size=(nsamples,ndim))
        self._consequtive = np.repeat(np.array([np.arange(nsamples)]).T,
                                      ndim, axis=1)
        self._normal      = np.random.normal(size=(nsamples,ndim))

    def _testEqual(self,exp,val,prec,msg):
        if prec is None:
            self.assertEqual(val,exp,msg)
        else:
            self.assertAlmostEqual(val,exp,prec,msg)
                             
    def _testResult(self,nm,nv,m,v,prec=None,
                    mrtol=1e-2,vrtol=1e-2,
                    matol=1e-5,vatol=1e-5):
        mmsg = "Mean     {} does not match expected {} [{}]"
        vmsg = "Variance {} does not match expected {} [{}]"
        if np.ndim(nv) > 1:
            mmsg = "Mean\n{}\ndoes not match expected\n{}\nratio\n{}"
            vmsg = ("Covariance\n{}\ndoes not match expected\n{}\nratio\n{}")

        with np.printoptions(precision=5,suppress=True):
            srtol, self._rtol = self._rtol, mrtol
            satol, self._atol = self._atol, matol
            self._testEqual(nm,m,prec, mmsg.format(m, nm, m/nm))
            self._rtol = vrtol
            self._atol = vatol
            self._testEqual(nv,v,prec,vmsg.format(v, nv, v/nv))
            self._rtol = srtol
            self._atol = satol
        
        
    
# ====================================================================
class TestWelfordUpdate(TestUpdate):
    """Test of the Welford algorithm for on-line mean and variance"""
    
    def test_1d_problem(self):
        """Catastrophic rounding sample gives right result"""
        x    = self._problem
        stat = nbi.welfordInit()
        for xx in x:
            stat = nbi.welfordUpdate(xx, *stat)

        self._testResult(x.mean(),x.var(),*stat[0:2])

    def test_1d_random(self):
        """Uniform random sample is roughly OK"""
        x    = self._uniform[:,0]
        stat = nbi.welfordInit()
        for xx in x:
            stat = nbi.welfordUpdate(xx, *stat)

        self._testResult(x.mean(),x.var(),*stat[0:2],7)
        
    def test_nd_normal(self):
        """Random sampling in 3 dimensions of a normal distribution"""
        x    = self._normal
        stat = nbi.welfordInit(len(x[0]))
        for xx in x:
            stat = nbi.welfordUpdate(xx, *stat)

        self._testResult(x.mean(axis=0),x.var(axis=0),*stat[0:2],
                         matol=1e-2, vatol=1e-3)

        
# ====================================================================
class TestWestUpdate(TestUpdate):
    def _np(self,x,w,frequency=True,axis=None):
        d = w.sum(axis=axis)
        if not frequency:
            d = w.sum(axis=axis)-(w**2).sum(axis=axis)/w.sum(axis=axis)

        a = np.average(x,axis,w)
        v = np.sum([ww*(xx-a)**2 for ww,xx in zip(w,x)],axis=axis) / d
        return  a, v
    
    def test_1d_problem(self):
        """Catastrophic rounding sample gives right result with weights"""
        x  = self._problem
        stat = nbi.westInit()
        for xx in x:
            stat = nbi.westUpdate(xx, 1, *stat)

        self._testResult(x.mean(),x.var(),*stat[0:2])

    def test_1d_frequency(self):
        """1D sample w/frequency weights"""
        w    = self._poisson[:,0]
        x    = self._consequtive[:,0]
        stat = nbi.westInit()
        for xx,ww in zip(x,w):
            stat = nbi.westUpdate(xx, ww, *stat)

        self._testResult(*self._np(x,w),*stat[0:2],7)


    def test_1d_nonfrequency(self):
        """1D sample w/non-frequency weights"""
        w    = self._uniform[:,0]
        x    = self._normal[:,0]
        stat = nbi.westInit(frequency=False)
        for xx,ww in zip(x,w):
            stat = nbi.westUpdate(xx, ww, *stat)

        self._testResult(*self._np(x,w,False),*stat[0:2],2)
        
    def test_nd_frequency(self):
        """N-D sample w/frequency weights"""
        w    = self._poisson
        x    = self._consequtive
        stat = nbi.westInit(len(x[0]),component=True)
        for xx,ww in zip(x,w):
            stat = nbi.westUpdate(xx, ww, *stat)

        self._testResult(*self._np(x,w,True,0), *stat[0:2])

    def test_nd_nonfrequency(self):
        """N-D sample w/non-frequency weights"""
        w  = self._uniform
        x  = self._normal
        stat = nbi.westInit(len(x[0]),False)
        for xx,ww in zip(x,w):
            stat = nbi.westUpdate(xx, ww, *stat)

        self._testResult(*self._np(x,w,False,0),*stat[0:2])

# ====================================================================
class TestWelfordCovUpdate(TestUpdate):

    def test_normal(self):
        """Covariance of normal distributed variables"""
        x    = self._normal
        stat = nbi.welfordInit(len(x[0]),covar=True)
        for xx in x:
            stat = nbi.welfordCovUpdate(xx, *stat)

        self.assertTrue(np.allclose(stat[1],stat[1].T),
                        "Covariance not symmetric")
        self._testResult(x.mean(axis=0),np.cov(x,rowvar=False),*stat[0:2])

# ====================================================================
class TestWestCovUpdate(TestUpdate):

    def test_frequency_sample(self):
        """Covariance of frequency,observation weighted sample"""
        x    = self._consequtive
        w    = self._poisson[:,0]
        nm   = np.average(x,0,w)
        nc   = np.cov(x,rowvar=False,fweights=w)
        stat = nbi.westInit(len(x[0]),covar=True)
        for xx,ww in zip(x,w):
            stat = nbi.westCovUpdate(xx, ww, *stat)

        self.assertTrue(np.allclose(stat[1],stat[1].T),
                        "Covariance not symmetric\n{}".format(stat[1]))
        self._testResult(nm,nc,*stat[0:2])

    def test_nonfrequency_sample(self):
        """Covariance of non-frequency,observation weighted sample"""
        x    = self._normal
        w    = self._uniform[:,0]
        stat = nbi.westInit(len(x[0]),frequency=False,covar=True)
        for xx,ww in zip(x,w):
            stat = nbi.westCovUpdate(xx, ww, *stat)

        self.assertTrue(np.allclose(stat[1],stat[1].T),
                        "Covariance not symmetric\n{}".format(stat[1]))
        self._testResult(np.average(x,0,w),np.cov(x,rowvar=False,aweights=w),
                         *stat[0:2])

    def test_frequency_component(self):
        """Covariance of frequency,component weighted sample"""
        x    = self._consequtive
        w    = self._poisson
        stat = nbi.westInit(len(x[0]),covar=True,component=True)
        for xx,ww in zip(x,w):
            stat = nbi.westCovUpdate(xx, ww, *stat)

        self.assertTrue(np.allclose(stat[1],stat[1].T,1e-3),
                        "Covariance not symmetric\n{}".format(stat[1]))
        self._testResult(np.average(x,0,w),nbi.wcov(x,w),*stat[0:2],None,
                         vrtol=1e-1)

    def test_nonfrequency_component(self):
        """Covariance of non-frequency,component weighted sample"""
        x    = self._normal
        w    = self._uniform
        stat = nbi.westInit(len(x[0]),frequency=False,covar=True,component=True)
        for xx,ww in zip(x,w):
            stat = nbi.westCovUpdate(xx, ww, *stat)

        self.assertTrue(np.allclose(stat[1],stat[1].T,1e-1),
                        "Covariance not symmetric\n{}".format(stat[1]))
        self._testResult(np.average(x,0,w),nbi.wcov(x,w,frequency=False),
                         *stat[0:2],None,vrtol=1e-1,vatol=1e-2)

    def test_reduce(self):
        x    = self._normal
        w    = np.ones_like(x)
        stat = nbi.westInit(len(x[0]),frequency=False,covar=True,component=True)
        for xx,ww in zip(x,w):
            stat = nbi.westCovUpdate(xx, ww, *stat)

        self.assertTrue(np.allclose(stat[1],stat[1].T,1e-1),
                        "Covariance not symmetric\n{}".format(stat[1]))
        self._testResult(x.mean(axis=0),np.cov(x,rowvar=False),
                         *stat[0:2],None,vrtol=1e-2,vatol=1e-2)

        
        
# ====================================================================
class TestPropagateUncertainty(unittest.TestCase):
    def setUp(self):
        x = np.random.normal(2,1,size=100)
        y = np.random.normal(3,1,size=100)
        self._c  = np.cov(x,y)
        self._xm = x.mean()
        self._ym = y.mean()
        
    def test_add(self):
        ee = np.sqrt(self._c.sum())
        vv = np.sqrt(nbi.propagateUncertainty(lambda x:x.sum(),
                                              [self._xm,self._ym],
                                              self._c))
        self.assertAlmostEqual(ee,vv,1,
                               "Uncertainty {} does not match expected {}"
                               .format(vv,ee))
        
    def test_minus(self):
        ee = np.sqrt(self._c[0,0]+self._c[1,1]+2*(-1)*self._c[1,0])
        vv = np.sqrt(nbi.propagateUncertainty(lambda x:x[0]-x[1:].sum(),
                                              [self._xm,self._ym],
                                              self._c))
        self.assertAlmostEqual(ee,vv,1,
                               "Uncertainty {} does not match expected {}"
                               .format(vv,ee))

    def test_mult(self):
        re = (self._c[0,0]/self._xm**2 +
              self._c[1,1]/self._ym**2 +
              2*self._c[0,1]/(self._xm*self._ym))
        ee = np.sqrt(re)*self._xm*self._ym
        vv = np.sqrt(nbi.propagateUncertainty(lambda v:v[0]*v[1],
                                              [self._xm,self._ym],
                                              self._c))
        self.assertAlmostEqual(ee,vv,1,
                               "Uncertainty {} does not match expected {}"
                               .format(vv,ee))

    def test_div(self):
        re = (self._c[0,0]/self._xm**2 +
              self._c[1,1]/self._ym**2 -
              2*self._c[0,1]/(self._xm*self._ym))
        ee = np.sqrt(re)*self._xm/self._ym
        vv = np.sqrt(nbi.propagateUncertainty(lambda v:v[0]/v[1],
                                              [self._xm,self._ym],
                                              self._c))
        self.assertAlmostEqual(ee,vv,1,
                               "Uncertainty {} does not match expected {}"
                               .format(vv,ee))

    def test_explog(self):
        def f(v):
            return np.exp(v[0])+np.log(v[1])
        def ef(x,y,c):
            return np.sqrt(np.exp(x)**2*c[0,0]+1/y**2*c[1,1]
                           + 2 * np.exp(x)/y**2 * c[0,1])
        ee = ef(self._xm, self._ym, self._c)
        ss = .1*np.sqrt(np.diagonal(self._c))
        vv = np.sqrt(nbi.propagateUncertainty(f, [self._xm,self._ym],
                                              self._c, ss))
        self.assertAlmostEqual(ee,vv,1,
                               "Uncertainty {} does not match expected {}"
                               .format(vv,ee))
        
    def test_trig(self):
        def f(v):
            return np.sin(v[0]) + np.cos(v[1])
        def ef(x,y,c):
            return np.sqrt(np.cos(x)**2*c[0,0] +
                           np.sin(y)**2*c[1,1] +
                           2*np.cos(x)*(-np.sin(y))*c[0,1])
        ee = ef(self._xm, self._ym, self._c)
        ss = .1*np.sqrt(np.diagonal(self._c))
        vv = np.sqrt(nbi.propagateUncertainty(f, [self._xm,self._ym],
                                              self._c, ss))

        self.assertAlmostEqual(ee,vv,1,
                               "Uncertainty {} does not match expected {}"
                               .format(vv,ee))
        
    def test_const(self):
        vv = np.sqrt(nbi.propagateUncertainty(lambda x:x,self._xm,
                                              self._c[0,0]))
        self.assertAlmostEqual(vv,np.sqrt(self._c[0,0]),5)
                     
    
# ====================================================================
class TestHistogram(ArrayTest):
    def setUp(self):
        self._x = np.random.normal(size=1000)
        self._b = np.linspace(-3,3,31)
        
        self._np = np.histogram(self._x, self._b, density=True)
        
    
    def test_1(self):
        """Check normalized histogram against NumPy density histogram"""
        nc = self._np[0]
        hc, hm, hw, he = nbi.histogram(self._x, self._b,normalize=True)

        self.assertEqual(nc,hc,"NumPy histogram\n{}\n"
                         "not equal to histogram\n{}".format(nc,hc))

    def test_online(self):
        """Check normalized histogram against NumPy density histogram"""
        hist = nbi.initHistogram(self._b)
        for x in self._x:
            hist = nbi.fillHistogram(x,*hist)

        nc = self._np[0]
        hc, hm, hw, he = nbi.finiHistogram(*hist,normalize=True)

        self.assertEqual(nc,hc,"NumPy histogram\n{}\n"
                         "not equal to histogram\n{}".format(nc,hc))
        
    def test_class(self):
        hist = nbi.Histogram(self._b)
        for x in self._x:
            hist.fill(x)

        hist.finalize(normalize=True)
        nc = self._np[0]
        hc = hist.heights()

        self.assertEqual(nc,hc,"NumPy histogram\n{}\n"
                         "not equal to histogram\n{}".format(nc,hc))


# ====================================================================
if __name__ == '__main__':
    unittest.main()
    

#
# EOF
#
