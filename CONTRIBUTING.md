# Developer notes

## Titles and such 

One should make a Markdown cell at the top of the notebook with
content like 

	### Author Name <a class="tocSkip"></a>
	# Title <a class="tocSkip"></a>
	## Sub title <a class="tocSkip"></a>
	## Version and date <a class="tocSkip"></a>
	
	> Abstract 
	
	### Niels Bohr Institute <a class="tocSkip"></a>
	
Furthermore, the cell should have the _tag_ 

	"title"
	
so that the PDFs come out right.  To edit the metadata, select 

	View -> Cell Toolbar -> Tags 

## Module code 

If your Notebook is supposed to export functionality (either to other
notebooks or to a Python module), edit the Metadata to have 


	"pyfile": "lib" 
	
To edit the metadata, select 

	View -> Cell Toolbar -> Edit
	Metadata 

To export the code cells to a Python module, run 

	./utils/writelib.py Notebook.ipynb -o PythonModule.py 
	
replacing the Notebook filename and the desired Python module filename 

## Large notebooks 

Some of these notebooks may become _very_ large - for example
[Best Practises](basics/BedsteVaner.ipynb) - which may cause problem when loading,
especially if `nbextensions` are used. We therefore add the cell

	%%javascript
    var rto = 120;
    window.requirejs.config({waitSeconds: rto});

to create a longer timeout for loading `nbextensions`.

However, the large notebooks are a pain to work with.  Some of the
notebooks have therefore (e.g., [Statistics Overview](statistics/Stat_00.ipynb)) been
split into several parts (one for each chapter).  These are then
merged to together by the script [merge.py](utils/merge.py).  This
script requires an output filename and a _sorted_ list of input
notebooks. The script then takes the cells of each input and the
_metadata_ of the _first_ input to build a new notebook containing
everything.

Note, to include code from previous parts of a notebook put in a code
cell near the top 

	import nbimport
	nbimport.initialize()
    
and edit the metadata of that cell to contain 

	"nomerge": true
	
In a cell after that, do 

	from PreviousPart import *

to import from previous parts.  

To edit the metadata, select 

	View -> Cell Toolbar -> Edit
	Metadata 

You should put all module and package loads into this cell and then
make sure that some initial part loads all code need in a code cell
_not_ marked as `"nomerge":true`. 

## Multi-lingual 

The notebooks are intended to be available in both Danish _and_
English.  To that end we use the extension
[`nbTranslate`](https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/nbTranslate/README.html)
from the [unofficial Jupyter notebook
extensions](https://jupyter-contrib-nbextensions.readthedocs.io)
package.  This allows us to use [Google
translate](https://translate.google.com/) to translate markdown cells
from Danish to English (or vice verse).  Obviously this means that the
notebooks will grow to almost twice the number of cells. 

## Extensions used 

- [`nbTranslate`](https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/nbTranslate/)
- [toc2](https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/toc2/)

## LaTeX considerations 

- Please put multi-line equations into an `align*` environment (rather
  than `$$`). 
  
	  \begin{align*}
	    
	  \end{align*}

- Differentials are written with an upright `d`.  That is, we write 

      \mathrm{d}N/\mathrm{d}x   \int\mathrm{d}x\,f(x)
	  
  to obtain 
  
  ```math
  \mathrm{d}N/\mathrm{d}x
  ```
  
  and 
  
  ```math
  \int\mathrm{d}x\,f(x)
  ```
  

## NBConvert considerations 

We use our own templates for conversion to $`\mathrm{\LaTeX}`$, HTML (including Reveal slides), and EPUB.
These templates live in 

    templates/latex/
	templates/html 
    templates/epub
	
respectively.  The differences to the official templates are 

### LaTeX 

The main difference between our $`\mathrm{\LaTeX}`$ templates and the
ones that come with `nbconvert` is that we use the
[listings](https://ctan.org/pkg/listings) package to format the source
code.  We do this because it generally does a better job than what we
get from plain `nbconvert` (`pandoc`).

Furthermore, the templates allows for translated documents, automatic
inclusion of `\tableofcontents` if we have `toc` cell, and similar.  

To properly format the title page, the templates _require_ that a
markdown cell has the _tag_ `"title"` (see
[above](#Titles_and_such).

Cells can be marked as ignore for the  $`\mathrm{\LaTeX}`$ by setting
the tag

	"hide_input"
    "hide_output"
	
on the cell.   The templates are 

- `templates/latex/minebase.tplx`  - Base template for all templates 
- `templates/latex/minearticle.tplx` - Base templates for documents
  that will use the `article` document class 
- `templates/latex/minearticle.en.tplx` - Template for English documents
  that will use the `article` document class 
- `templates/latex/minearticle.da.tplx` - Template for Danish documents
  that will use the `article` document class 
- `templates/latex/minereport.tplx` - Base templates for documents
  that will use the `report` document class 
- `templates/latex/minereport.en.tplx` - Template for English documents
  that will use the `report` document class 
- `templates/latex/minereport.da.tplx` - Template for Danish documents
  that will use the `report` document class 
  

## HTML 

For plain HTML the template isn't all that different, except that we
make sure that the table of content cell (if present) is properly
updated on loading the page. 

For Reveal slides, we also update the table of contents on load, _and_
we set the screen size to be a bit larger than what is normally used. 

The templates are 

- `templates/html/minehtml.tpl` - Template for HTML files 
- `templates/html/minereveal.tpl` - Template for Reveal slides 


## EPUB 

We have a special HTML template to generate the basic input for the EPUB generator `ebook-convert` from _Calibre_.  The template is 

    templates/epub/mineepub.tpl 
    
This exports to HTML which uses _MathJax_ for $`\mathrm{\LaTeX}`$ rendering.  However, many E-Book viewers (including Google Play Books) does not support _JavaScript_ (and hence does not support _MathJax_), and even if _MathML_ is part of the EPUB-3 standard that formatting is poorly supported (including by Goggle Play Books).  Thus, the template sets up _MathJax_ to render $`\mathrm{\LaTeX}`$ as SVG images.  

However, that does not mean that the exported HTML page _actually_ contain SVG images.  All it means is that _MathJax_ will _render_ SVGs on load in a browser.  Therefore, we load the page in a headless _Chrome_ session, and wait until _MathJax_ has completely rendered the contained $`\mathrm{\LaTeX}`$ (which may take minutes), and then we grap the page source as presented by the browser.  This, then, contains the rendered SVG images.  

We parse the source and replace all _MathJax_ calls with the generated SVG.  But, again, not all E-Book viewers support embedded SVGs (Goggle Play Books, for example), so we convert all SVGs into PNGs and embed those instead.  All along this process we make sure that anchors and references are kept.  

We also clean up some CSS and remove all references to external JavaScript.  When all this is done (see [`utils/save_as.py`](utils/save_as.py)) we call `ebook-convert` with appropriate options. 

Note, the EPUBs cannot be made in the GitLab CI as the image used does not have `ebook-convert`.  Instead EPUBs are generated and uploaded _by-hand_ separately. 

2018 © Christian Holm Christensen 