#
#
# Define EXECUTE to execute all notebooks in-place#

# VPATH		:= . basics mathematics mechanics statistics misc
REQUIREJS	:= file:/usr/share/javascript/requirejs/require.min.js
JQUERY		:= file:/usr/share/javascript/jquery/jquery.min.js
MATHJAX		:= file:/usr/share/javascript/mathjax/MathJax.js
REVEAL		:= https://unpkg.com/reveal.js@4.0.2

# Make sure MathJax is version 2 - version 3 isn't supported by nbconvert (templates) just yet 
REQUIREJS_REM	:= https://cdn.jsdelivr.net/npm/requirejs@2.3.6/require.js
JQUERY_REM	:= https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js
MATHJAX_REM	:= https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js
REVEAL_REM	:= https://unpkg.com/reveal.js@4.0.2

#https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.5.0
#https://cdn.jsdelivr.net/npm/reveal.js@4.1.3/dist/reveal.min.js

# ====================================================================
# nbConvert
#
# --------------------------------------------------------------------
# Default templates 
#
JEXT		:= /usr/lib/python3/dist-packages/jupyter_contrib_nbextensions
JEXT_DOCKER	:= $(HOME)/.local/lib/python3.9/site-packages/jupyter_contrib_nbextensions
TPLX		:= templates/latex/minearticle
TPL		:= templates/html/minehtml
ETPL		:= templates/html/minehtml
RTPL		:= templates/html/minereveal
NBCONV		:= jupyter nbconvert \
		   --TemplateExporter.extra_template_paths=.

EPUB_TIMEOUT	:= 30 
#
# Use Pandoc for Markdown -> HTML
#
#M2H_FLAGS	:= --TemplateExporter.filters={\"markdown2html\":\"nbconvert.filters.markdown2html_pandoc\"}
M2H_FLAGS	:= 
#
# Flags for nbconvert depending output format
#
LTX_FLAGS	:= --to latex --no-prompt
PY_FLAGS	:= --to script
HTML_URLS	:= --HTMLExporter.require_js_url=$(REQUIREJS) \
		   --HTMLExporter.jquery_url=$(JQUERY) \
		   --HTMLExporter.mathjax_url=$(MATHJAX) \
		   --TemplateExporter.filters="mathjax_u=mj.mj"
HTML_FLAGS	:= --to html   --template=$(TPL)  $(M2H_FLAGS) $(HTML_URLS)
EPUB_FLAGS	:= --to utils.epub.EPUBExporter --template=$(ETPL) \
		    $(M2H_FLAGS) $(HTML_URLS) --no-prompt
SLIDES_FLAGS	:= --to slides --template=$(RTPL) $(M2H_FLAGS) $(HTML_URLS) \
		   --SlidesExporter.reveal_theme=simple	\
		   --TemplateExporter.filters="mathjax_u=mj.mj" \
		   --reveal-prefix $(REVEAL)
NB_FLAGS	:= --to notebook --execute --inplace 
LANG_FLAGS	=  --to notebook \
		   --LanguagePreprocessor.language=$(1)	\
		   --SetLanguage.language=$(1) \
		   --allow-errors		\
		   --Exporter.preprocessors utils.preprocess.SetLanguage \
		   --Exporter.preprocessors utils.preprocess.LanguagePreprocessor

# EXECUTE		:= --execute

# Remote URL for releal.js.  Used by Gitlab CI
INTERNAL_PY	:= utils/hide_toggle.py 	\
		   utils/css_styling.py 	\
		   utils/patch_execute.py	\
		   utils/show_all.py		\
		   utils/setup_matplotlib.py

# ====================================================================
# Programs
#
XELATEX		:= TEXINPUTS=.:../..:..:../../..: xelatex
LATEX_FLAGS	:= -synctex=15 -interaction=batchmode
PYTHON		:= python3

# ====================================================================
# PIP
ifndef PYPI_URL
PYPI		:=
else
PYPI		:= --repository-url $(PYPI_URL)
endif

# ====================================================================
# Sources (parts and full)
#
SUBS		:= basics statistics mechanics mathematics misc 
STAT_SRC	:= $(sort $(wildcard statistics/Stat_*.ipynb))
SRCNT		:= misc/NewtonSecondLaw.ipynb			\
		   courses/appstat/PythonIntro.ipynb		\
		   courses/appstat/PlottingAndFitting.ipynb	\
		   statistics/R2.ipynb				\
		   statistics/PCA.ipynb				\
		   statistics/Bootstrap.ipynb			\
		   statistics/Examples.ipynb			\
		   statistics/Barlow.ipynb 			\
		   mechanics/Glauber.ipynb			\
		   mechanics/d2Ndetadphi.ipynb			\
		   misc/DoesAndDonts.ipynb
SRC		:= basics/Python.ipynb 			\
		   basics/Indexing.ipynb		\
		   basics/BedsteVaner.ipynb 		\
		   basics/BasalData.ipynb      		\
		   basics/PeakFind.ipynb		\
		   basics/ToyExpSim.ipynb		\
		   basics/Notebook.ipynb		\
		   statistics/Statistik.ipynb 		\
		   statistics/Anscombe.ipynb		\
		   statistics/Lecture.ipynb		\
		   mechanics/DobbeltPendul.ipynb	\
		   mechanics/Schwarzchild.ipynb		\
		   mathematics/Symbolic.ipynb		\
		   mathematics/OrthonormalF.ipynb	\
		   mathematics/FFT.ipynb		\
		   misc/Sjov.ipynb			\
		   misc/Eratosthenes.ipynb		
IDX		:= index.ipynb				\
		   basics/index.ipynb			\
		   statistics/index.ipynb		\
		   mechanics/index.ipynb		\
		   mathematics/index.ipynb		\
		   misc/index.ipynb			\
		   courses/index.ipynb			\
		   courses/amas/index.ipynb		\
		   courses/appstat/index.ipynb			

COURSES		:= courses/amas courses/appstat

# --------------------------------------------------------------------
# Generated notebooks, PDFS, and slides 
#
TNBS		:= $(SRC:%.ipynb=%.da.ipynb) 		\
		   $(SRC:%.ipynb=%.en.ipynb)		
NBS		:= $(TNBS) $(SRCNT)
PDFS		:= $(SRC:%.ipynb=%_da.pdf)   		\
		   $(SRC:%.ipynb=%_en.pdf)		\
		   $(SRCNT:%.ipynb=%.pdf)
NHTMLS		:= misc/NewtonSecondLaw.ipynb		\
		   courses/appstat/PythonIntro.ipynb	\
		   courses/appstat/PlottingAndFitting.ipynb
HTMLS		:= $(filter-out				\
			$(NHTMLS:%.ipynb=%.html)	\
		        $(NHTMLS:%.ipynb=%.en.html)	\
		        $(NHTMLS:%.ipynb=%.da.html),	\
			$(NBS:%.ipynb=%.html) 		\
		   	$(IDX:%.ipynb=%.da.html) 	\
		   	$(IDX:%.ipynb=%.en.html))
NSLDS		:= statistics/Statistik.ipynb		\
		   basics/Python.ipynb			\
		   mathematics/OrthonormalF.ipynb 	\
		   basics/ToyExpSim.ipynb		\
		   misc/NewtonSecondLaw.ipynb		
SLDS		:= $(filter-out 				\
		      	$(NSLDS:%.ipynb=%.slides.html) 		\
		      	$(NSLDS:%.ipynb=%.en.slides.html) 	\
			$(NSLDS:%.ipynb=%.da.slides.html), 	\
			$(NBS:%.ipynb=%.slides.html))
CODES		:= statistics/nbi_stat.py 		\
		   statistics/nbi_pca.py		\
		   basics/nbi_sim.py			\
		   basics/nbi_sim_exa.py		\
		   basics/nbi_sim_test.py		\
		   basics/nbi_sim_brahms.py		\
		   basics/nbi_sim_onion.py		\
		   mechanics/nbi_glauber.py		\
		   mechanics/nbi_d2ndetadphi.py
DIRS		:= misc basics statistics mathematics mechanics
APIS		:= statistics/nbi_stat			\
		   basics/nbi_sim
EPUBS		:= statistics/Statistik.en.epub	\
		   statistics/Statistik.da.epub	\
		   statistics/PCA.epub		\
		   statistics/Bootstrap.epub	\
		   basics/BasalData.en.epub	\
		   basics/BasalData.da.epub	\
		   basics/BedsteVaner.da.epub	\
		   basics/BedsteVaner.en.epub	\
		   basics/Python.da.epub	\
		   basics/Python.en.epub

# A macro to extract the title from a notebook
get_title	= $(shell ./utils/write_opf.py $(1) | sed -n 1p)
get_authors     = $(shell ./utils/write_opf.py $(1) | sed -n 3p)
get_publisher	= $(shell ./utils/write_opf.py $(1) | sed -n 4p)
get_language	= $(patsubst .%,%,$(suffix $(notdir $*)))

%.meta:%.ipynb
	$(MUTE)echo "Meta pat:  $(meta_pattern)"
	$(MUTE)$(call get_meta $<)
	$(MUTE)echo "Title:     $(call get_title, $<)"
	$(MUTE)echo "Authors:   $(call get_authors, $<)"
	$(MUTE)echo "Publisher: $(call get_publisher, $<)"
	$(MUTE)echo "Language:  $(call get_language, $<)"

# ====================================================================
# Redirection of output
#
ifndef VERBOSE
REDIRECT	=  >  $(dir $@)/.$(notdir $@).log 2>&1 && true || \
		      (cat $(dir $@)/.$(notdir $@).log;false)
AREDIRECT	=  >> $(dir $@)/.$(notdir $@).log 2>&1 && true || \
		      (cat $(dir $@)/.$(notdir $@).log;false)
LREDIRECT	=  >  .$(notdir $@).log 2>&1 && true || \
		      (cat .$(notdir $@).log;false)
MUTE		=  @
else
REDIRECT	=
AREDIRECT	=
LREDIRECT	=  
MUTE		=
endif

# ====================================================================
# Pattern rules
#
%.ex.ipynb:%.ipynb
ifdef EXECUTE
	@echo "Executing $< in-place ($(NB_FLAGS))" 
	$(MUTE)$(NBCONV) $(NB_FLAGS) $< $(REDIRECT)
endif
	$(MUTE)touch $@ 

# --------------------------------------------------------------------
# Danish and English NB from NB
#
%.da.ipynb:%.ex.ipynb utils/preprocess.py
	@echo "Making Danish version of $*"
	$(MUTE)$(NBCONV) $(call LANG_FLAGS,da) --output=$(notdir $@) $*.ipynb \
		$(REDIRECT)

%.da.ex.ipynb:%.da.ipynb %.da.ipynb
	$(MUTE)touch $*.da.ex.ipynb 

%.en.ipynb:%.ex.ipynb utils/preprocess.py
	@echo "Making English version of $*"
	$(MUTE)$(NBCONV) $(call LANG_FLAGS,en) --output=$(notdir $@) $*.ipynb \
		$(REDIRECT)

%.en.ex.ipynb:%.en.ipynb %.ex.ipynb
	$(MUTE)touch $*.en.ex.ipynb 

# --------------------------------------------------------------------
# Update internal code
#
%.update.ipynb:%.ipynb
	./utils/update_internal.py $< $@ -c $(INTERNAL_PY)

%.update_idx.ipynb:%.ipynb
	./utils/update_internal.py $< $@ -c utils/css_styling_idx.py

# --------------------------------------------------------------------
# LaTeX from NB
#
%.tex:%.ex.ipynb utils/post.sh
	@echo "Making LaTeX source of $*"
	$(MUTE)$(NBCONV) $(LTX_FLAGS) \
		--template=$(TPLX) \
		--template-file=index$(suffix $*).tex.j2 \
		--output=$(notdir $(subst .,_,$*).tex) $*.ipynb $(REDIRECT)
	$(MUTE)./utils/post.sh < $(subst .,_,$*).tex > $*.tmp && \
		mv $*.tmp $(subst .,_,$*).tex
	$(MUTE)touch $@ 

# Move file 
%_en.tex:%.en.tex
ifndef DOCKER_BUILD
	$(MUTE)if test -f $< ; then : else \
		$(MAKE) $(MAKEFLAGS) $< ; fi
endif

# Move file 
%_da.tex:%.da.tex
ifndef DOCKER_BUILD
	$(MUTE)if test -f $< ; then : else \
		$(MAKE) $(MAKEFLAGS) $< ; fi
endif

# First step from LaTeX to PDF
%.aux:%.tex nbi-python.sty
	@echo "Preparing for PDF of $*"
	$(MUTE)(cd $(dir $*) && $(XELATEX) $(LATEX_FLAGS) \
		$(notdir $*) $(LREDIRECT))

# Second step from LaTeX to PDF
%.pdf:%.aux
	@echo "Making PDF of $*"
	$(MUTE)(cd $(dir $*) && $(XELATEX)  $(LATEX_FLAGS) \
		$(notdir $*) $(LREDIRECT))
	$(MUTE)(cd $(dir $*) && $(XELATEX)  $(LATEX_FLAGS) \
		$(notdir $*) $(LREDIRECT))
# Create the title page 
%.cover.svg:%.ipynb
	@echo "Generating SVG cover for $*: $(call get_title, $<)"
	$(MUTE)sed 's/A sample title/$(call get_title, $<)/' \
		< imgs/cover.svg > $@

# Create image for cover 
%.pdf:%.svg
	@echo "Making PDF of SVG $*"
	$(MUTE)cairosvg -f pdf $< -o $@ $(REDIRECT)

# Create image for cover 
%.png:%.svg
	@echo "Making PNG of from SVG $*"
	$(MUTE)cairosvg -f png $< -o $@ $(REDIRECT)

# --------------------------------------------------------------------
# Plain HTML page
#
%.html:%.ex.ipynb mj.py
	@echo "Making web-page of $*"
	$(MUTE)$(NBCONV) $(HTML_FLAGS) $*.ipynb $(REDIRECT)

# --------------------------------------------------------------------
# Plain HTML for EPUB
#
# Needs
# - python3-selenium
# - python3-cairosvg
# - cairosvg
%.epub:%.ipynb %.cover.png
	@echo "Making E-Book of $* (Time out: $(EPUB_TIMEOUT))"
	$(MUTE)$(NBCONV) $(EPUB_FLAGS) \
		--EPUBExporter.cover=$*.cover.png \
		--EPUBExporter.title="$(call get_title, $<)" \
		--EPUBExporter.authors="$(call get_authors, $<)" \
		--EPUBExporter.publisher="$(call get_publisher, $<)" \
		--EPUBExporter.series="NBI Python" \
		--EPUBExporter.language="$(call get_language, $*)" \
		--EPUBExporter.render_timeout=$(EPUB_TIMEOUT) \
		$< 

#$(MUTE)mv $< $@

# Create the title page 
#%.epub.svg:%.epub.xhtml
#	@echo "Generating EPub cover for $*"
#	$(MUTE)sed 's/A sample title/$(shell sed -ne 's,.*<h1[^>]*>\([^<]*\)<a class=.tocSkip.*,\1,p' < $< | head -n 1)/' \
#		< imgs/cover.svg > $@


# --------------------------------------------------------------------
# Reveal slides
# 
%.slides.html:%.ex.ipynb
	@echo "Making slides of $*"
	$(MUTE)$(NBCONV) $(SLIDES_FLAGS) $*.ipynb $(REDIRECT)

# Python module 
%.py:%.ipynb
	@echo "Making Python module from $*"
	$(MUTE)$(NBCONV) $(PY_FLAGS) $< $(REDIRECT)


%-book.pdf:%.pdf
	pdfbook --a4paper \
		--landscape \
	    	--twoside \
	    	--booklet false \
	    	--signature 4 \
	    	--scale 1.05 \
	    	--delta '2cm 0cm' \
	    	-o $@ $<

# ====================================================================
#
# Default target 
# 
all:	code pdfs htmls

# This generated python code that specifies a Jinja filter that fixes
# the MathJax url to the setting in this Makefile.  It is a bit
# tricky, but it works.
mj.py:utils/mj.py.in
	@echo "Creating filter for setting MathJax URL - sigh"
	@sed s',@url@,$(MATHJAX),' < $< > $@

# --------------------------------------------------------------------
#
# Various targets
#
pdfs:	$(PDFS)
htmls:	$(HTMLS)
slds:	$(SLDS)
code:	$(CODES)
idx:	$(IDX:%.ipynb=%.da.html) $(IDX:%.ipynb=%.en.html)

stat:	
	$(MAKE) -j2 Statistik_en.pdf Statistik_da.pdf
best:	
	$(MAKE) -j2 BedsteVaner_en.pdf BedsteVaner_da.pdf
py:	
	$(MAKE) -j2 Python_en.pdf Python_da.pdf

epubs:	$(EPUBS)

show-slds:
	$(MUTE)$(foreach s,$(SLDS),echo "$(s)";)
show-htmls:
	$(MUTE)$(foreach s,$(HTMLS),echo "$(s)";)

UPDATEABLE = $(filter-out statistics/Statistik.update.ipynb,\
	 $(SRC:%.ipynb=%.update.ipynb) $(SRCNT:%.ipynb=%.update.ipynb)) \
	 statistics/Stat_00.update.ipynb

show_update:
	$(foreach p, $(UPDATEABLE), echo $(p);)

update:$(UPDATEABLE)
	$(foreach p,$^, mv $(p) $(p:%.update.ipynb=%.ipynb);)

update_idx:$(IDX:%.ipynb=%.update_idx.ipynb)
	$(foreach p,$^, mv $(p) $(p:%.update_idx.ipynb=%.ipynb);)


# --------------------------------------------------------------------
# Install into public (for Gitlab CI)
#
install:$(PDFS) $(HTMLS) $(NBS) $(SLDS) $(CODES)
	@echo "Installing generated files"
	$(MUTE)$(foreach f, $(PDFS) $(HTMLS) $(NBS) $(SLDS) $(CODES), \
	  cp $(f) public/$(f);)
	$(MUTE)cp custom.css public/
ifndef DOCKER_BUILD
	$(MUTE)$(foreach c, $(COURSES), \
		$(MAKE) -C $(c) DESTDIR=../../public $@;)
endif

# --------------------------------------------------------------------
# Generate and install API documentation
#
html:	$(CODES)
	$(MUTE)$(foreach a, $(APIS), \
		$(MAKE) -C $(subst _,-,$(a))-doc $@;)

install-html:html
	@echo "Installing HTML documentation"
	$(MUTE)$(foreach a, $(APIS), \
		rm -rf public/$(a) ; \
		cp -a $(subst _,-,$(a))-doc/.build/html public/$(a);)
	$(MUTE)$(foreach c, $(COURSES), \
		$(MAKE) -C $(c) DESTDIR=../../public $@ ;)

# --------------------------------------------------------------------
# Make loadable modules
#
statistics/nbi_stat.py:statistics/Statistik.ipynb utils/writelib.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py -t lib $< -o - > $@ 

statistics/nbi_pca.py:statistics/PCA.ipynb utils/writelib.py statistics/nbi_stat.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py -t lib $< -o - > $@ 

basics/nbi_sim.py:basics/ToyExpSim.ipynb utils/writelib.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py -t lib $< -o - > $@ 

basics/nbi_sim_exa.py:basics/ToyExpSim.ipynb utils/writelib.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py $< -t exa -i nbi_sim -o - > $@ 

basics/nbi_sim_test.py:basics/ToyExpSim.ipynb utils/writelib.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py $< -t test -i nbi_sim_exa -o - > $@ 

basics/nbi_sim_brahms.py:basics/ToyExpSim.ipynb utils/writelib.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py $< -t brahms -i nbi_sim_exa -o - > $@ 

basics/nbi_sim_onion.py:basics/ToyExpSim.ipynb utils/writelib.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py $< -t onion -i nbi_sim -o - > $@ 

mechanics/nbi_glauber.py:mechanics/Glauber.ipynb \
		utils/writelib.py statistics/nbi_stat.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py $< -t glauber -y 2020 \
		-a "Christian Holm Christensen"  "Zlatko Šaldić" -o - > $@ 


mechanics/nbi_d2ndetadphi.py:mechanics/d2Ndetadphi.ipynb \
		utils/writelib.py mechanics/nbi_glauber.py
	@echo "Writing Python $@ module from $<"
	$(MUTE)./utils/writelib.py $< -t d2ndetadphi -y 2021 \
		-a "Christian Holm Christensen"  "Zlatko Šaldić" -o - > $@ 

test_nbi_stat:nbi_stat.py tests/test_nbi_stat.py
	$(PYTHON) -m unittest discover -s tests 

#
# A reminder
# 
matplotlibrc:
	echo "backend : Agg" > $@


# --------------------------------------------------------------------
# Specific rules for Statistik (merging and templates)
#
statistics/Statistik_da.tex:		TPLX:=templates/latex/minereport
statistics/Statistik_en.tex:		TPLX:=templates/latex/minereport
statistics/Statistik.ipynb:		utils/merge.py $(STAT_SRC)
	@echo "Making collected $@"
	$(MUTE)./$< -o $@ $(STAT_SRC) $(REDIRECT) || (test -f $@ && rm -f $@)

statistics/Statistik.da.epub:		EPUB_TIMEOUT=300
statistics/Statistik.en.epub:		EPUB_TIMEOUT=300

# --------------------------------------------------------------------
# Specific rules for Notebook
#
basics/Notebook.da.tex basics/Notebook.en.tex: LTX_FLAGS=--to latex

# --------------------------------------------------------------------
# Specific rules for NewtonSecondLaw
#
misc/NewtonSecondLaw.tex:misc/NewtonSecondLaw.ipynb statistics/nbi_stat.py
	@echo "Making $@ from $<"
	$(MUTE)(cd misc && $(NBCONV) --no-input --to latex \
		$(notdir $<)) $(REDIRECT)

misc/NewtonSecondLaw.pdf:misc/NewtonSecondLaw.tex
	$(MUTE)(cd misc && xelatex $(LATEX_FLAGS) $(notdir $<)) $(LREDIRECT)


# --------------------------------------------------------------------
# Specific rules for PCA
#
statistics/PCA.ipynb       :statistics/nbi_stat.py
statistics/PCA.tex         :statistics/PCA.ex.ipynb statistics/nbi_stat.py
statistics/PCA.html        :statistics/PCA.ex.ipynb statistics/nbi_stat.py
statistics/PCA.slides.html :statistics/PCA.ex.ipynb statistics/nbi_stat.py

# --------------------------------------------------------------------
# Specific rules for Glauber
#
mechanics/Glauber.ipynb	   :statistics/nbi_stat.py
mechanics/Glauber.tex	   :mechanics/Glauber.ex.ipynb statistics/nbi_stat.py
mechanics/Glauber.html	   :mechanics/Glauber.ex.ipynb statistics/nbi_stat.py

# --------------------------------------------------------------------
# Specific rules for D2ndetadphi
#
mechanics/d2Ndetadphi.ipynb :statistics/nbi_stat.py mechanics/nbi_glauber.py
mechanics/d2Ndetadphi.tex   :mechanics/d2Ndetadphi.ex.ipynb \
			     statistics/nbi_stat.py mechanics/nbi_glauber.py
mechanics/d2Ndetadphi.html  :mechanics/d2Ndetadphi.ex.ipynb \
			     statistics/nbi_stat.py mechanics/nbi_glauber.py

# --------------------------------------------------------------------
# Specific rules for DoesAndDonts
#
misc/DoesAndDonts.ipynb    :statistics/nbi_stat.py
misc/DoesAndDonts.tex      :misc/DoesAndDonts.ex.ipynb statistics/nbi_stat.py
misc/DoesAndDonts.html     :misc/DoesAndDonts.ex.ipynb statistics/nbi_stat.py

# --------------------------------------------------------------------
# Specific rules for courses/appstat/PythonIntro
#
courses/appstat/PythonIntro.ex.ipynb:	NB_FLAGS:=$(NB_FLAGS) --allow-errors

# --------------------------------------------------------------------
# Specific rules for courses/appstat/PlottingAndFitting
#
courses/appstat/PlottingAndFitting.ex.ipynb:	\
	courses/appstat/PlottingAndFitting.ipynb \
	statistics/nbi_stat.py

# ====================================================================
# Glauber calculations
#
PbPb_%.npy:mechanics/nbi_glauber.py
	xsec=$(word 1,$(subst _, ,$*)); \
	dxsec=$(word 2,$(subst _, ,$*)); \
	xmodel=$(word 3,$(subst _, ,$*)); \
	profile=$(word 4,$(subst _, ,$*)); \
	python3 mechanics/nbi_glauber.py Pb Pb -b 22 \
		-x $${xsec} -X $${dxsec} -p $${profile} -m $${xmodel} \
		-n 10000 -r 1000 -o $@

PbPb_%.png:PbPb_%.npy mechanics/plot_glauber.py
	python3 mechanics/plot_glauber.py $< -o $@

glauber: PbPb_70_5_fixed_blackdisc.png		\
	 PbPb_70_5_fixed_defuse.png  		\
	 PbPb_70_5_fixed_normal.png  		\
	 PbPb_70_5_fixed_simple.png		\
	 PbPb_70_5_gribov_blackdisc.png		\
	 PbPb_70_5_gribov_defuse.png  		\
	 PbPb_70_5_gribov_normal.png  		\
	 PbPb_70_5_gribov_simple.png		\
	 PbPb_70_5_color_blackdisc.png		\
	 PbPb_70_5_color_defuse.png  		\
	 PbPb_70_5_color_normal.png  		\
	 PbPb_70_5_color_simple.png

# ====================================================================
# Clean-up
#
clean:
	@echo "Cleaning"
	$(MUTE)rm -f *~ *.aux *.log *.out *.toc *.synctex* *.tmp *.pyc .*.log *.npy *.png
	$(MUTE)rm -f output.ipynb None*.png mj.py
	$(MUTE)rm -f nbi_stat.py statistics/Statistik.ipynb
	$(MUTE)rm -f statistics/nbi_pca.py
	$(MUTE)rm -f $(PDFS)
	$(MUTE)rm -f $(HTMLS)
	$(MUTE)rm -f $(TNBS)
	$(MUTE)rm -f $(SLDS)
	$(MUTE)rm -f $(CODES)
	$(MUTE)rm -f $(EPUBS)
	$(MUTE)rm -f $(EPUBS:%.epub=%.cover.png)
	$(MUTE)rm -f $(PDFS:%.pdf=%.tex)
	$(MUTE)rm -f $(SRC:%.ipynb=%.tex) 
	$(MUTE)rm -f $(SRC:%.ipynb=%.da.tex) 
	$(MUTE)rm -f $(SRC:%.ipynb=%.en.tex) 
	$(MUTE)rm -f $(SRC:%.ipynb=%.py)
	$(MUTE)rm -f $(SRC:%.ipynb=%.pdf)
	$(MUTE)rm -f index.da.* index.en.* public/index.en.* public/index.da.*
	$(MUTE)rm -f $(addprefix public/,$(HTMLS))
	$(MUTE)rm -f $(addprefix public/,$(SLDS))
	$(MUTE)rm -f $(DIRS:%=%/danish.ldf)
	$(MUTE)rm -f public/custom.css matplotlibrc
	$(MUTE)rm -f templates/latex/*~ templates/html/*~ *.slides.html
	$(MUTE)rm -f utils/*~ misc/*~ statistics/*~ tests/*~
	$(MUTE)rm -f .gitignore~ danish.ldf
	$(MUTE)rm -f misc/*Eratosthenes*.hh misc/*Eratosthenes*.cc
	$(MUTE)rm -f misc/*Eratosthenes*.cc.out
	$(MUTE)rm -f mathematics/.png
	$(MUTE)rm -f basics/counts.dat basics/counts.csv
	$(MUTE)rm -f basics/data.dat basics/mymodule.py
	$(MUTE)rm -f mechanics/None0000000.png
	$(MUTE)(cd public && \
		rm -f $(PDFS) $(HTMLS) $(NBS) nbi_stat.py *~)
	$(MUTE)rm -f *~ */*.aux */*.log */*.out */*.toc */*.synctex* */*.tmp \
		*/*.pyc */.*.log
	$(MUTE)find . -name "*.slides.html" | xargs rm -f 
	$(MUTE)find . -name "*.da.*" -or -name "*.en.*"|grep -v j2 | xargs rm -f
	$(MUTE)find . -name "*_da.*" -or -name "*_en.*"|grep -v j2 | xargs rm -f
	$(MUTE)find . -name "*.ex.ipynb" | xargs rm -f 
	$(MUTE)rm -f */*-book.pdf 
	$(MUTE)$(foreach a, $(APIS), $(MAKE) -C $(subst _,-,$(a))-doc $@;)
	$(MUTE)$(foreach c, $(COURSES), \
		$(MAKE) -C $(c) DESTDIR=../../public $@;)
	$(MUTE)rm -rf   pkg/nbi_stat/build \
			pkg/nbi_stat/nbi_stat \
			pkg/nbi_stat/dist \
			pkg/nbi_stat/deb_dist \
			pkg/nbi_stat/nbi_stat.egg-info \
			pkg/nbi_stat/nbi_stat/__init__.py
	$(MUTE)rm -rf   pkg/nbi_pca/build \
			pkg/nbi_pca/nbi_pca \
			pkg/nbi_pca/dist \
			pkg/nbi_pca/deb_dist \
			pkg/nbi_pca/nbi_pca.egg-info \
			pkg/nbi_pca/nbi_pca/__init__.py

realclean: clean 
	@echo "Cleaning for real"
	$(MUTE)rm -rf $(DIRS:%=%/__pycache__)
	$(MUTE)rm -rf $(DIRS:%=%/auto)
	$(MUTE)rm -rf $(DIRS:%=%/*_files)
	$(MUTE)rm -rf utils/__pycache__
	$(MUTE)rm -rf __pycache__
	$(MUTE)rm -rf $(APIS:%=public/%*)
	$(MUTE)rm -rf .*.log 
	$(MUTE)$(foreach a, $(APIS), $(MAKE) -C $(subst _,-,$(a))-doc $@;)
	$(MUTE)$(foreach c, $(COURSES), \
		$(MAKE) -C $(c) DESTDIR=../../public $@;)

# ====================================================================
# Docker testing (to test for Gitlab CI)
#
docker-prep:
	@echo "Preparing for Docker"
	pip install --upgrade --user -r requirements.txt # $(REDIRECT)
	PATH=$(PATH):$(HOME)/.local/bin ./utils/postBuild      # $(AREDIRECT)

# NJOB	:= $(shell expr `nproc` - 1)
docker-build: docker-prep
	@echo "Building in Docker"
	$(MUTE)cp utils/danish.ldf . 
	make install \
		LATEX_FLAGS:="-interaction=nonstopmode" \
		REVEAL="$(REVEAL_REM)" \
		JQUERY="$(JQUERY_REM)" \
		REQUIREJS="$(REQUIREJS_REM)" \
		MATHJAX="$(MATHJAX_REM)" \
		JEXT="$(JEXT_DOCKER)" \
		PATH=$(PATH):$(HOME)/.local/bin
	make install-html \
		SPHINX=sphinx-build \
		LATEX_FLAGS:="-interaction=nonstopmode" \
		JEXT="$(JEXT_DOCKER)" \
		JQUERY="$(JQUERY_REM)" \
		REQUIREJS="$(REQUIREJS_REM)" \
		MATHJAX="$(MATHJAX_REM)" \
		PATH=$(PATH):$(HOME)/.local/bin
	rm -f danish.ldf # $(AREDIRECT)

docker_old:
	docker run --user root --group-add users -e GRANT_SUDO=yes -it --rm \
	  	-v $(PWD):/home/jovyan/$(notdir $(PWD)) \
	  	jupyter/scipy-notebook start.sh /bin/bash
docker:
	docker run -it --rm \
	  	-v $(PWD):/home/jovyan/$(notdir $(PWD)) \
	  	jupyter/scipy-notebook start.sh /bin/bash

docker-pypkg: docker-prep
	pip install --upgrade --user twine
	$(MAKE) pypkg

utils/ci-build.sh:
	echo "#!/bin/bash"  						>  $@
	echo "alias make=true" 						>> $@
	echo "pip install --upgrade --user -r requirements.txt" 	>> $@
	echo "./postBuild.sh"						>> $@
	echo "cp utils/danish.ldf ."					>> $@
	$(MAKE) --just-print -B install install-html SPHINX=sphinx-build \
		LATEX_FLAGS:="-interaction=nonstopmode" \
		| grep -v "make\[" >> $@

utils/ci-upload.sh:
	echo "#!/bin/bash"  		>  $@
	echo "alias make=true" 		>> $@
	echo "pip install --upgrade --user -r requirements.txt" 	>> $@
	echo "./postBuild.sh"						>> $@
	echo "cp utils/danish.ldf ."					>> $@
	echo "pip install --upgrade --user twine"			>> $@ 
	$(MAKE) --just-print -B pypi SPHINX=sphinx-build \
		LATEX_FLAGS:="-interaction=nonstopmode" \
		| grep -v "make\[" >> $@


# ====================================================================
# Deploy
#
%_pkg: statistics/%.py
	rm -rf pkg/$*/dist pkg/$*/$*
	mkdir -p pkg/$*/$*/
	cp $< pkg/$*/$*/__init__.py
	(cd pkg/$*/ && \
	  python3 setup.py sdist bdist_wheel)

pypkg: nbi_stat_pkg nbi_pca_pkg

pypi:	pypkg
	(cd pkg/nbi_stat/ && \
	  python3 -m twine upload $(PYPI) dist/nbi_stat-*)

epub-lxplus: $(EPUBS)
	scp $^ lxplus.cern.ch:~/eos/www/nbi-python/epub/

install-dev:	nbi_stat_pkg
	(cd pkg/nbi_stat && pip install --editable .)

.PRECIOUS:	%.ex.ipynb

#
# EOF
#
