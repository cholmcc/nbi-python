Installation
============

File download
-------------

The package consist if a single file `nbi_stat.py <../nbi_stat.py>`_
and you can simply download that file and put somewhere searched by
Python, for example in your projects directory.

Pip install
-----------

To install using ``pip``

.. code-block:: none
   
    pip install nbi_stat

See also

    https://pypi.org/project/nbi-stat
    

From Jupyter Notebook
---------------------

If you execute the notebook `Statistics Overview <../#Statistik>`_ it
will automatically generate the ``nbi_stat.py`` module.  Move that
file to somewhere searched by Python. 

From GitLab sources
-------------------

You can clone the repostory at

    https://gitlab.com/cholmcc/nbi-python

Note, you need some additional packages for this.  Please refer to the
``CONTRIBUTING.md`` file in the repository.
