Tutorials
=========

Please also see the `the examples notebook <../#nbi_stat_exa>` for a
Jupyter notebook using this module. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rounding
   means
   visual
   propagation
   histograms
   fitting 
   random
