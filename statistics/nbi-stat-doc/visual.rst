Visualisation of samples and fits
=================================

The module :mod:`nbi_stat` provides various functions for visualising
data and fits.

One-dimensional samples
-----------------------

The drop-in function :meth:`nbi_stat.hist` will visualise a
one-dimensional sample as a histograms with uncertainties.

.. plot::
   :include-source:

   >>> import nbi_stat
   >>> 
   >>> x = np.random.normal(size=1000)
   >>> nbi_stat.hist(x)


.. plot::
   :include-source:

   >>> import nbi_stat
   >>>
   >>> x = np.random.exponential(size=1000)
   >>> b = np.geomspace(.01,10,10)
   >>> nbi_stat.hist(x,bins=b,fmt='none')
   >>> plt.yscale('log')
   

Multi-dimensional samples
-------------------------

The function :meth:`nbi_stat.corner_plot` allows visualisation of
multi-dimensional data by showing the distribution of each variable on
the diagonal, and correlations in the off-diagonal

.. plot::
   :include-source:

   >>> import nbi_stat
   >>>
   >>> x = np.random.normal(size=(100,4))
   >>> nbi_stat.corner_plot(x,names='alpha')


We can customise the plot by passing functions to do the visualisation

.. plot::
   :include-source:

   >>> import nbi_stat
   >>>
   >>> x = np.random.normal(size=(100,4))
   >>> nbi_stat.corner_plot(x,off=plt.hist2d,names=lambda i: f'$v_{{{i+1}}}$',
   ...                      off_kw={'alpha':.5,'cmap':'cool'})


The function can also visualise several data sets, each of which we
can customise

.. plot::
   :include-source:

   >>> import nbi_stat
   >>>
   >>> x = np.random.normal(size=(100,4))
   >>> y = np.random.normal(size=(100,4))
   >>> nbi_stat.corner_plot(x,'X',{'off':plt.hist2d,'off_kw':{'cmap':'Greys'}},
   ...                      y,{'dia_kw':{'fmt':'o'},'label':'Y'},
   ...                      legend=True, names=lambda i: f'$v_{{{i+1}}}$',
   ...                      off_kw={'alpha':.5})


Fit visualisation
-----------------

We can use the function :meth:`nbi_stat.fit_plot` to visualise data,
fitted function, and parameters. The idea is to make showing the
results easily without too much hassle. 

.. plot::
   :include-source:

   >>> import nbi_stat
   >>> data = np.array ([[22000 , 440, -4.017 ,  0.5],   
   ...                   [22930 , 470, -2.742 ,  0.25],   
   ...                   [23880 , 500, -1.1478 , 0.08], 
   ...                   [25130 , 530,  1.491 ,  0.09],   
   ...                   [26390 , 540,  6.873 ,  1.90]])
   >>> 
   >>> def f(omega,a,b): return a*omega + b/omega
   >>> 
   >>> omega,domega = data[:,0], data[:,1]
   >>> cott,dcott   = data[:,2], data[:,3]
   >>> p, cov = nbi_stat.curve_fit(f,omega,cott,(1,1),dcott,domega,
   ...                             absolute_sigma=True)
   >>> 
   >>> nbi_stat.fit_plot(omega, cott, dcott, f, p, cov, domega, 
   ...                  parameters=['a','b'],
   ...                  fit ={ 'label':'Fit'},
   ...                  data ={'fmt':'none','label':'Data'})

The plot produced is highly customisable.  The figure belows some
options. 

.. plot::
   :include-source:

   >>> import sys
   >>> sys.path.append('statistics')
   >>> import nbi_stat
   >>> import scipy as sp
   >>> import scipy.stats
   >>> 
   >>> b = np.linspace(-3,3,31)
   >>> s = np.random.normal(size=1000)
   >>> 
   >>> y,x,w,d = nbi_stat.histogram(s,b)
   >>> 
   >>> f = lambda x, *p: p[0]*sp.stats.norm.pdf(x,*p[1:])
   >>> 
   >>> yz = y[d>0]
   >>> xz = x[d>0]
   >>> wz = w[d>0]
   >>> dz = d[d>0]
   >>> 
   >>> p, cov = nbi_stat.fit(f,xz,yz,(1,0,1),dz)
   >>> 
   >>> fig, ax = plt.subplots(ncols=2,nrows=3,figsize=(10,10),
   ...                        sharex=True,sharey=True,
   ...                        gridspec_kw=dict(hspace=0,wspace=0))
   >>> 
   >>> kw = [{},
   ...       {'table':{'title':'Title',
   ...                 'loc':'upper left'},
   ...        'band':False},
   ...       {'data':{'fmt':'o','ls':'--','xerr':w/2,
   ...                'markerfacecolor':'none',
   ...                'label':'Data'},
   ...        'band':{'color':'tab:green'}},
   ...       {'data':{'fmt':'o','label':'Data'},
   ...        'fit': {'label':'fit'},
   ...        'chi2':False,'pvalue':False,
   ...        'parameters':['A',r'\mu',r'\sigma']
   ...       },
   ...       {'parameters':[{'scale':'auto'},
   ...                      {'scale':-2,'unit':'a.u'},
   ...                      {'scale':-2}]},
   ...       {'table':False,
   ...        'data':{'color':'k','fmt':'none'}}
   ...       ]
   >>> 
   >>> for a,k in zip(ax.ravel(),kw):
   ...     nbi_stat.fit_plot(x,y,d,f,p,cov,axes=a,**k)
   ...     a.set_ylim([None,600])
   ...     a.legend(loc='lower center')
   >>> 
   >>> fig.tight_layout()

We can plot confidence contours using
:meth:`nbi_stat.plot_nsigma_contour`

      
.. plot::
   :include-source:

   >>> import sys
   >>> sys.path.append('statistics')
   >>> import nbi_stat
   >>> import scipy as sp
   >>> import scipy.stats
   >>> 
   >>> b = np.linspace(-3,3,31)
   >>> s = np.random.normal(size=1000)
   >>> 
   >>> y,x,w,d = nbi_stat.histogram(s,b)
   >>> 
   >>> f = lambda x, *p: p[0]*sp.stats.norm.pdf(x,*p[1:])
   >>> 
   >>> yz = y[d>0]
   >>> xz = x[d>0]
   >>> wz = w[d>0]
   >>> dz = d[d>0]
   >>> 
   >>> p, cov = nbi_stat.fit(f,xz,yz,(1,0,1),dz)
   >>> 
   >>> print(p,cov)
   >>> 
   >>> nbi_stat.plot_nsigma_contour(p,cov,[1,2],pnames=['A',r'\mu',r'\sigma'])


   
