Robust online calculation of means, variances, and covariance
=============================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

:mod:`nbi_stat` defines a number of methods for robust calculation of
the sample means, variances, and covariance.

These algorithms provide

* A single pass through data to calculate mean *and* (co)variance.
  This also allows for observations to be discarded as soon as we are
  done with them - that is, we do not need to keep the *whole* sample
  around to calculate the statistics. 
* Stablity with respect to rounding errors normally endured by
  single-pass calculations of the (co)variance.

The "challenge"
---------------

A typical single pass calculation of the variance is done by
calculating the sum and sum of squares, and then evaluate

.. math::

   s^{2}_{x} =
   \frac{N}{N-\delta}\left[\frac{1}{N}\sum_{i=1}^{N} x_{i}^{2}
   - \left(\frac{1}{N}\sum_{i=1}^{N} x_{i}\right)^2\right]

where we may sum incrementally.   However, although mathematically
correct, suffers from *catastrophic cancellation* due to finite
machine precision, as illustrated in the example below

.. runblock:: pycon

   >>> def var(x,ddof=1):
   >>>     "Variance by difference - ddof=1 for unbiased"
   ...     n      = len(x)
   ...     sum_x  = sum(x)
   ...     sum_x2 = sum([xx**2 for xx in x])
   ...     avg_x  = sum_x / n
   ...     avg_x2 = sum_x2 / n
   ...     return n / (n-ddof) * (avg_x2 - avg_x**2)
   >>>
   >>> x = [4,7,13,16]
   >>> y = [xx + 1e8 for xx in x]
   >>> 
   >>> print(f'Variance of x: {var(x)}')
   >>> print(f'Variance of y: {var(y)}')

Since for :math:`c` a constant 

.. math ::

   \mathbb{V}[x+c] = \mathbb{V}[x]

we see that the calculation above fails.  Note, that the data above
*is* sorted, meaning we will gain nothing from the traditional
heuristic of sorting the data first.  A typical remedy is to require a
two-pass calculation

.. math ::

   s_{x}^{2} = \frac{1}{N-\delta}\sum_{i=1}^{N} (x_{i} - \overline{x})^{2}
   \qquad\overline{x} = \frac{1}{N}\sum_{i=1}^{N} x_{i}


where we *first* calculate the sample average, and *then* calculate
the sample average over the square residuals
:math:`r_i=x_i-\overline{x}`.

.. runblock:: pycon

   >>> def var(x,ddof=1):
   >>>     "Variance by difference - ddof=1 for unbiased"
   ...     n      = len(x)
   ...     sum_x  = sum(x)
   ...     avg_x  = sum_x / n
   ...     sum_r2 = sum([(xx-avg_x)**2 for xx in x])
   ...     return 1 / (n-ddof) * sum_r2
   >>>
   >>> x = [4,7,13,16]
   >>> y = [xx + 1e8 for xx in x]
   >>> 
   >>> print(f'Variance of x: {var(x)}')
   >>> print(f'Variance of y: {var(y)}')

This calculation works as we expect, but *requires* two passes.  This
is how, for example, *NumPy* calculates the variance, and thus require
that the *whole* sample is avaiable 

The algorithms present here alleviate *both* of these problems

- No catastrophic cancellation since numbers of equal magnitude are
  added at all times
- Single pass over data, allowing us to free up resources as quickly
  as possible. 


Unweighted samples
------------------

For unweighted samples of 1 or more dimensions, we use the function
:meth:`nbi_stat.welford_update`.

For example, for a 1 dimensional sample, we can do 

.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>> mean, var, n = 0, 0, 0
   >>> for _ in range(100):
   >>>     mean, var, n = nbi_stat.welford_update(numpy.random.normal(),
   >>>                                           mean,var,n)
   >>> 
   >>> print(mean[0],var[0],n)

Note, :meth:`nbi_stat.welford_update` (and it's sibling
:meth:`nbi_stat.west_update`) *always* returns arrays, even if the
states were initially declared as scalars.

The function :meth:`nbi_stat.welford_init` can help us declare our
data structure.  It needs the number of dimensions, and whether to
calculate the covariance matrix or just the variances.

For example, for a 3-dimensional sample where we calculate means and
variances, we can do 
      
.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>> state = nbi_stat.welford_init(3)
   >>> for _ in range(100):
   >>>     state = nbi_stat.welford_update(numpy.random.normal(size=3),*state)
   >>> 
   >>> print(state)

To also calculate the covariances, we must pass ``covar=True`` to
:meth:`nbi_stat.welford_init`.
      
.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>> state = nbi_stat.welford_init(3,covar=True)
   >>> for _ in range(100):
   >>>     state = nbi_stat.welford_update(numpy.random.normal(size=3),*state)
   >>> 
   >>> print(state)

Note, :meth:`nbi_stat.welford_init` *always* returns 3 variables

* ``mean`` - the means
* ``cv`` - (co)variances
* ``n`` - Counter

If :math:`N` is the number of dimensions (variables) declared, then 
``mean`` is :math:`N` vector, while ``cv`` is either :math:`N` or
:math:`N\times N` if ``covar`` is ``False`` or ``True``,
respectively.  ``n`` is always scalar. 
  
Merging statistics
^^^^^^^^^^^^^^^^^^

Suppose we have calculated the means and (co)variances of sample
:math:`A` and :math:`B`, and now we want to combine these to into
one.  We can do that using :meth:`nbi_stat.welford_merge`

.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>> 
   >>> a = nbi_stat.welford_init(3,covar=True)
   >>> b = nbi_stat.welford_init(3,covar=True)
   >>> 
   >>> for _ in range(100):
   ...     a = nbi_stat.welford_update(numpy.random.normal(size=3),*a)
   ...     b = nbi_stat.welford_update(numpy.random.normal(size=3),*b)
   >>>
   >>> print(a)
   >>> print(b)
   >>>
   >>> c = nbi_stat.welford_merge(*a,*b)
   >>> print(c) 

Object oriented interface
^^^^^^^^^^^^^^^^^^^^^^^^^

The class :class:`nbi_stat.Welford` provides an object oriented
interface to the Welford algorithm.  

.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>> 
   >>> w1 = nbi_stat.Welford(3,covar=True)
   >>> w2 = nbi_stat.Welford(3,covar=True)
   >>>
   >>> for _ in range(100):
   ...     w1 += numpy.random.normal(size=3)
   ...     w2 += numpy.random.normal(size=3)
   >>>
   >>> print(f'Sample 1 {w1.n} (mean,sem,covariance)\n{w1}')
   >>> print(f'Sample 2 {w2.n} (mean,sem,covariance)\n{w2}')
   >>>
   >>> w3 = w1 + w2
   >>>
   >>> print(f'Merged sample {w3.n} (mean,sem,covariance)\n{w3}')

Note, we can query the object for the *standard error on the mean*
(SEM) using the method :meth:`nbi_stat.Welford.sem`. 
   
Weighted samples
----------------

Weighted samples are samples of 1 or more dimensions where each
observation has a weight.   Weights come in different flavours though,
and the :meth:`nbi_stat.west_update` supports them all.

Here, we will use the function :meth:`nbi_stat.west_init` to
initialise our data structure, but as we saw above, it is also
possible to declare the variables directly.

The function :meth:`nbi_stat.west_init` *always* returns 5 variables

* ``mean`` - means
* ``cv`` - (co)variances
* ``sumw`` - sum of weights
* ``sumw2`` - sum of square weights (possibly ``None``)
* ``summw`` - sum of mean weights (possibly ``None``)

and these are *always* arrays.  The dimensionality of these variables
depends on the various parameters given to  :nbi_stat:`west_init`

.. _freq_weights:

Frequency and non-frequency weights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Weights primarily comes in 2 flavours: *frequency* and *non-frequency*
weights.

* Frequency weights are positive whole numbers and represents the
  number of times a given value as seen.  That is,

  ..

      >>> mean,var,sumw,_ = nbi_stat.west_update(1,3,mean,var,sumw)

  and

  ..

      >>> mean,var,n = nbi_stat.welford_update(1,mean,var,n)
      >>> mean,var,n = nbi_stat.welford_update(1,mean,var,n)
      >>> mean,var,n = nbi_stat.welford_update(1,mean,var,n)

  are equivalent.  This kind of weights is assumed if no ``sumw2`` is
  ``None``.  However, here we will use :meth:`nbi_stat.west_init` to
  help us out. 

  .. runblock:: pycon

     >>> import sys; sys.path.insert(0,"..") # ignore
     >>> import nbi_stat # ignore
     >>> import numpy # ignore
     >>> numpy.random.seed(123456) # ignore
     >>> state = nbi_stat.west_init(1,frequency=True)
     >>> for _ in range(100):
     >>>     state = nbi_stat.west_update(numpy.random.normal(),
     >>>                                  numpy.random.poisson(3),*state)
     >>> 
     >>> mean, var, sumw, *_ = state
     >>> print(f'Mean        {mean[0]:.3f}\n'
     ...       f'Variance    {var[0]:.3f}\n'
     ...       f'Sum weights {sumw[0]:.3f}')

* Non-frequency weights are positive, real numbers.  These may
  represent, for example efficiencies, acceptance, fiducial values, or
  other such quantities.  This interpretation is triggered by passing
  a ``sumw2`` argument to :meth:`nbi_stat.west_update` or
  ``frequency=False`` to :meth:`nbi_stat.west_init` 

  .. runblock:: pycon

     >>> import sys; sys.path.insert(0,"..") # ignore
     >>> import nbi_stat # ignore
     >>> import numpy # ignore
     >>> numpy.random.seed(123456) # ignore
     >>> state = nbi_stat.west_init(1,frequency=False)
     >>> for _ in range(100):
     >>>     state = nbi_stat.west_update(numpy.random.normal(),
     >>>                                  numpy.random.random(),*state)
     >>>
     >>> mean, var, sumw, sumw2, _ = state
     >>> print(f'Mean                {mean[0]:.3f}\n'
     ...       f'Variance            {var[0]:.3f}\n'
     ...       f'Sum weights         {sumw[0]:.3f}')
     ...       f'Sum squared-weights {sumw2[0]:.3f}')

  
Multi-dimensional samples
^^^^^^^^^^^^^^^^^^^^^^^^^

We can also use :meth:`nbi_stat.welford_update` and
:meth:`nbi_stat.west_update` to calculate the individual component
(weighted) means and (co)variances of a multi-dimensional sample.  To
do that, we simply pass an array of observations and appropriate sized
states.

We can, as for the 1-dimensional case, distinguish between *frequency*
and *non-frequency* weights by not specifying or specifying the
``sumw2`` argument or passing the ``frequency`` keyword to
:meth:`nbi_stat.west_init`. 

.. _obs_weights:

Observation and component weights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

However, for multi-dimensional weighted samples, we *also* (that is,
in *addition*) distinguish between *observation* and *component* (or
element) weights.

* Observation weights are either frequency or non-frequency weights
  that apply to the "event" *as a whole*.  That is, if all components
  of a given "event" have the same weight, we pass a single value for
  the weight.

  Let's take an example of *frequency, observation* weights.  

  .. runblock:: pycon

     >>> import sys; sys.path.insert(0,"..") # ignore
     >>> import nbi_stat # ignore
     >>> import numpy # ignore
     >>> numpy.random.seed(123456) # ignore
     >>> state = nbi_stat.west_init(2,frequency=True,component=False)
     >>> 
     >>> for _ in range(100):
     >>>     state = nbi_stat.west_update(numpy.random.normal(size=2),
     >>>                                  numpy.random.poisson(3),*state)
     >>> 
     >>> mean, var, sumw, *_ = state
     >>>
     >>> numpy.set_printoptions(precision=3, suppress=True)
     >>> print(f'Mean         {mean}\n'
     ...       f'Variance:    {var}\n'
     ...       f'Sum weights: {sumw[0]:.3f}')

  To enable non-frequency weights we need to pass ``sumw2``.  To make
  life easier, :meth:`nbi_stat.west_init` helps us get the data
  structure right 

  .. runblock:: pycon

     >>> import sys; sys.path.insert(0,"..") # ignore
     >>> import nbi_stat # ignore
     >>> import numpy # ignore
     >>> numpy.random.seed(123456) # ignore
     >>> state = nbi_stat.west_init(2,frequency=False,component=False)
     >>> 
     >>> for _ in range(100):
     >>>     state = nbi_stat.west_update(numpy.random.normal(size=2),
     >>>                                  numpy.random.random(),*state)
     >>> 
     >>> mean, var, sumw, sumw2, _ = state
     >>> numpy.set_printoptions(precision=3, suppress=True)
     >>> print(f'Mean                 {mean}\n'
     ...       f'Variance:            {var}\n'
     ...       f'Sum weights:         {sumw[0]:.3f}\n'
     ...       f'Sum squared-weights: {sumw2[0]:.3f}')

* Component weights are either frequency or non-frequency weights that
  apply to the *individual* components (or elements) of an "event".
  These are then passed as an array to :meth:`nbi_stat.west_update`. 

  First, let's do *frequency, component* weights.  We need to pass an
  appropriate sized state for ``sumw`` to enable this, and we of course
  need to give as many weights as there are components in the
  observation. 

  .. runblock:: pycon

     >>> import sys; sys.path.insert(0,"..") # ignore
     >>> import nbi_stat # ignore
     >>> import numpy # ignore
     >>> numpy.random.seed(123456) # ignore
     >>> state = nbi_stat.west_init(2,frequency=True,component=True)
     >>> 
     >>> for _ in range(100):
     >>>     state = nbi_stat.west_update(numpy.random.normal(size=2),
     >>>                                  numpy.random.poisson(3,size=2),*state)
     >>> 
     >>> mean, var, sumw, *_ = state
     >>> numpy.set_printoptions(precision=3, suppress=True)
     >>> print(f'Mean         {mean}\n'
     ...       f'Variance:    {var}\n'
     ...       f'Sum weights: {sumw}')
  

  For *non-frequency, component* weights we also need to pass an
  appropriately sized ``sumw2``

  .. runblock:: pycon

     >>> import sys; sys.path.insert(0,"..") # ignore
     >>> import nbi_stat # ignore
     >>> import numpy # ignore
     >>> numpy.random.seed(123456) # ignore
     >>> state = nbi_stat.west_init(2,frequency=False,component=True)
     >>> for _ in range(100):
     >>>     state = nbi_stat.west_update(numpy.random.normal(size=2),
     >>>                                  numpy.random.random(size=2),*state)
     >>> 
     >>> mean, var, sumw, sumw2, _ = state
     >>> numpy.set_printoptions(precision=3, suppress=True)
     >>> print(f'Mean                 {mean}\n'
     ...       f'Variance:            {var}\n'
     ...       f'Sum weights:         {sumw}\n'
     ...       f'Sum squared-weights: {sumw2}')
  

Thus, for multi-dimensional samples, we have the following matrix of
weight types

.. _multidim_weights:
.. table:: Weight type combinations for multi-dimensional samples

   +-------------+-------------------+--------------------+
   |             | Frequency         | Non-frequency      |
   +-------------+-------------------+--------------------+
   | Observation | - ``sumw`` scalar | - ``sumw`` scalar  |
   |             | - ``sumw2=None``  | - ``sumw2`` scalar | 
   +-------------+-------------------+--------------------+
   | Component   | - ``sumw`` array  | - ``sumw`` array   |
   |             | - ``sumw2=None``  | - ``sumw2`` array  | 
   +-------------+-------------------+--------------------+

Covariance
^^^^^^^^^^

Here, we distinguish between *observation* and *component* weights, as
well as *frequency* and *non-frequency* weights (see also
:ref:`multidim_weights` and surrounding discussion).
     
Let's take as the single example *non-frequency, component* weights
and calculate the means and covariance.  For this particular case, we
need to pass ``summw`` - an array of the size of the input. Remember,
that the variance is of each component is the corresponding diagonal
element of the covariance matrix

.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>> state = nbi_stat.west_init(2,covar=True,frequency=False,component=True)
   >>> for _ in range(100):
   >>>     state = nbi_stat.west_update(numpy.random.normal(size=2),
   >>>                                  numpy.random.random(size=2),
   >>>                                  *state)
   >>> 
   >>> mean, covar, sumw, sumw2, summw = state
   >>> numpy.set_printoptions(precision=3, suppress=True)
   >>> print(f'Means: {mean}\n'
   ...       f'Covariances:\n{covar}\n'
   ...       f'Sum weights:\n{sumw}\n'
   ...       f'Sum squared weights:\n{sumw2}\n'
   ...       f'Sum mean weights:\n{summw}')

Note, for *component* weights, the sum of weights and squared-weights
are :math:`N\times N` matrices, while ``summw`` is an :math:`N`
vector.

Merging sample statistics
^^^^^^^^^^^^^^^^^^^^^^^^^

Suppose we have calculated the means and (co)variances of sample
:math:`A` and :math:`B`, and now we want to combine these to into
one.  We can do that using :meth:`nbi_stat.west_merge`

.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>>
   >>> opts = dict(covar=True,frequency=True,component=False)
   >>> a = nbi_stat.west_init(3,**opts)
   >>> b = nbi_stat.west_init(3,**opts)
   >>>
   >>> def obs():
   ...     return numpy.random.normal(size=3),numpy.random.poisson(3)
   >>>
   >>> for _ in range(100):
   ...     a = nbi_stat.west_update(*obs(),*a)
   ...     b = nbi_stat.west_update(*obs(),*b)
   >>>
   >>> print(a)
   >>> print(b)
   >>>
   >>> c = nbi_stat.west_merge(*a,*b)
   >>> print(c) 

Object oriented interface
^^^^^^^^^^^^^^^^^^^^^^^^^

The class :class:`nbi_stat.West` provides an object oriented
interface to the West algorithm.  

.. runblock:: pycon
	      
   >>> import sys; sys.path.insert(0,"..") # ignore
   >>> import nbi_stat # ignore
   >>> import numpy # ignore
   >>> numpy.random.seed(123456) # ignore
   >>>
   >>> opts = dict(frequency=True,component=False,covar=True)
   >>> w1 = nbi_stat.West(3,**opts)
   >>> w2 = nbi_stat.West(3,**opts)
   >>>
   >>> def obs():
   ...     return numpy.random.normal(size=3),numpy.random.poisson(3)
   >>>
   >>> for _ in range(100):
   ...     w1 += obs()
   ...     w2 += obs()
   >>>
   >>> print(f'Sample 1 {w1.sumw} (mean,sem,covariance)\n{w1}')
   >>> print(f'Sample 2 {w2.sumw} (mean,sem,covariance)\n{w2}')
   >>>
   >>> w3 = w1 + w2
   >>>
   >>> print(f'Merged sample {w3.sumw} (mean,sem,covariance)\n{w3}')

Note, we can query the object for the *standard error on the mean*
(SEM) using the method :meth:`nbi_stat.West.sem`. 
   
..  LocalWords:  fiducial
