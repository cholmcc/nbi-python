.. NBI Stat documentation master file, created by
   sphinx-quickstart on Fri Dec  7 23:58:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NBI Stat's documentation!
====================================

Here, you will find various Python functions for statistical analysis,
as described in the `Statistics Jupyter Notebook <../#Statistik>`_

.. toctree::
   :maxdepth: 2

   install	      
   tutorials	      
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
