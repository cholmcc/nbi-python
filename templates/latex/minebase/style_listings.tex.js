%% ===================================================================
%%
%% Macro to generate unique identifiers
%% 
%% Sequence of random integers 
((*- macro random_int(len) -*))
  ((*- for n in range(len) -*))
    ((( [0,1,2,3,4,5,6,7,8,9]|random )))
  ((*- endfor -*))
((*- endmacro -*))

%% Unique identifier 
((*- macro unique_id(count_groups=5, group_len=6, separator='') -*))
  ((*- set parts -*))
    ((*- for n in range(count_groups) -*))
     ((( random_int(group_len) )))
    ((*- endfor -*))
  ((*- endset -*))
  ((( parts|join(separator) )))
((*- endmacro -*))

%% ===================================================================
%%
%% Header template
%%
((* block header *))
  ((*- block docclass    -*))((*- endblock docclass -*))
  ((*- block packages    -*))((*- endblock packages -*))
  ((*- block definitions -*))((*- endblock definitions -*))
  ((*- block commands    -*))((*- endblock commands -*))
  ((*- block custom      -*))((*- endblock custom -*))
((* endblock header *))

%% ===================================================================
%% Packages to include
%%
%% - + iftex 
%% - + if pdf mathpazo else fontspec
%% - + graphicx
%% - caption (sets no caption)
%% - + float
%% - + xcolor
%% - + geometry
%% - + amsmath
%% - + amssymb
%% - + textcomp
%% - + upquote
%% - + eurosym
%% - + ucs, option mathletters
%% - - fancyvrb
%% - + adjustbox - option Export
%% - + hyperref
%% - - titling
%% - + longtable
%% - + booktabs
%% - - enumitem - option inline
%% - + ulem - option normalem
%% - - mathrsfs
%% 
%% We also need
%% 
%% - ifxetex ifluatex
%% - amstext
%% - if not xetex inputenx w/option utf else fontspec
%% - hypcap
%% - calc
%% - longfbox
%% - framed
%% - ltablex
%% - listings
%% - environment
((*- block packages -*))
\usepackage{ifpdf,ifxetex,ifluatex}  %% Engine identification
\usepackage{mathpazo}                %% Nicer fonts
\usepackage{graphicx}                %% Graphics
\usepackage{svg}                     %% SVG graphics
\usepackage{xcolor}                  %% Colours
\usepackage{geometry}                %% Page layout
\usepackage{amsmath}                 %% Maths
\usepackage{amstext}                 %% Maths in text
\usepackage{amssymb}                 %% Maths symbols
\usepackage{textcomp}                %% Various text commands
\usepackage{upquote}                 %% Various text commands
\usepackage{eurosym}                 %% Euro symbols
\usepackage[mathletters]{ucs}        %% Math font
\ifxetex
  \usepackage{fontspec}              %% Font encoding or input encoding 
\else
  \usepackage[utf8]{inputenx}        %% Font encoding or input encoding   
\fi
\usepackage{hyperref}                %% Hyper reference for PDF output
\usepackage{hypcap}                  %% Hyper reference for PDF output  
\usepackage{calc}                    %% Calculations
\usepackage{float}		     %% Float optimisation
\usepackage{longfbox}		     %% Long frame box
\usepackage{longtable}		     %% Long table 
\usepackage{framed}		     %% Framed boxes
\usepackage{booktabs}		     %% 
\usepackage{ltablex}                 %%  
\usepackage{adjustbox}               %% Adjusting box sizes (mainly graphics)
\usepackage{listings}                %% Code listings   
\usepackage{environ}                 %% Modify environments
((*- endblock packages -*))

%% ===================================================================
%%
%% Definitions
%%
%% Various definitions.  Typically split into blocks that are related
%% occationally with sub-blocks that can be overloaded to customize 
((*- block definitions -*))
  ((*- block definitions_input    -*))((*- endblock definitions_input -*))
  ((*- block definitions_output   -*))((*- endblock definitions_output -*))
  ((*- block definitions_captions -*))((*- endblock definitions_captions -*))
  ((*- block definitions_headings -*))((*- endblock definitions_headings -*))
  ((*- block definitions_compat   -*))((*- endblock definitions_compat -*))
((*- endblock definitions -*))

%% -------------------------------------------------------------------
((*- block definitions_input -*))
  ((*- block input_colors      -*))((*- endblock input_colors -*))
  ((*- block input_language    -*))((*- endblock input_language -*))
  ((*- block input_inline      -*))((*- endblock input_inline -*))
  ((*- block input_environment -*))((*- endblock input_environment -*))
  ((*- block input_exported    -*))((*- endblock input_exported -*))
((*- endblock definitions_input -*))

%% ___________________________________________________________________
((*- block input_color -*))
\colorlet{jp@keywordcolor}{green!50!black}
\colorlet{jp@identifiercolor}{blue!50!black}
\colorlet{jp@stringcolor}{brown!50!black}
\colorlet{jp@commentcolor}{red!50!black}
\colorlet{jp@inputbgcolor}{yellow!5}
\colorlet{jp@inputframecolor}{lightgray}
\colorlet{jp@@keywordcolor}{cyan!50!black}
\colorlet{jp@@@keywordcolor}{magenta!50!black}
((*- endblock input_color -*))

%% ___________________________________________________________________
((*- block input_language -*))
\lstloadlanguages{Python}
\lstdefinestyle{jpinput}{
  language=Python,
  frame=single,
  basicstyle=\ttfamily\footnotesize,
  keywordstyle=\color{jp@keywordcolor}\bfseries,
  identifierstyle=\color{jp@identifiercolor}\slshape,
  stringstyle=\color{jp@stringcolor},
  commentstyle=\color{jp@commentcolor}\itshape,
  backgroundcolor=\color{jp@inputbgcolor},
  rulecolor=\color{jp@inputframecolor},
  inputencoding=utf8,
  belowskip=0pt,
  morekeywords={as,with,assert},
  morekeywords=[2]{bytes,BaseException,Exception,ArithmeticError,BufferError,
    LookupError,AssertionError,AttributeError,EOFError,FloatingPointError,
    GeneratorExit,ImportError,ModuleNotFoundError,IndexError,KeyError,
    KeyboardInterrupt,MemoryError,NameError,NotImplementedError,
    OSError,OverflowError,RecursionError,ReferenceError,RuntimeError,
    StopIteration,StopAsyncIteration,SyntaxError,IndentionError,TabError,
    SystemError,SystemExit,TypeError,UnboudLocalError,UnicodeError,
    UnicodeEncodeError,UnicodeDecodeError,UnicodeTranslateError,
    ValueError,ZeroDivisionError,EnvironmentError,IOError,WindowsError,
    BlockingIOError,ChildProcessError,ConnectionError,BrokenPipeError,
    ConnectionAbortedError,ConnectionRefusedError,FileExistsError,
    FileNotFoundError,InterruptError,IsADirectoryError,PermissionError,
    ProessLookupError,TimeoutError,Warning,UserWarning,DeprecationWarning,
    PendingDeprecationWarning,SyntaxWarning,RunetimeWarning,FutureWarning,
    ImportWarning,UnicodeWarning,BytesWarning,ResourceWarning},
  keywordstyle={[2]\color{jp@@keywordcolor}\bfseries},
  morekeywords=[3]{np,sp,sy,plt,nbi,ipy,
    numpy,scipy,matplotlib,pyplot,sympy,IPython},
  keywordstyle={[3]\color{jp@@@keywordcolor}\bfseries},
  numberstyle=\ttfamily\footnotesize,
  numberfirstline=true,
  stepnumber=1000}
((*- endblock input_language -*))

%% ___________________________________________________________________
((*- block input_inline -*))
\def\jpinline{\lstinline[style=jpinput]}
\let\texttt\jpinline
((*- endblock input_inline -*))

%% ___________________________________________________________________
((*- block input_environment -*))
\def\jp@inprompt{%
  In [\ifnum\c@lstnumber=0\relax\space\else\arabic{lstnumber}\fi]}
\let\othelstnumber\thelstnumber
\lstnewenvironment{jpinput}[1][]{%
  \let\thelstnumber\jp@inprompt%
  \lstset{style=jpinput,numberstyle=\texttt,#1}}{}
((*- endblock input_environment -*))

%% ___________________________________________________________________
((*- block input_exported -*))
\lstdefinestyle{jpexported}{rulecolor=\color{black}}  
((*- endblock input_exported -*))

%% -------------------------------------------------------------------
((*- block definitions_output -*))
  ((*- block output_colors      -*))((*- endblock output_colors -*))
  ((*- block output_language    -*))((*- endblock output_language -*))
  ((*- block output_environment -*))((*- endblock output_environment -*))
  ((*- block output_latex_env   -*))((*- endblock output_latex_env -*))
((*- endblock definitions_output -*))


%% ___________________________________________________________________
((*- block output_colors -*))
\colorlet{jp@outputbgcolor}{blue!5}
\colorlet{jp@outputframecolor}{lightgray}
\colorlet{jp@tracecolor}{red!5}
((*- endblock output_colors -*))

%% ___________________________________________________________________
((*- block output_language -*))
\lstdefinestyle{jpoutput}{
  language={},%
  extendedchars=true,%
  frame=single,%
  breaklines=true,%
  basicstyle=\ttfamily\small,%
  backgroundcolor=\color{jp@outputbgcolor},%
  rulecolor=\color{jp@outputframecolor},%
  inputencoding=utf8,%
  aboveskip=0pt,
  numberstyle=\ttfamily\footnotesize,
  numberfirstline=true,
  stepnumber=1000,
}
((*- endblock output_language -*))

%% ___________________________________________________________________
((*- block output_environment -*))
\newcommand\jp@outprompt{Out [\arabic{lstnumber}]}
\lstnewenvironment{jpoutput}[1][]{
  \let\thelstnumber=\jp@outprompt%
  \lstset{style=jpoutput,numberstyle=\texttt,#1}}{}
\lstnewenvironment{jptraceback}{
  \lstset{style=jpoutput,backgroundcolor=\color{jp@tracecolor}}}{}
((*- endblock output_environment -*))

%%___________________________________________________________________
((*- block output_latex_env -*))
\newenvironment{jpltxoutput}{%
  \begin{longfbox}[width=.999\linewidth,
    background-color=nbipy@execbgcolor,%
    margin-left=-.3em,
    margin-top=-.5\baselineskip, % 7pt,
    margin-bottom=1ex]}{%
  \end{longfbox}}
\renewenvironment{jpltxoutput}{%
  \center}{\endcenter}
((*- endblock output_latex_env -*))

%% -------------------------------------------------------------------
((*- block definitions_captions -*))
\let\jp@saved@caption\@empty
\newcommand*\savecaption[1]{\gdef\jp@saved@caption{#1}}
\def\usecaption{%
  \caption{\jp@saved@caption}
  \global\let\jp@saved@caption\@empty}
\newcommand\jpcodefigureref[1]{%
  {\begin{tabular}{p{\linewidth}}%
     \hfil(\figurename~\ref{#1})\hfil%
   \end{tabular}\\%
 }}
((*- endblock definitions_captions -*))

%% -------------------------------------------------------------------
((*- block definitions_headings -*))
\def\thetitle{}
\def\ps@jpheadings{%
  \let\@oddfoot\@empty
  \let\@evenfoot\@empty
  \def\@evenhead{\thepage\hfil\thetitle}
  \def\@oddhead{\rightmark\hfil\thepage}
  \def\chaptermark##1{%
    \markright{\ifnum \c@secnumdepth >\z@
      \thechapter\quad
      \fi
      ##1}}
  \def\sectionmark##1{%
    \markright{\ifnum \c@secnumdepth >\z@
      \thesection\quad
      \fi
      ##1}}
  \let\subsectionmark\@gobble}
\pagestyle{jpheadings}
((*- endblock definitions_headings -*))

%% -------------------------------------------------------------------
((*- block definitions_compat -*))
%% Various compatibility optionse
%% Change in template to override
\def\gt{>}
\def\lt{>}
\let\jp@TeX
\let\jp@LaTeX
\renewcommand{\TeX}{\textrm{\jp@TeX}}
\renewcommand{\LaTeX}{\textrm{\jp@LaTeX}}
((*- endblock definitions_compat -*))


%% ===================================================================
%% Various customisations
((*- block commands -*))
  ((*- block hyperref_setup -*))((*- endblock hyperref_setup -*))
  ((*- block geometry_setup -*))((*- endblock geometry_setup -*))
  ((*- block title          -*))((*- endblock title          -*))
  ((*- block date           -*))((*- endblock date           -*))
  ((*- block author         -*))((*- endblock author         -*))
((*- endblock commands -*))

%% -------------------------------------------------------------------
((*- block hyperref_setup -*))
% Override in template 
\definecolor{jp@urlcolor}{rgb}{0,.145,.698}
\definecolor{jp@linkcolor}{rgb}{.71,0.21,0.01}
\definecolor{jp@citecolor}{rgb}{.12,.54,.11}

\hypersetup{
  breaklinks=true,  % so long urls are correctly broken across lines
  colorlinks=true,
  urlcolor=jp@urlcolor,
  linkcolor=jp@linkcolor,
  citecolor=jp@citecolor,
}
((*- endblock hyperref_setup -*))

%% -------------------------------------------------------------------
((*- block geometry_setup *))
\geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in}
((*- endblock geometry_setup *))

%% -------------------------------------------------------------------
((*- block title -*))
((*- set nb_title = nb.metadata.get('title', '') or resources['metadata']['name'] -*))
\title{((( nb_title | escape_latex )))}
\newenvironment{titlesetup}{}{}
((*- endblock title -*))

%% -------------------------------------------------------------------
((* block date *))
\date{\today}
((* endblock date *))

%% -------------------------------------------------------------------
((* block author *))
  ((* if 'authors' in nb.metadata *))
  \author{((( nb.metadata.authors | join(', ', attribute='name') )))}
  ((* endif *))
((* endblock author *))

%% ===================================================================
%% Load custom package if present
%%
%% The user can do most stuff from that package 
((*- block custom -*))
\newcommand*\usepackageifpresent[2][]{%
  \IfFileExists{#2.sty}{\usepackage[#1]{#2}}{}}
\usepackageifpresent{custom}
((*- endblock custom -*))


%% ===================================================================
%%
%% Markdown cells
%%
%% - Cells with meta data 'toc' are considered to contain table of
%%   contents
%% - Cells with tag 'title' are considered to contain the title page
%% - In cells with meta data 'tocSkip' or tag 'tocSkip', sectioning
%%   commands are redefined (temporarily) to their stared versions
%% - Cell with tag 'nextcaption' are considered to be captions for the
%%   next figure. Cell content is stored in '\savedcaption'
%% - Cell with tag 'lol' will contaiin listing of codes
%% - Cell with tag 'nolatex' are ignored
%% - Other cell tags are turned into environments surrounding the
%%   cells content.  For example, if the cells has the tag 'foo',
%%   then the cells content will be surrounded by
%%   '\begin{foo} ... \end{foo}'.  Users can provide a 'custom.sty'
%%   package to define these environments. 
%%
((* block markdowncell scoped *))
((* if 'toc' in cell.metadata *))
  
  ((* block toc *))
   ((* set tocname = nb.metadata.get('toc','').get('title_cell','Contents') *))
\tableofcontents
\cleardoublepage
\pagenumbering{arabic}
  ((* endblock toc *))
 ((* elif 'title' in cell.metadata.get('tags',{}) *))
\begingroup
\titlesetup
     ((( super() )))
\endtitlesetup
\endgroup
((* elif cell.metadata.get('tocSkip',false) or
         'tocSkip' in cell.metadata.get('tags',{}) *))
   \begingroup
    \let\ooldsection\section
    \let\ooldsubsection\subsection
    \let\ooldsubsubsection\subsubsection
    \def\section#1{\ooldsection*{#1}}
    \def\subsection#1{\ooldsubsection*{#1}}
    \def\subsubsection#1{\ooldsubsubsection*{#1}}
     ((( super() )))
\endgroup
 ((* elif 'nextcaption' in cell.metadata.get('tags',[]) *))
\savecaption{((( super() )))}
 ((* elif 'lol' in cell.metadata.get('tags',[]) *))
\lstlistoflistings
 ((* elif cell.metadata.get('nolatex',false) *))
   % Ignored output
 ((* else *))
   ((* set clss = cell.metadata.get('tags',[]) *))
   ((*- for cls in clss -*))
   ((( '\\begin{' ~ (cls | replace('-','_')) ~ '}')))
   ((*- endfor -*))
   ((( super() )))
   ((*- for cls in clss[::-1]  -*))
   ((( '\\end{' ~ (cls | replace('-','_')) ~ '}')))
   ((*- endfor -*))
  ((* endif *))
((* endblock markdowncell *))

%% ===================================================================
%%
%% Input cells (code cells) are put in the environment 'jpinput'
%%
%% - If the cell has tag 'hide_input' or 'nolatex', the are ignored
%% - If the cell has the tag 'title', then that title is set on the
%%   code listing, and a label 'lst:<title>' is defined. 
%% - If the cell has the tag 'export' then the listing will have
%%   the additional style 'jpexport'.  If not, the listing will have
%%   the tag 'nolol' to skip the listing of listings.
%% - If the resources specifies to include the prompt, then the first
%%   line number will be the input prompt
%% - If the cell has the tag 'maxlines', then only that number of
%%   lines will be output and the string 'input truncated' added 
%%
((*- block input scoped -*))
 ((* if cell.metadata.get('nolatex',false) or
        cell.metadata.get('hide_input',false) or
        'nolatex' in cell.metadata.get('tags',[]) or
        'hide_input' in cell.metadata.get('tags',[]) *))
  % Ignored input
 ((* else *))
  ((*- set arg = '' -*))
  ((*- if cell.metadata.get('title',false) -*))
   ((* set arg = ','.join([arg,'name={\\jpinline{' ~ (cell.metadata['title']) ~ '}},label=lst:' ~ (cell.metadata['title'])]) *))
  ((*- endif -*))
  ((*- if 'export' in cell.metadata -*))
   ((*- set arg = ','.join([arg, 'style=jpexport']) -*))
  ((*- else -*))
   ((* set arg = ','.join([arg,'nolol']) *))
  ((*- endif -*))  
  ((*- if resources.global_content_filter.include_input_prompt *))
    ((* set arg = ','.join([arg,'numbers=left','firstnumber=' ~ (cell.execution_count | replace(None, "0"))]) *))
  ((*- endif -*))
  ((*- set maxl = cell.metadata.get('maxlines',-1) -*))
\begin{jpinput}[(((arg)))]
  ((*- if maxl > 0 *))
((( '\n'.join(cell.source.split('\n')[:maxl]) )))
...
# Source code has been truncated
  ((* else *))
((( cell.source )))
  ((*- endif *))
\end{jpinput}
 ((*- endif -*))
((*- endblock input -*))

%% ===================================================================
%%
%% Override output types
%% (markdown as in base)

%% -------------------------------------------------------------------
%% Text output in environment jpoutput
((*- block data_text -*))
  ((*- set arg = '' -*))
  ((*- if resources.global_content_filter.include_output_prompt *))
    ((* set arg = ','.join([arg,'numbers=left','firstnumber=' ~ (cell.execution_count | replace(None, "0"))]) *))
  ((*- endif -*))
\begin{jpoutput}[(((arg)))]
((( output.data['text/plain'] )))
\end{jpoutput}
((*- endblock data_text -*))

%%-------------------------------------------------------------------
%% Output streams are set in the environment 'jpoutput'
%%
((* block stream *))
\begin{jpoutput}
((( output.text )))
\end{jpoutput}
((* endblock stream *))

%%-------------------------------------------------------------------
%% Output latex are set in the environment 'jpltxoutput'
%%
((* block data_latex *))
\begin{jpltxoutput}
((( output.data['text/latex'] )))
\end{jpltxoutput}	   
((* endblock data_latex *))

%% -------------------------------------------------------------------
%%
%% Tracebacks are set in the environment 'jptraceback'
%%
%% Lines with dashes only get an extra newline
%% A new line is put after the word 'Traceback'
%% 
((* block error *))
\begin{jptraceback}
  ((( '' )))
  ((*- for line in output.traceback -*))
     ((*- block traceback_line scoped -*))
       ((( line.replace('---------------------------------------------------------------------------','-----------------------------------------------------------------\n').replace('Traceback','\nTraceback') | strip_ansi )))
     ((*- endblock traceback_line -*))
  ((*- endfor -*))
\end{jptraceback}
((* endblock error *))

%%-------------------------------------------------------------------
%% HTML, widgets output ignored
((* block data_html         *))((* endblock data_html *))
((* block data_widget_view  *))((* endblock data_widget_view *))
((* block data_widget_state *))((* endblock data_widget_state *))
((* block data_other        *))((* endblock data_other *))

%%--------------------------------------------------------------------
%% Figures.  In all cases, we get the cell caption and/or label
%% from the cell metadata 
%%
%%____________________________________________________________________
%%
%% PNGs
((*- block data_png -*))
  ((( draw_figure(output.metadata.filenames['image/png'],cell.metadata.caption,cell.metadata.label) )))
((*- endblock -*))

%%____________________________________________________________________
%% JPGs
((*- block data_jpg -*))
  ((( draw_figure(output.metadata.filenames['image/jpeg'],cell.metadata.caption,cell.metadata.label) )))
((*- endblock -*))

%%____________________________________________________________________
%% SVGs
((*- block data_svg -*))
  ((( draw_figure(output.metadata.filenames['image/svg+xml'],cell.metadata.caption,cell.metadata.label) )))
((*- endblock -*))

%%____________________________________________________________________
%% PDFs
((*- block data_pdf -*))
  ((( draw_figure(output.metadata.filenames['application/pdf'],cell.metadata.caption,cell.metadata.label) )))
((*- endblock -*))


%% ===================================================================
%%
%% Output cells
%%
%% - Cells with 'nolatex' or 'hide_output' are ignored
%% - If cell mimetype is 'text/latex', put in the special environment
%%   'jpltxoutput' 
%% - If resource specifies to output prompt, make the prompt the first
%%   line
%% - 'application/javascript' is ignored
%% - Normal output is put in the environment 'jpoutput'
%%
((* block execute_result scoped *))
((* if cell.metadata.get('nolatex',false) or
       cell.metadata.get('hide_output',false) or
       'hide_output' in cell.metadata.get('tags',[]) or
       'nolatex' in cell.metadata.get('tags',[]) *))
  % Ignored output
 ((* else *))
  ((( super() )))
 ((* endif *))
((* endblock execute_result *))


%%====================================================================
%%
%% Macro to draw figures.  This takes into account saved captions
%%
%% - If notebook meta data contains 'always_float', then put in float
%% - If we have a caption or a label or a always_float, then put in
%%   figure.
%% - If we are making a float - either because that option was set in
%%   the metadata, or if we have caption, and  we do not have label,
%%   generate a unique one  
%% - If we have a label, make a reference to the figure at this point
%%   in the document (typically right after listing) 
%% - To support different languages, we check if the current language
%%   is set on the notebook 
%% - If not a float, just center the image
%% - Images are adjusted to take up a maximum of 90% of the line width
%%   or height. 
%%
%% Draw a figure using the graphicx package.
((*- macro draw_figure(filename,caption,label) -*))
  ((* set filename = filename | posix_path *))
  ((* set always_float = nb.metadata.get('latex',{}).get('always_float',false) *))
  ((*- block figure scoped -*))
    ((*- if caption or label or always_float -*))
      ((*- if (caption or always_float) and not label -*))
        ((*- set label = 'fig:'+((( unique_id() ))) -*))
      ((*- endif -*))
      ((*- if label -*))
        {\jpcodefigureref{((( label )))}}
      ((*- endif -*))
      ((*- if caption -*))
        ((*- set lng = nb.metadata.get('nbTranslate',{}).get('transLang','') -*)) 
        ((*- if caption is mapping -*))
          ((*- set cap = '\\caption{'+caption.get(lng,'')+'}' -*))
        ((*- else -*))
          ((*- set cap = '\\caption{'+caption+'}' -*))
       ((*- endif -*))
      ((*- endif -*))
      ((*- if not cap -*))
         ((*- set cap = '\\usecaption{}' -*))
      ((*- endif -*))
      \begin{figure}
        \capstart   
        \centering
        \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{((( filename )))}
        ((( cap )))
        \label{((( label )))}
      \end{figure}
    ((*- else -*))  
      \begin{center}
      \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{((( filename )))}
      \end{center}
      { \hspace*{\fill} \\}
    ((*- endif -*))
  ((*- endblock figure -*))
((*- endmacro -*))


{# Local Variables: #}
{#    mode: jinja2 #}
{# End: #}
