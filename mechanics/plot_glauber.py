import sys  
sys.path.append('statistics') 

import nbi_glauber
from matplotlib.pyplot import ion

if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser('Plot results')
    ap.add_argument('input',type=FileType('rb'),nargs='+',
                    help='Input files')
    ap.add_argument('-o','--out',help='Possible output file',
                    type=FileType('wb'),default=None)

    args = ap.parse_args()
    
    ion()
    nbi_glauber.file_summary_plots(*args.input,out=args.out)

