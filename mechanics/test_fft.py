from numpy import linspace, pi, cos, arange, array, newaxis, zeros_like, abs, angle, printoptions
from scipy.fft import *

def g(phi,vn,psin):
    n = arange(len(vn))+1

    return 1 + (vn[...,newaxis]*cos(n[...,newaxis]*(phi+psin[...,newaxis]))).sum(axis=0)

phi  = linspace(0,2*pi,30)
vn   = array([0.01,0.05,0.03])
psin = array([0,pi/6,5*pi/6])
#psin = array([0,0,0])


from matplotlib.pyplot import ion, close, gca, subplots

ion()
close('all')

def ana_fft(y,n):
    f       = rfft(y)
    amp     = 2*abs(f) / len(y)
    ang     = angle(f)
    x       = zeros_like(y,dtype=complex)
    x[:n+1] = f[:n+1]
    ret     = irfft(x,n=len(y))
    return amp,ang,ret

def ana_dct(y,n):
    f        =  dct(y)
    amp      =  abs(f) / len(y)
    amp[0]   /= 2;
    ang      =  f
    x        = zeros_like(y)
    x[:2*n]  = f[:2*n]
    ret      = idct(x)
    return amp[::2],ang[1::2],ret


def plot(phi,vn,psin,ana=ana_fft,ax=None):
    fig, ax = subplots(ncols=2,figsize=(10,10))

    n           = len(vn)
    y           = g(phi,vn,psin)
    amp,ang,ret = ana(y,n)
    with printoptions(floatmode='fixed',suppress=True,precision=4):
        print(amp,ang)


    ax[0].plot(phi,y,color='k',label='Input',lw=2)
    ax[0].plot(phi,g(phi,amp[1:n+1],ang[1:n+1]),'--',color='k',label='FFT',lw=2)
    ax[0].plot(phi,ret,':',color='k',label='rFFT',lw=4)
    for i,(v,p,a,f) in enumerate(zip(vn,psin,amp[1:],ang[1:])):
        vi = zeros_like(vn)
        pi = zeros_like(psin)
        ai = zeros_like(vn)
        fi = zeros_like(psin)
        vi[i] = v
        pi[i] = p
        ai[i] = a
        fi[i] = f
        n = i + 1

        print(f'v{n}={v:.3f} ({a:.3f}) psi{n}={p:.5f} {f:.5f}')
        ax[0].plot(phi,g(phi,vi,pi),ls='-',label=f'$n={n}$',color=f'C{n}')
        ax[0].plot(phi,g(phi,ai,fi),ls='--',label=f'FFT $n={n}$',color=f'C{n}')
        

    n = len(vn)
    ax[1].plot(amp)
    ax[0].legend(ncol=n+1)
    fig.tight_layout()
        
plot(phi,vn,psin)
# plot(phi,vn,psin,ana_dct)

                
    
