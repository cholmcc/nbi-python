#!/usr/bin/env python

def f(phi,vn,psin,verb=False):
    from numpy import newaxis, cos, atleast_1d, arange, ndim
    x  = atleast_1d(phi)
    an = atleast_1d(vn)
    pn = atleast_1d(psin)
    assert ndim(an) >= ndim(pn), \
        f'Incompatible vn {an.shape} and psin {pn.shape}'
    assert pn.shape[-1] in an.shape or pn.shape[0] == 1, \
        f'Incompatible vn {an.shape} and psin {pn.shape}'
    
    n  = arange(1,an.shape[-1]+1)
    ar = (x+pn[...,newaxis])
    s  = -2
    if verb: print(f'x-psi            shape= {ar.shape}')
    if ar.shape[-2] != n.shape[0] and ar.shape[-2] != 1:
        print('Reshape')
        n  = n[...,newaxis]
        s  = 0
        if an.T.shape != ar.shape[:2]:
            an = an.T
        
    na = n[...,newaxis]*ar
    if verb: print(f'n(x-psi)         shape={na.shape}')
    ca = cos(na)
    if verb: print(f'cos(n(x-psi))    shape={ca.shape}')
    ac = an[...,newaxis]*ca
    if verb:  print(f'vn*cos(n(x-psi)) shape={ac.shape}')
    
    return ac.sum(axis=s)

from numpy import linspace, pi, repeat, array, zeros

vn   = repeat(array([[0.01,0.08,0.03]]),10,axis=0)
psin = zeros((10,3))
phi  = linspace(0,2*pi,30)

print('30 phi 10,3 vn, 10,3 psi ->',f(phi,   vn,psin,          False).shape)
print('30 phi 10,3 vn, 10   psi ->',f(phi,   vn,psin[:,0],     False).shape)
print('30 phi 10,3 vn, 3    psi ->',f(phi,   vn,psin[0],       False).shape)
print('30 phi 10,3 vn  1    psi ->',f(phi,   vn,     0,        False).shape)
print('30 phi 10   vn, 10   psi ->',f(phi,   vn[:,0],psin[:,0],False).shape)
print('30 phi 10   vn  1    psi ->',f(phi,   vn[:,0],0,        False).shape)
print('30 phi 3    vn, 3    psi ->',f(phi,   vn[0],  psin[0],  False).shape)
print('30 phi 3    vn  1    psi ->',f(phi,   vn[0],  0,        False).shape)
print('1  phi 10,3 vn, 10,3 psi ->',f(phi[0],vn,     psin,     False).shape)
print('1  phi 10,3 vn, 10   psi ->',f(phi[0],vn,     psin[:,0],False).shape)
print('1  phi 10,3 vn, 3    psi ->',f(phi[0],vn,     psin[0],  False).shape)
print('1  phi 10,3 vn, 1    psi ->',f(phi[0],vn,     0,        False).shape)
print('1  phi 10   vn, 10   psi ->',f(phi[0],vn[:,0],psin[:,0],False).shape)
print('1  phi 10   vn, 1    psi ->',f(phi[0],vn[:,0],0,        False).shape)
print('1  phi 3    vn, 3    psi ->',f(phi[0],vn[0],  psin[0],  False).shape)
print('1  phi 3    vn, 1    psi ->',f(phi[0],vn[0],  0,        False).shape)




def s(rng,vn,psin,verb=False,n=100):
    from numpy import linspace, pi, cumsum, searchsorted, array, ndim, newaxis, arange

    phi =  linspace(0,2*pi,30)
    ff  =  f(phi,vn,psin)
    ff  += -ff.min(axis=-1)[...,newaxis]
    cc  =  cumsum(ff,axis=-1)
    cc  /= cc[...,-1][...,newaxis]
    n   =  len(cc) if ndim(cc) > 1 else n
    yy  =  rng.uniform(size=n)
    if ndim(cc) > 1:
        bb = array([searchsorted(u,b) for u,b in
                    zip(cc,yy)])
        nn = arange(n)
        yf = (yy - cc[nn,bb-1]) / (cc[nn,bb]-cc[nn,bb-1])
    else:
        bb = searchsorted(cc,yy)
        yf = (yy - cc[bb-1]) / (cc[bb]-cc[bb-1])

    return phi[...,bb-1]+yf*(phi[...,bb]-phi[...,bb-1])


from numpy.random import default_rng

rng = default_rng(42)

print('30 phi 10,3 vn, 10,3 psi ->',s(rng,vn,psin,          False).shape)
print('30 phi 10,3 vn, 10   psi ->',s(rng,vn,psin[:,0],     False).shape)
print('30 phi 10,3 vn, 3    psi ->',s(rng,vn,psin[0],       False).shape)
print('30 phi 10,3 vn  1    psi ->',s(rng,vn,     0,        False).shape)
print('30 phi 10   vn, 10   psi ->',s(rng,vn[:,0],psin[:,0],False).shape)
print('30 phi 10   vn  1    psi ->',s(rng,vn[:,0],0,        False).shape)
print('30 phi 3    vn, 3    psi ->',s(rng,vn[0],  psin[0],  False).shape)
print('30 phi 3    vn  1    psi ->',s(rng,vn[0],  0,        False).shape)
print('1  phi 10,3 vn, 10,3 psi ->',s(rng,vn,     psin,     False).shape)
print('1  phi 10,3 vn, 10   psi ->',s(rng,vn,     psin[:,0],False).shape)
print('1  phi 10,3 vn, 3    psi ->',s(rng,vn,     psin[0],  False).shape)
print('1  phi 10,3 vn, 1    psi ->',s(rng,vn,     0,        False).shape)
print('1  phi 10   vn, 10   psi ->',s(rng,vn[:,0],psin[:,0],False).shape)
print('1  phi 10   vn, 1    psi ->',s(rng,vn[:,0],0,        False).shape)
print('1  phi 3    vn, 3    psi ->',s(rng,vn[0],  psin[0],  False).shape)
print('1  phi 3    vn, 1    psi ->',s(rng,vn[0],  0,        False).shape)

    
