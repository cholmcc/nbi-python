from numpy import linspace, pi, atleast_1d, atleast_2d, atleast_3d, sin, newaxis, reshape, arange, repeat, diff, cos


def f(phi,vn,psin,detailed=False):
    x          = atleast_3d(phi)
    phases     = atleast_3d(psin)
    amplitudes = atleast_3d(vn)

    phases     = phases.reshape(phases.shape[0],
                                phases.shape[2],
                                phases.shape[1])
    amplitudes = amplitudes.reshape(amplitudes.shape[0],
                                    amplitudes.shape[2],
                                    amplitudes.shape[1])

    assert phases.shape[-1] in [1,amplitudes.shape[-1]], \
        f'Inconsistent n ({phases.shape[-1]}!={amplitudes.shape[-1]})'
    assert phases.shape[-3] in [1,amplitudes.shape[-3]], \
        f'Inconsistent m ({phases.shape[-3]}!={amplitudes.shape[-3]})'

    nn           = amplitudes.shape[-1]
    n            = (arange(nn)+1).reshape(1,1,nn)
    scaled_terms = amplitudes * cos(n * (x - phases))
    ret          = (1 + scaled_terms.sum(axis=-1)) / (2*pi)

    if detailed:
        if phases.shape[0] != amplitudes.shape[0]:
            phases = repeat(phases,amplitudes.shape[0],axis=0)
            
        return ret, amplitudes[:,0,:], phases[:,0,:]
    return ret


def F(phi,vn,psin,detailed=False):
    x          = atleast_3d(phi)
    phases     = atleast_3d(psin)
    amplitudes = atleast_3d(vn)

    phases     = phases.reshape(phases.shape[0],
                                phases.shape[2],
                                phases.shape[1])
    amplitudes = amplitudes.reshape(amplitudes.shape[0],
                                    amplitudes.shape[2],
                                    amplitudes.shape[1])

    assert phases.shape[-1] in [1,amplitudes.shape[-1]], \
        f'Inconsistent n ({phases.shape[-1]}!={amplitudes.shape[-1]})'
    assert phases.shape[-3] in [1,amplitudes.shape[-3]], \
        f'Inconsistent m ({phases.shape[-3]}!={amplitudes.shape[-3]})'

    nn           = amplitudes.shape[-1]
    n            = (arange(nn)+1).reshape(1,1,nn)
    scaled_terms = amplitudes / n * (sin(n * (x - phases)) + sin(n * phases))
    ret          = (x[0,:,0] + scaled_terms.sum(axis=-1)) / (2*pi)

    if detailed:
        if phases.shape[0] != amplitudes.shape[0]:
            phases = repeat(phases,amplitudes.shape[0],axis=0)
            
        return ret, amplitudes[:,0,:], phases[:,0,:]
    return ret


def g(phi,vn,psin):
    try:
        pdfs,vns,psins = f(phi,vn,psin,detailed=True)
        cdfs,vns,psins = F(phi,vn,psin,detailed=True)
    except Exception as e:
        print(e)
        return

    from matplotlib.pyplot import subplots

    fig, ax = subplots(nrows=2,sharex=True,gridspec_kw={'hspace':0},
                       figsize=(6,8))
    print(f'Figure # {fig.number}')

    for pdf,cdf,vn,psin in zip(pdfs,cdfs,vns,psins):
        show = False

        if any(pdf < 0):
            print('W: df/dphi not positive everywhere')
            show = True

        if any(cdf < 0):
            print('W: F not positive everywhere')
            show = True
            
        dcdf = diff(cdf)
        if any(dcdf < 0):
            print('W: dF/dphi not positive everywhere')
            show = True

        if show:
            print(' v  =',vn,vn.sum())
            # print(' psi=',psin)

        tv   = '$v=('+','.join([f'{v:.3f}' for v in vn])+')$'
        tpsi = r'$\Psi=('+','.join([f'{p:.3f}' for p in psin])+')$'
        ax[0].plot(phi,pdf,label=tv+' '+tpsi,ls='--' if show else '-')
        ax[1].plot(phi,cdf,label=tv+' '+tpsi,ls='--' if show else '-')
        print(vn.sum())

    ax[0].set_ylabel('$f$')
    ax[1].set_ylabel('$F$')
    ax[1].set_xlabel(r'$\varphi$')
    ax[1].legend()

from matplotlib.pyplot import ion, close

close('all')
ion()

phi  = linspace(0,2*pi,50)
psi0 = pi/2
psi1 = [pi/4,pi/2,pi]
psi2 = [[p+i/8*pi for p in psi1] for i in range(6)]
vn1  = [0.01,0.05,0.03]
vn2  = [[v*f for v in vn1] for f in [0.5,1,2,5,10,20]]
psil = repeat(psi2,100,axis=0)
vnl  = repeat(vn2, 100,axis=0)

g(phi,vn1,psi0)
g(phi,vn1,psi1)
g(phi,vn2,psi0)
g(phi,vn2,psi1)
g(phi,vn2,psi2)

# g(phi,[1,0.05,0.03],0)
# g(phi,[1.01,0.05,0.03],0)
# g(phi,[0.01,0.05,1.01],0)
# g(phi,[1.01,0,0],0)
# g(phi,[0,1.01,0],0)
# g(phi,[0,0,1.01],0)
# g(phi,[.5,.51,0],[0,pi/4,0])
g(phi,[.6]*4,0)
g(phi,[.7]*4,0)

try:
    f(phi,vn2,[[0,1]]*10)
except Exception as e:
    print(f'Expected n: {e}')

try:
    f(phi,vn1,psi2)
except Exception as e:
    print(f'Expected m: {e}')

    
