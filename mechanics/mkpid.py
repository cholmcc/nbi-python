#!/usr/bin/env python
#
# The input file is downloaded from
#
#    https://pdg.lbl.gov/2021/html/computer_read.html

with open('mass_width_2021.mcd','r') as file:
    lines = file.readlines()

db = {}
ndb = {}

def getfloat(sub,default=-1):
    # print(f'"{sub}"')
    if len(sub.strip()) <= 0:
        return default

    return float(sub)

ch_states = {'0':    0,
             '++':   +2*3,
             '--':   -2*3,
             '+':    +1*3,
             '-':    -1*3,
             '-1/3': -1,
             '+2/3': +2 }

def nam(n,q,ndb):
    from re import sub

    bas = sub(r'([^\(\)]+)(.*)',fr'\1 {q}\2',n).replace(' ','')
    tmp = sub(r'(.*)\([0-9]*\)$',r'\1',bas)
    if tmp in ndb:
        #print(f'Already have {tmp} {q}')
        tmp = bas

    if tmp in ndb:
        raise ValueError(f'{nm} already defined')
        
    return tmp 

maxn = 0

for line in lines:
    if line[0] == '*':
        continue

    spids = [line[i:i+8].strip() for i in [0,8,16,24]]
    pids  = [int(s) for s in spids if len(s) > 0]

    m     = getfloat(line[33:51])
    epm   = getfloat(line[52:60])
    enm   = getfloat(line[61:69])
    w     = getfloat(line[70:88])
    wpm   = getfloat(line[89:97]) 
    wnm   = getfloat(line[98:106])
    nch   = line[107:128]
    n,ch  = nch.split()
    chs   = ch.split(',')

    assert len(pids)==len(chs), f'Mismatch: {pids} vs {chs}'
    for pid,sch in zip(pids,chs):
        chg = ch_states[sch]
        if pid in [1,2,3,4,5,6,12,14,16,21,22,25, #q,g,gamma,H
                   223,1000223,30223,227, # omega
                   2112,2212, # n, p
                   10223, 10333, 10553, 10443, # h
                   ]:
            sch = ''
        if n.startswith('f(') or n.startswith('Omega(c)') \
           or n.startswith('Upsilon') or n.startswith('chi') \
           or n.startswith('eta') or n.startswith('phi') \
           or n.startswith('psi'):
            sch = ''
        if pid % 10000 in [3122,3124,3126,3128,5122]: # Lambda0 states
            sch = '' 
        nm  = nam(n,sch,ndb)
        if pid == 443:  # J/psi
            nm = 'J/psi'
            

        maxn = max(len(nm),maxn)
        
        db[pid] = { 'n': nm,
                    'm': m,
                    'q': chg }
        ndb[nm] = {'i':pid,'q':chg,'m':m}


print('pid_db = {')
for k in sorted(db.keys()):
    v       = db[k]
    n, m, q = v['n'],v['m'],v['q']
    n       = '"'+n+'"'
    print(f'    {k:<8d}: {{"n": {n:{maxn+2}s}, "m": {m:15.10g}, "q": {q:+2d} }},')
print('}')
    
from pprint import pprint
# pprint(db)
pprint(ndb)

    
    


