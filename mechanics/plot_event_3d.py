from matplotlib.pyplot import show, ion, subplot
from mpl_toolkits.mplot3d import Axes3D
from numpy import vstack, sqrt, pi, logical_not

import sys

sys.path.append('statistics')
sys.path.append('mechanics')

import nbi_glauber as glauber 

a = glauber.make_nucleus('Pb',lambda d:d<.4)
b = glauber.make_nucleus('Pb',lambda d:d<.4)
p = glauber.make_profile('blackdisc',70)

aa = a()
bb = b()
aa,bb,coll,ap,bp,ac,bc = glauber.collide(aa,bb,7,0,p)


ax = subplot(projection='3d') 

r = sqrt(70/10/pi)

sa = aa
sb = bb
st = 'wire'
glauber.plot_nucleons_3d(sa,ax,r,style=st,alpha=.3,color='C0') 
glauber.plot_nucleons_3d(sb,ax,r,style=st,alpha=.3,color='C1') 
 
pp = vstack((aa[ap],bb[bp])) 
glauber.plot_nucleons_3d(pp,ax,r,style=st,alpha=.8,color='C2')

show()

