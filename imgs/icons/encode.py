#!/usr/bin/env python3

import os
import sys
from pathlib import Path
from base64 import b64encode as encode

ignore    = ['natfak', 'nbviewer']

for path in Path('.').glob('*'):
    if path.stem in ignore:
        continue

    
    if path.suffix == '.svg':
         tpe = 'svg+xml'
    elif path.suffix == '.png':
        tpe = 'png'
    elif path.suffix == '.py':
        continue    
    else:
        print('Unknown file type: ',path,file=sys.stderr)
        continue

    with open(path,'rb') as file:
        data = encode(file.read())
        file.close()
        
        print(f"            --{path.stem+'_img:':15s} url(data:image/{tpe};base64,"
              f"{data.decode()});")

