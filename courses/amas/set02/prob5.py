import numpy as np
import scipy as sp
import scipy.special
import scipy.optimize
import matplotlib.pyplot as plt
import iminuit 


data = np.genfromtxt('ProblemSet2_Prob5_NucData.txt')
sub  = data.reshape(100,200)

def pdf(t,tau,sigma):
    from numpy import isclose,exp,sqrt,finfo,float
    from scipy.special import erfc
    
    return (exp(sigma**2/2/tau**2-t/tau)
            *erfc((sigma/tau-t/sigma)/sqrt(2))/(2*tau))


def llh(tau,sigma,data):
    return -sum([np.log(pdf(t,tau,sigma)) for t in data])

dllha = np.zeros(len(sub))
for i,s in enumerate(sub):
    fh1  = lambda p : llh(*p,s)
    m    = iminuit.Minuit.from_array_func(fh1,[1,.5],error=[.1,.1],errordef=1)
    o,_  = m.migrad()
    llh1 = o.fval

    fh0  = lambda p : llh(*p,0.5,s)
    m    = iminuit.Minuit.from_array_func(fh0,[1],error=[.1],errordef=1)
    o,_  = m.migrad()
    llh0 = o.fval

    dllh = (llh0-llh1)/2
    
    print(f'{i:3d}\t{llh1:f}\t{llh0}\t{dllh}')
    dllha[i] = dllh

plt.ion()
plt.close('all')
plt.hist(dllha,density=True)

print(sum(dllha > 2.706))


    
    
