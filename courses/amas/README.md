# Advanced Methods in Applied Statistics 2019

Here are materials from the course [_Advanced Methods in Applied
Statistics_](https://www.nbi.dk/~koskinen/Teaching/AdvancedMethodsInAppliedStatistics2019/AdvancedMethodsAppliedStatistics2019.html)
in 2019.

## What I've done 

I've taken some of the code examples provided by the course and tried
to modernise and streamline the code.  I've made Jupyter Notebooks of
the code, since it's a good pedagogical tool to explain what is going
on in the solutions.

In many cases, I've used [_SymPy_](https://sympy.org) to do symbolic
derivations of results.  I've generally tried to take these algebraic
solutions and turn them into numerical solutions by the use of
`sympy.lambdify`.  This is a powerful tool that allows us to go
_directly_ from analytic (or otherwise algebraic) results to
computational code. 

In most cases I've been inspired by the code already provided - in
particular Jean-Loup's solutions are already in pretty good shape. 

I've tried as far as possible to _only_ use the regular
[_SciPy_](https://scipy.org) stack - that is 

- [_NumPy_](https://numpy.org)
- [_SciPy_](https://scipy.org)
- [_Matplotlib_](https://matplotlib.org)
- [_SymPy_](https://sympy.org)

I've tried to avoid other packages, such as 

- [https://iminuit.readthedocs.io](iminuit)
- [https://root.cern.ch](ROOT)
- [https://lmfit.github.io/lmfit-py/](LMFIT)

since these packages are not part of the standard setup on most
platforms (including [https://erda.dk](ERDA)

I do use the non-standard packages 

- [_nestle_]()
- [_scikit-learn_]()
- [_healpy_]()

My versions of the code are all called `cholm.ipynb` in each
directory.

I've _not_ refrained from doing "clever" solutions, but I've tried not
to be too clever and explain what is going on.  

## The classes 

In general, I've modernised the code quite a bit.  A lot of the
examples where not really done the _Pythonic_ way, and some even
suffered from _Fortrantitis_ - a particular ailment that tend to
effect only the code of physicist :-)

- [Class 1](class01/cholm.ipynb) Anscombe's Quartet. The main changes 
  - No explicit `for` loops over data 
  - Robust reading of data file, rather than some hand-crafted parsing 
  
- [Class 2](class02/cholm.ipynb) Monte-Carlo integration 
  - No explicit loops over data 
  - Use of `reduce` idiom in the calculations 
  
- [Class 3](class03/cholm.ipynb) Likelihood estimation 
  - Use of _SymPy_ to derive likelihood function, log-likelihood,
    maximum likelihood estimators, and so on. 
  - Use of `sympy.lambdify` to go from symbolic expressions to numeric
    code. 
	
- [Class 4](class04/cholm.ipynb) 
  - Clean python implementation of both Bayesian analysis and splines 
  
- [Class 5](class05/cholm.ipynb)
  - Again, use of _SymPy_ to derive some equations.
  - Generic implementation of an MLE fitter.
  - Also go Least-squares fits for comparison 
  - Rigorous derivation of uncertainties 
  - Note, unlike the original code, which did an envelope MC
    integration of the PDF for the last code, I actually use the
    inverse CDF, and I do not the troubles noted in a previous
    lecture. 
	
- [Class 6](class06/cholm.ipynb)
  - Handwritten MCMC which is dead simple, 
    - does just as good as egcee
	- and in a quarter of the time 
   
- [Class 7](class07/cholm.ipynb)
  - Re-use code from earlier 
  - Again, use _SymPy_.
  
- [Class 8](class08/cholm.ipynb)
  - Generic KDE code 
  - Effective reuse of earlier code and results 
  
- [Class 9](class09/cholm.ipynb)
  - Use _SymPy_ to solve for CL for normal distribution 
  - Show why Poisson is harder 
  - Generic Feldman-Cousins 
  
- [Class 10](class10/cholm.ipynb)
  - Use PCA rather than machine learning 
    - PCA is much more robust and allows for propagation of
      uncertainties (linear transform)
    - Uncertainties of method known 
	- Mostly just as good as AdaBoost 
  - Consistent treatment of classifier results 

- [Class 11](class11/cholm.ipynb)
  - Short (but complete) code to illustrate Andy's note 
  
- [Class 12](class12/cholm.ipynb)
  - Significant speed ups (factor 100 at least) over original code.  
  - A lot of modernisation (this code had a really bad case of
    Fortrantitis - to the point that it was really hard to read) 
  - Removed a lot of stuff that wasn't needed in the code 
  
- [Class 13](class13/cholm.ipynb)
  - All examples done.  A lot of stuff just taken from the `nestle`
    examples. 

- [Problem set 2](set02/cholm.ipynb)
  - Effective and modern code 
  - Note, in problem 5, I get no where near the -2 log lambda values
    quoted in the (partial) solution available.  To check what is
    going on, I write a ROOT script (`prob5.C`) and a Python script
    (`prob5.py`) which uses Minuit for the minimization.  I get
    consistent results with what I find in the Notebook above.  Either
    I got something very wrong, or the partial solution is wrong
    somehow. 
	


 
  


