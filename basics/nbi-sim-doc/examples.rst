Examples of custom volumes
==========================

Below we give some examples of custom volumes

A tracker
---------

This volume will produce measurements along the path of a track
traversing the volume.  The volume is defined as a rectangle.

.. literalinclude:: ../nbi_sim_exa.py
   :pyobject: Tracker

	      
A hodoscope
-----------

This volume will produce a single measurements of a track traversing
the volume.  The volume is defined as a rectangle.

.. literalinclude:: ../nbi_sim_exa.py
   :pyobject: TOF

A magnet
--------

This volume will bend the trajectory of a charged-particle track track
traversing the volume.  The volume is defined as a rectangle.  Tracks
*not* entering or leaving the volume at one of the ends will be marked
as stopped. 

.. literalinclude:: ../nbi_sim_exa.py
   :pyobject: Magnet


An example of using these volumes
---------------------------------

.. literalinclude:: ../nbi_sim_exa.py
   :pyobject: Test4

.. plot::
   :include-source:

   >>> from nbi_sim_exa import *
   >>> t = Test4()
   >>> t.run()


