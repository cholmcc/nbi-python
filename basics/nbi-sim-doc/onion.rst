An Onion experiment
===================

This shows the transverse design of a typical *onion* design of a
high-energy particle-physics experiment.  The design is essentially
concentric layers of detectors embedded in a solenoidial magnetic
field point out of (or in to) the transverse plane. 

Custom volumes
--------------

First, an inner tracker layer, which is a regular *n*-sided polygon.
This produces a single measurement when a particle traverses the
volume. 

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: Inner

Next, a *Flight-Time* detector.  Similar to the ``Inner`` class
above.

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: FT

The final detector type is a *Time Projection Chamber*.  Here, we
produce measurements along the trajectory of the track.  Note,
however, since it is in a magnetic field, we need to calculate the
path and make the measurements there.

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: TPC

The final volume is the volume representing the magnet.  This is a
large octagon.  Since the magnet consist of mostly iron, and since it
is relatively thick, we will stop all particles that impinge on this
element.

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: Solenoid

Putting it all together
-----------------------

The constructor sets up the volumes.  Note, we pass the magnetic field
medium ``bfl`` to all components, since they all live inside the
magnet.

.. literalinclude:: ../nbi_sim_onion.py
   :lines: 85-99

We define a method to show the experiment

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: Onion.show 		     

We want to throw particles with a transverse momentum
:math:`p_{\mathrm{T}}` drawn from an exponential distribution.
However, we will enforce a lower cut off, since partcles with a
:math:`p_{\mathrm{T}}<p_0` will never be measured.   We there for
define the method `Onion.cutexp` which give back exactly *n* particles
within the range.

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: Onion.cutexp 		     

To shoot a single particle we define `Onion.shoot`.  This will
propagate ``size`` particles through the set-up, and return a
``nbi_sim.History`` object 

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: Onion.shoot		     

And finally, a method to run the whole ting

.. literalinclude:: ../nbi_sim_onion.py
   :pyobject: Onion.run

Running the example
-------------------

.. plot::
   :include-source:

   >>> from nbi_sim_onion import *
   >>> o = Onion()
   >>> o.run()

 

