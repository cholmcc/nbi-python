.. NBI Simulation documentation master file, created by
   sphinx-quickstart on Fri Dec  7 23:58:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NBI Toy Simulation's documentation!
==============================================

Here, you will find various Python classes for simulating an
experimental setup for toy simulation of a HEP experiment. More can be
found in `Toy Simulation
Notebook <../#ToyExpSim>`_

.. toctree::
   :maxdepth: 2

   tutorials	      
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
