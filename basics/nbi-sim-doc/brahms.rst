An Example - BRAHMS
===================

Here, we make an almost complete example of a simulation of a real
life experiment - `BRAHMS <https://www4.rcf.bnl.gov/brahms/WWW/>`_.

The setup
---------

Below we define our master class ``BRAHMS``.  This contains two
spectrometer arms

- the forward spectrometer (*FS* - setup by ``BRAHMS.fs``) consisting
  of a filtering magnet, then a tracker, another bending magnet, and
  another tracker and finally a hodoscope.

 - the mid-rapidity spectrometer (*MRS* - setup by ``BRAHMS.mrs``)
   which starts with a tracker, then a bending magnet, another
   tracker, and then a hodoscope wall with 4 panels.

First, we declare our constructor which accepts the two angles of the
FS and MRS.

.. literalinclude:: ../nbi_sim_brahms.py
   :lines: 8-18

Next, we define a function which will translate coordinates relative
to either FS or MRS to global coordinates.  We use the functionality
of nbi_sim.Combined to do this for us.

.. literalinclude:: ../nbi_sim_brahms.py
   :pyobject: BRAHMS._t

The method ``BRAHMS.fs`` will set-up the forward spectrometer, taking
care to translate and rotate the components according to the passed
angle. 

.. literalinclude:: ../nbi_sim_brahms.py
   :pyobject: BRAHMS.fs
	      
The method ``BRAHMS.mrs`` will set-up the mid-rapidity spectrometer,
taking care to translate and rotate the components according to the
passed angle.

.. literalinclude:: ../nbi_sim_brahms.py
   :pyobject: BRAHMS.mrs
	      
The next method will draw up the experimental setup.  Note, we do a
bit more than usual, just to colour the detectors a little bit nicer.

.. literalinclude:: ../nbi_sim_brahms.py
   :pyobject: BRAHMS.show

We want to shoot particles in the direction of either spectrometer, so
we define a function for that.

.. literalinclude:: ../nbi_sim_brahms.py
   :pyobject: BRAHMS.shoot
	     

The final method implemented will run the simulation

.. literalinclude:: ../nbi_sim_brahms.py
   :pyobject: BRAHMS.run

Running the example
-------------------

Let us run this code

.. plot::
   :include-source:

   >>> from nbi_sim_brahms import *
   >>> brahms = BRAHMS(5,90)
   >>> brahms.run()
   
We can turn the two spectrometer arms

.. plot::
   :include-source:

   >>> from nbi_sim_brahms import *
   >>> brahms = BRAHMS(30,45)
   >>> brahms.run(10,-33,43)

